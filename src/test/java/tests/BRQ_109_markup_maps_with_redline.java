package tests;

import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;
import utils.Utils;

import java.util.Properties;

public class BRQ_109_markup_maps_with_redline extends BaseTest {

    @Test(description = "UGIS-2110 verify that user can create redline (annotation graphics) on a map - ArcGIS Enterprise Portal")
    public void UGIS_2210_Validate_Admin_can_create_redline() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Create new map with layers and notes");

        // reportLog("Click on Organization Tab", test);
        headerPage.clickOnMapTab();

        //reportLog("Click on Add Members", test);
        //mapPage.clickOnBaseMapBtn();
        //mapPage.selectBaseMap("Dark Gray Canvas");

        //reportLog("Choose SAML method", test);
        //mapPage.clickOnAddBtn();

        // reportLog("Click on Next button", test);
        //mapPage.clickOnBrowseLivingAtlasLayers();

        // reportLog("Click on New members from a file button", test);
        //mapPage.selectLivingAtlasLayer("A Children’s Map");

        //  reportLog("Click on Add file button", test);
        mapPage.clickOnAddBtn();
        mapPage.clickOnAddMapNotes();
        mapPage.clickOnCreateMapNotes();

        // reportLog("Click Next", test);
        mapPage.addMapNote("Stickpin");
        mapPage.addMapNote("Freehand Area");
        mapPage.addMapNote("Area");
        //mapPage.addMapNote("Triangle");
        mapPage.addMapNote("Circle");
        mapPage.addMapNote("Text");

        mapPage.clickOnSaveWebMapBtn();
        String mapName = "Test Map "+ Utils.getRandomNumberInRange(1, 500);
        mapPage.inputWebMapName(mapName);
        mapPage.inputTags("#test");
        mapPage.saveWebMap();

        mapPage.goToContentPage();
        contentPage.searchAndSelectMap(mapName);
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.verifyNumberOfMapNotes(5);
        mapPage.verifyTextMapNote("test");
        mapPage.logout();
    }
}
