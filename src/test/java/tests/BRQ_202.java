package tests;

import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.OrganizationPage;
import pages.ProfilePage;
import utils.API.APIUtils;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_202 extends BaseTest {

    @Test(description = "A registered admin user should be able to successfully  add a user role into portal.")
    public void validate_admin_is_able_to_add_a_user_manually_into_the_enterprise_portal() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");
        String selectedUserName = "";
        try {

            headerPage.clickOnOrgTab();

            //reportLog("Click on Add Members", test);
            organizationPage.clickOnAddMembers();

            organizationPage.chooseBuiltInMethod();

            // reportLog("Click on Next button", test);
            organizationPage.clickOnNextBtn();

            // reportLog("Click on Add Members button", test);
            organizationPage.clickOnAddMemberBtn();

            // reportLog("Input First Name", test);
            organizationPage.inputFirstName("First Name");

            // reportLog("Input Last Name", test);
            organizationPage.inputLastName("Last Name");

            //  reportLog("Input Email", test);
            organizationPage.inputEmail("Test@test.com");

            // reportLog("Input Username", test);
            organizationPage.inputUsername("testuser");
            organizationPage.inputPassword("Test@2020");

            organizationPage.selectUserType("Viewer");
            organizationPage.selectUserRole("Viewer");

            // reportLog("Click on Add button", test);
            organizationPage.clickOnAddBtn();

            selectedUserName = organizationPage.getUserName();

            // reportLog("Click Next", test);
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();

            organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

            organizationPage.clickOnMembersTab();
            organizationPage.selectSearchByUsername();
            organizationPage.inputSearchText(selectedUserName);
            organizationPage.selectFirstMemberSearchResult();
            headerPage.logout();

        }
        finally {
            APIUtils.deleteUser(selectedUserName);
        }

    }

    @Test(description = "A registered admin user should be able to successfully  add a user role into portal.")
    public void validate_admin_is_able_to_add_a_user_manually_into_the_enterprise_portal_SAML_method() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        String selectedUserName = "";

        try {
            headerPage.clickOnOrgTab();

            //reportLog("Click on Add Members", test);
            organizationPage.clickOnAddMembers();

            organizationPage.chooseSAMLMethod();

            // reportLog("Click on Next button", test);
            organizationPage.clickOnNextBtn();

            // reportLog("Click on Add Members button", test);
            organizationPage.clickOnAddMemberBtn();

            // reportLog("Input First Name", test);
            organizationPage.inputFirstName("First Name");

            // reportLog("Input Last Name", test);
            organizationPage.inputLastName("Last Name");

            //  reportLog("Input Email", test);
            organizationPage.inputEmail("Test@test.com");

            // reportLog("Input Username", test);
            organizationPage.inputUsername("testuser");

            organizationPage.selectUserType("Creator");
            organizationPage.selectUserRole("User");

            // reportLog("Click on Add button", test);
            organizationPage.clickOnAddBtn();

            selectedUserName = organizationPage.getUserName();

            // reportLog("Click Next", test);
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();

            organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

            organizationPage.clickOnMembersTab();
            organizationPage.selectSearchByUsername();
            organizationPage.inputSearchText(selectedUserName);
            organizationPage.selectFirstMemberSearchResult();
            headerPage.logout();
        }
        finally {
            APIUtils.deleteUser(selectedUserName);
        }

    }

    @Test(description = "A registered admin user should be able to successfully  edit a user role into portal.")
    public void edit_a_user_manually_through_the_Enterprise_Portal_Environment() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        String selectedUserName = "";

        try {
            headerPage.clickOnOrgTab();

            //reportLog("Click on Add Members", test);
            organizationPage.clickOnAddMembers();

            organizationPage.chooseBuiltInMethod();

            // reportLog("Click on Next button", test);
            organizationPage.clickOnNextBtn();

            // reportLog("Click on Add Members button", test);
            organizationPage.clickOnAddMemberBtn();

            // reportLog("Input First Name", test);
            organizationPage.inputFirstName("First Name");

            // reportLog("Input Last Name", test);
            organizationPage.inputLastName("Last Name");

            //  reportLog("Input Email", test);
            organizationPage.inputEmail("Test@test.com");

            // reportLog("Input Username", test);
            organizationPage.inputUsername("testuser");
            organizationPage.inputPassword("Test@2020");

            organizationPage.selectUserType("Creator");
            organizationPage.selectUserRole("User");

            // reportLog("Click on Add button", test);
            organizationPage.clickOnAddBtn();

            selectedUserName = organizationPage.getUserName();

            // reportLog("Click Next", test);
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();

            organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

            organizationPage.clickOnMembersTab();
            organizationPage.selectSearchByUsername();
            organizationPage.inputSearchText(selectedUserName);

            organizationPage.updateUserRole("Viewer");

            organizationPage.selectMemberOption("Manage user type");
            organizationPage.updateUserType("Viewer");
            organizationPage.verifyThatSuccessMsgIsDisplayed("User type and role updated successfully");
            organizationPage.selectMemberOption("View profile");
            profilePage.verifyUserType("Viewer");
            profilePage.verifyUserRole("Viewer");

            headerPage.logout();
        }
        finally {
            APIUtils.deleteUser(selectedUserName);
        }

    }

    @Test(description = "A user can successfully edit a user profile into portal.")
    public void user_can_successfully_edit_their_profile_manually_through_the_Enterprise_Portal_Environment () {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();
        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText("testuser"); //use testuser in sandpit and test77 in dev
        organizationPage.selectFirstMemberSearchResult();
        organizationPage.openUserProfile();
        profilePage.clickOnEditProfile();
        profilePage.updateBio("This is a test user.");
        profilePage.clickOnSaveBtn();
        headerPage.logout();

    }


    @Test(description = "A registered admin user should be able to successfully delete a user in portal.")
    public void delete_a_user_manually_through_the_Enterprise_Portal_Environment() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        organizationPage.chooseBuiltInMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName("First Name");

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName("Last Name");

        //  reportLog("Input Email", test);
        organizationPage.inputEmail("Test@test.com");

        // reportLog("Input Username", test);
        organizationPage.inputUsername("testuser");
        organizationPage.inputPassword("Test@2020");

        organizationPage.selectUserType("Creator");
        organizationPage.selectUserRole("User");

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.selectMemberOption("Delete Member");
        organizationPage.clickOnDeleteMemberBtn();
        organizationPage.verifyThatSuccessMsgIsDisplayed("The member '" + selectedUserName + "' has been deleted.");

        headerPage.logout();
    }


    @Test(description = "A registered admin user should be able to successfully  add a user to a group into portal.")
    public void assign_a_user_manually_through_the_enterprise_portal_environment_to_a_group () {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        GroupsPage groupsPage = new GroupsPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        String selectedUserName = "";
        try {
            headerPage.clickOnOrgTab();

            //reportLog("Click on Add Members", test);
            organizationPage.clickOnAddMembers();

            organizationPage.chooseBuiltInMethod();

            // reportLog("Click on Next button", test);
            organizationPage.clickOnNextBtn();

            // reportLog("Click on Add Members button", test);
            organizationPage.clickOnAddMemberBtn();

            // reportLog("Input First Name", test);
            organizationPage.inputFirstName("First Name");

            // reportLog("Input Last Name", test);
            organizationPage.inputLastName("Last Name");

            //  reportLog("Input Email", test);
            organizationPage.inputEmail("Test@test.com");

            // reportLog("Input Username", test);
            organizationPage.inputUsername("testuser");
            organizationPage.inputPassword("Test@2020");

            organizationPage.selectUserType("Creator");
            organizationPage.selectUserRole("User");

            // reportLog("Click on Add button", test);
            organizationPage.clickOnAddBtn();

            selectedUserName = organizationPage.getUserName();

            // reportLog("Click Next", test);
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();

            organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

            organizationPage.clickOnMembersTab();
            organizationPage.selectSearchByUsername();
            organizationPage.inputSearchText(selectedUserName);

            organizationPage.selectMemberOption("View Groups");
            groupsPage.clickOnAddToGroupsBtn();
            groupsPage.searchForGroupInAddGroupPopup("Testing group"); //"Network Data Viewer"
            groupsPage.selectGroupFromTheListAndAdd("Testing group");
            groupsPage.verifyThatSuccessMsgIsDisplayed("Member successfully added to the selected groups.");
            headerPage.clickOnOrgTab();
            organizationPage.clickOnMembersTab();
            organizationPage.selectSearchByUsername();
            organizationPage.inputSearchText(selectedUserName);

            organizationPage.selectMemberOption("View Groups");
            groupsPage.searchForGroupOrContent("Testing group");
            groupsPage.verifyThatGroupIsPresent("Testing group");
        }
        finally {
            APIUtils.deleteUser(selectedUserName);
        }

    }

    @Test(description = "A registered admin user should be able to successfully  add a user to a group into portal from a group tab")
    public void assign_a_user_manually_through_the_enterprise_portal_environment_to_a_group_from_group_tab () {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        GroupsPage groupsPage = new GroupsPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        String selectedUserName = "";
        try {
            headerPage.clickOnOrgTab();

            //reportLog("Click on Add Members", test);
            organizationPage.clickOnAddMembers();

            organizationPage.chooseBuiltInMethod();

            // reportLog("Click on Next button", test);
            organizationPage.clickOnNextBtn();

            // reportLog("Click on Add Members button", test);
            organizationPage.clickOnAddMemberBtn();

            // reportLog("Input First Name", test);
            organizationPage.inputFirstName("First Name");

            // reportLog("Input Last Name", test);
            organizationPage.inputLastName("Last Name");

            //  reportLog("Input Email", test);
            organizationPage.inputEmail("Test@test.com");

            // reportLog("Input Username", test);
            organizationPage.inputUsername("testuser");
            organizationPage.inputPassword("Test@2020");

            organizationPage.selectUserType("Creator");
            organizationPage.selectUserRole("User");

            // reportLog("Click on Add button", test);
            organizationPage.clickOnAddBtn();

            selectedUserName = organizationPage.getUserName();

            // reportLog("Click Next", test);
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();
            organizationPage.clickOnNextBtn();

            organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

            headerPage.clickOnGroupsTab();
            groupsPage.searchForGroupOrContent("Testing group");
            groupsPage.selectGroup("Testing group");
            groupsPage.clickOnInviteUser();
            groupsPage.searchForUser(selectedUserName);
            groupsPage.selectUserFromTheList(selectedUserName);
            groupsPage.clickOnSendInvitation();
            headerPage.logout();
            headerPage.login(selectedUserName, "Test@2020");
            headerPage.inputSecurityAnswer("Security Answer");
            headerPage.clickOnGroupsTab();
            System.out.println(selectedUserName);
            groupsPage.verifyThatGroupInvitationIsReceived();
            groupsPage.acceptTheInvitationToJoinTheGroup();
            groupsPage.verifyThatGroupIsPresent("Testing group");
        }
        finally {
            APIUtils.deleteUser(selectedUserName);
        }

    }
}

