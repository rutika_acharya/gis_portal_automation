package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_18b extends BaseTest {

    @Test(description = "An ArcGIS user can successfully export data in Shapefile format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_Shapefile_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("Shapefile");
        contentPage.webMapPage.inputExportFileTitle("Export Shapefile");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        contentPage.webMapPage.clickOnDownloadBtn();
        contentPage.verifyThatFileIsDownloaded("Export_Shapefile.zip");
        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
    }

    @Test(description = "An ArcGIS user can successfully export data in CSV format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_CSV_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("CSV");
        contentPage.webMapPage.inputExportFileTitle("Export CSV");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        contentPage.webMapPage.clickOnDownloadBtn();
        contentPage.verifyThatFileIsDownloaded("Export_CSV.zip");
        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
    }

    @Test(description = "An ArcGIS user can successfully export data in Excel format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_Excel_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("Excel");
        contentPage.webMapPage.inputExportFileTitle("Export Excel");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        contentPage.webMapPage.clickOnDownloadBtn();
        contentPage.verifyThatFileIsDownloaded("Export_Excel.xlsx");
        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
    }

    @Test(description = "An ArcGIS user can successfully export data in GeoJSON format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_GeoJSON_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("GeoJSON");
        contentPage.webMapPage.inputExportFileTitle("Export GeoJSON");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        contentPage.webMapPage.clickOnDownloadBtn();
        contentPage.verifyThatFileIsDownloaded("Export_GeoJSON.geojson");
        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
    }

    @Test(description = "An ArcGIS user can successfully export data in Feature Collection format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_Feature_Collection_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("Feature Collection");
        contentPage.webMapPage.inputExportFileTitle("Export Feature Collection");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        headerPage.clickOnContentTab();
        contentPage.verifyThatLayerIsPresentOnContentPage("Export Feature Collection", "1");
        contentPage.searchAndDeleteMapOnContentPage("Export Feature Collection");
        contentPage.searchAndDeleteMapOnContentPage("Test Feature Layer");
    }

    @Test(description = "An ArcGIS user can successfully export data in File Geodatabase format.")
    @Story("BRQ 18b")
    public void verify_that_user_can_successfully_export_data_in_File_Geodatabase_format() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.cleanDownloadDirectory();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndSelectMap("Test Feature Layer");
        contentPage.webMapPage.clickOnExportDropDwn();
        contentPage.webMapPage.chooseOptionToExport("File Geodatabase");
        contentPage.webMapPage.inputExportFileTitle("Export File Geodatabase");
        contentPage.webMapPage.inputExportFileTags("test");
        contentPage.webMapPage.clickOnExportBtn();
        contentPage.webMapPage.clickOnDownloadBtn();
        contentPage.verifyThatFileIsDownloaded("Export_File_Geodatabase.zip");
        headerPage.clickOnContentTab();
        contentPage.deleteAllOfMyContent();
    }
}
