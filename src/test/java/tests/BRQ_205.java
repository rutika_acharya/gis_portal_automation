package tests;

import org.testng.annotations.Test;
import pages.HeaderPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_205 extends BaseTest {

    @Test(description = "A registered user should be able to successfully navigate to the dataset manager from Portal.")
    public void validate_user_is_able_to_navigate_to_the_dataset_manager_from_portal() {

        HeaderPage headerPage = new HeaderPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.clickOnDataSetManagerLink();
        headerPage.verifyThatDataSetManagerIsOpenedInNewWindow();
    }
}

