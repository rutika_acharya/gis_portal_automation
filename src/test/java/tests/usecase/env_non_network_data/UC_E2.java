package tests.usecase.env_non_network_data;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class UC_E2 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully import data as a feature layer using Portal for ArcGIS.")
    @Story("UC_E2")
    public void test () {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        /*headerPage.corporateLogin(properties.getProperty("corpLoginId"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));*/

        headerPage.clickOnMapTab();

        /**
         * Part-A
         */
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Organization");
        mapPage.inputLayerNameInSearchBox("Cultural Heritage");
        mapPage.selectAndAddFeatureLayerToMap("Cultural Heritage");
        mapPage.clickOnBackBtn();
        mapPage.clickOnShowMapContentBtn();
        mapPage.verifyLayerIsDisplayedInContentPane("Cultural Heritage - DATSIP Cultural Heritage Site");
        mapPage.verifyLayerIsDisplayedInContentPane("Cultural Heritage - DATSIP Designated Landscape Area");

        /**
         * Part-B
         */
        mapPage.clickOnShowAttributeTableForLayer("Cultural Heritage - DATSIP Cultural Heritage Site");
        int prev_no_of_features = mapPage.getNoOfFeaturesInAttributeTable();
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuFilter();
        mapPage.selectFieldToFilter("NRW Site ID");
        mapPage.inputValueForSelectedField("CW:A23");
        mapPage.clickOnFilterAndZoomToButton();
        mapPage.verifyTheNoOfFeaturesInAttributeTableAreReduced(prev_no_of_features);

        /**
         * Part-C
         */
        mapPage.clickOnAnalysisBtn();
        mapPage.expandUseProximityTool();
        mapPage.selectCreateBuffersTool();
        mapPage.inputBufferDistance("15");
        mapPage.selectBufferDistanceUnit("Meters");
        mapPage.inputResultLayerName("Cultural Heritage 15m Buffer");
        mapPage.deselectUseCurrentMapExtentCheckbox();
        mapPage.clickOnRunAnalysisBtn();
        mapPage.verifyLayerIsDisplayedInContentPane("Cultural Heritage 15m Buffer");

        /**
         * Part-D
         */
        mapPage.clickOnAddBtn();
        mapPage.clickOnAddMapNotes();
        mapPage.clickOnCreateMapNotes("Use Caution", "Map Notes");
        mapPage.addMapNoteWithTitleAndDescription("Left Arrow", "Search Area", "A 15m search area has been established by a local university for archaeological research. Maintain a minimum distance of 15m from the cultural heritage site");
        String chosenColor = mapPage.changeColorOfTheSymbol(3, 3);
        mapPage.setTransparencyInChangeSymbolDialogue(0);
        mapPage.closeMapNotePopup();
        mapPage.clickOnShowMapContentBtn();
        mapPage.verifyColorOfTheSymbolIsChangedTo(chosenColor);
        mapPage.verifyTransparencyOfTheSymbolIsChangedTo("1", chosenColor);
        mapPage.verifyLayerIsDisplayedInContentPane("Use Caution");

        /**
         * Part-E
         */
        mapPage.clickOnSaveLayer("Cultural Heritage 15m Buffer");
        mapPage.verifyThatLayerIsSaved("Cultural Heritage 15m Buffer");
        mapPage.goToContentPage();
        contentPage.verifyThatLayerIsPresentOnContentPage("Cultural_Heritage_15m_Buffer", "1");
        contentPage.searchAndDeleteMapOnContentPage("Cultural_Heritage_15m_Buffer");
    }
}
