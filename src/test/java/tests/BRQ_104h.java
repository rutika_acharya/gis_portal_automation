package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104h extends BaseTest {

    /*@Test(description = "UGIS-2932 Verify that user can save a layer selection as a favourite in Portal")
    @Story("BRQ 104h")
    public void UGIS_2914_validate_user_can_save_layer_selection_as_favourite_in_Portal () {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        *//*headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        //contentPage.myOrganization.selectFilterItemType("Layers");
        //contentPage.myOrganization.selectFilterItemType("Imagery Layers");
        contentPage.searchAndSelectMap("QldBase_QgovSispUsers");
        contentPage.webMapPage.clickOnOpenInMapViewer();*//*

        headerPage.clickOnMapTab();

        mapPage.findPlaceOnMap("Brisbane");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Brisbane");
        mapPage.cleanDirectory();
        mapPage.takeScreenshot("Before Bookmark");
        mapPage.clickOnBookmarks();
        mapPage.clickOnAddBookmark();
        mapPage.inputBookmarkName("Brisbane");
        mapPage.closeBookmark();
        mapPage.clickOnZoomOut(5);
        mapPage.clickOnBookmarks();
        mapPage.verifyThatBookMarkIsPresent("Brisbane");
        mapPage.clickOnBookmarkWithName("Brisbane");
        mapPage.takeScreenshot("After Bookmark");
        mapPage.compareImage("Before Bookmark", "After Bookmark");
        mapPage.goToContentPage();
        headerPage.logout();
    }*/

    @Test(description = "An EQL user with the correct role, should be able to successfully create and save a new layer in portal, and add to a map")
    @Story("BRQ 104h")
    public void validate_user_can_create_and_save_layer_in_Portal () {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        /*headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        //contentPage.myOrganization.selectFilterItemType("Layers");
        //contentPage.myOrganization.selectFilterItemType("Imagery Layers");
        contentPage.searchAndSelectMap("QldBase_QgovSispUsers");
        contentPage.webMapPage.clickOnOpenInMapViewer();*/

        headerPage.clickOnMapTab();

        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Organization");
        mapPage.inputLayerNameInSearchBox("BiogeographicRegion");
        mapPage.selectAndAddLayerToMap("BiogeographicRegion");
        mapPage.clickOnBackBtn();
        mapPage.clickOnShowMapContentBtn();
        mapPage.clickOnFilterForLayer("BiogeographicRegion");
        mapPage.selectFieldToFilter("Q_REG_NAME");
        mapPage.selectOperationToFilter("contains");
        mapPage.inputValueForSelectedField("Brigalow");
        mapPage.clickOnFilterButton();
        mapPage.clickOnCopy("BiogeographicRegion");
        mapPage.renameLayer("BiogeographicRegion - copy", "Biogeographic Region - subset");
        mapPage.clickOnSaveLayerAsItem("Biogeographic Region - subset");
        mapPage.clickOnCreateItem();
        mapPage.goToContentPage();
        contentPage.searchMapOnContentPage("Biogeographic Region - subset");
        contentPage.verifyThatMapIsPresentOnContentPage("Biogeographic Region - subset");
        headerPage.clickOnMapTab();
        mapPage.createANewMap();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Content");
        mapPage.inputLayerNameInSearchBox("Biogeographic Region - subset");
        mapPage.selectAndAddLayerToMap("Biogeographic Region - subset");
        mapPage.clickOnBackBtn();
        mapPage.clickOnShowMapContentBtn();
        mapPage.verifyLayerIsDisplayedInContentPane("Biogeographic Region - subset");
        mapPage.goToContentPage();
        contentPage.searchAndDeleteMapOnContentPage("Biogeographic Region - subset");
    }
}
