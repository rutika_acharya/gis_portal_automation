package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;
import utils.Utils;

import java.util.Properties;

public class BRQ_109 extends BaseTest {

    @Test(description = "UGIS-2905 Validate User can mark up maps with annotation graphics (redline) Portal")
    @Story("BRQ 109")
    public void UGIS_2905_Validate_Admin_can_create_redline() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnMapTab();

        mapPage.clickOnAddBtn();
        mapPage.clickOnAddMapNotes();
        mapPage.clickOnCreateMapNotes();

        String chosenColor = mapPage.addLineAndChangeSymbolColorByChoosingColorIn(2, 4);
        mapPage.verifyColorOfTheLineIsChangedTo(chosenColor);

        mapPage.addTextMapNote("Brisbane");
        mapPage.verifyTextMapNote("Brisbane");

        mapPage.addMapNote("Stickpin");
        mapPage.addMapNote("Freehand Area");
        mapPage.addMapNote("Area");
        mapPage.addMapNote("Triangle");
        mapPage.addMapNote("Circle");

        mapPage.clickOnSaveWebMapBtn();
        String mapName = "Test Map "+ Utils.getRandomNumberInRange(1, 500);
        mapPage.inputWebMapName(mapName);
        mapPage.inputTags("#test");
        mapPage.saveWebMap();

        mapPage.goToContentPage();
        contentPage.searchAndSelectMap(mapName);
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.verifyNumberOfMapNotes(7);
        mapPage.goToContentPage();
        contentPage.searchAndDeleteMapOnContentPage(mapName);
        contentPage.verifyThatSuccessMsgIsDisplayed("Item successfully deleted.");
        contentPage.logout();
    }
}
