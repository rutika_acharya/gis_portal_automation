package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104e extends BaseTest {

    @Test(description = "Visibility Range –  Successfully Change the visibility range feature layers in Enterprise Portal")
    @Story("BRQ 104e")
    public void validate_administrator_can_change_visibility_range_on_each_feature_layer () {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        contentPage.searchAndSelectMap("Fish Habitat");
        contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.clickOnZoomOut(3);
        mapPage.clickOnSetVisibilityRange("Fish Habitat - Fish habitat area");
        mapPage.selectVisibilityOption("Metropolitan Area");
        mapPage.verifyVisibilityIsOutOfRangeForLayer("Fish Habitat - Fish habitat area");

        mapPage.clickOnZoomIn(9);
        mapPage.verifyVisibilityIsInRangeForLayer("Fish Habitat - Fish habitat area");
    }
}
