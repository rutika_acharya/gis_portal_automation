package tests;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;
import utils.WebDriverController;

import java.io.ByteArrayInputStream;
import java.util.Properties;

public class BaseTest {

    private static Logger logger = LogManager.getLogger(WebDriverController.class);

    @BeforeMethod
    public void setup() {
        logger.info("In base test setup");
        logger.info("BEFORE METHOD!!!!!!!!!");
        WebDriverController.getWebDriverControllerInstance().setDriver();
        //page = new BasePage();
        //System.out.println("Initialized "+ name +" with driver" + WebDriverController.getWebDriverControllerInstance().getDriver().hashCode());
		/*page.openPage(LoadProperties.getProperties().getProperty("app.link"));
		page.login();*/
    }

    @AfterMethod
    public void tearDown() {

        screenshot();
		/*System.out.println(WebDriverController.getWebDriverControllerInstance().getDriver().hashCode());
		WebDriverController.getWebDriverControllerInstance().killWebDriver();*/
        if (!(WebDriverController.getWebDriverControllerInstance().getDriver() == null)) {
            logger.info("Killing driver");
            WebDriverController.getWebDriverControllerInstance().getDriver().quit();
        }

    }

    public void reportLog(String message, ExtentTest test) {
        test.log(LogStatus.INFO, message);//For extentTest HTML report
        Reporter.log(message);
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] screenshot() {
        return ((TakesScreenshot) WebDriverController.getWebDriverControllerInstance().getDriver()).getScreenshotAs(OutputType.BYTES);
    }

   /* @AfterSuite
    public void cleanup(){
        System.out.println("###### Cleaning up My Content ######");
        ContentPage page = new ContentPage();
        page.deleteAllOfMyContent();
    }
*/
}
