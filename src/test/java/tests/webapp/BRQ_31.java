package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_31 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully apply buffer zone using the Screening widget in an ArcGIS Web App.")
    @Story("BRQ 31")
    public void test_BRQ_31_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        webAppPage.searchOnMap2("Toowoomba");
        webAppPage.closeSearchPopup();
        webAppPage.clickOnScreeningWidget();
        webAppPage.screeningWidgetPopup.clickOnDrawMode("Point");
        webAppPage.drawPointOnTheMap();
        webAppPage.screeningWidgetPopup.inputBufferDistance("50");
        webAppPage.screeningWidgetPopup.selectBufferUnit("Kilometers");
        webAppPage.screeningWidgetPopup.clickOnReportBtn();
        webAppPage.screeningWidgetPopup.clickOnDownloadBtn();

    }

}
