package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104e_104a_104k_104f_9b extends BaseTest {

    @Test(description = "An app user can successfully view layers and popups at different visibility range based on the feature layer relevance.")
    @Story("BRQ 104e 104a 104k 104f 9b")
    public void test_2_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));

        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Change visibility of a layer
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.turnOnLayer("Managed Environment");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnSetVisibilityRange();
        webAppPage.selectVisibilityOption("Cities");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.verifyVisibilityIsOutOfRangeForLayer("Managed Environment");
        webAppPage.clickOnZoomIn(10);
        webAppPage.verifyVisibilityIsInRangeForLayer("Managed Environment");

        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnSetVisibilityRange();
        webAppPage.selectVisibilityOption("States");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnZoomToForLayer();
        //webAppPage.turnOnAllLAyersOnTheWebApp();
        webAppPage.clickOnHomeBtn();
        webAppPage.closePanelForWidget("Layer List");
        webAppPage.clickOnAFeatureForEnvRestrictedMap2();
        webAppPage.verifyThatPopupIsPresentOnMap();
        webAppPage.clickOnMoreOptionsBtnInPopup();
        webAppPage.clickOnViewInAttributeTable();
        webAppPage.verifyThatAttributeTableIsDisplayed();
    }
}
