package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104k extends BaseTest {

    @Test(description = "An ArcGIS user can successfully loacte the the layer widget to understand the Symbology in the  web app")
    @Story("BRQ 104k")
    public void test_BRQ_104k_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));

        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Change visibility of a layer
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.turnOnLayer("Managed Environment");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnSetVisibilityRange();
        webAppPage.selectVisibilityOption("Cities");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.verifyVisibilityIsOutOfRangeForLayer("Managed Environment");
        webAppPage.clickOnZoomIn(10);
        webAppPage.verifyVisibilityIsInRangeForLayer("Managed Environment");

        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnSetVisibilityRange();
        webAppPage.selectVisibilityOption("States");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnZoomToForLayer();
        //webAppPage.turnOnAllLAyersOnTheWebApp();
        webAppPage.clickOnHomeBtn();
        webAppPage.closePanelForWidget("Layer List");
        webAppPage.clickOnAFeatureForEnvRestrictedMap2();
        webAppPage.verifyThatPopupIsPresentOnMap();
        webAppPage.clickOnMoreOptionsBtnInPopup();
        webAppPage.clickOnViewInAttributeTable();
        webAppPage.verifyThatAttributeTableIsDisplayed();
    }
}
