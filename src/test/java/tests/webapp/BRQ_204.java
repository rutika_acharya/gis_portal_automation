package tests.webapp;

import org.testng.annotations.Test;
import pages.*;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_204 extends BaseTest {

    @Test(description = "A registered user should be able to successfully view UNM data in the EQL QLD service extent in a web application")
    public void validate_user_is_able_to_view_UNM_data_in_the_EQL_QLD_service_extent_in_a_web_application() {

        HeaderPage headerPage = new HeaderPage();
        WebAppPage webAppPage = new WebAppPage();
        GroupsPage groupsPage = new GroupsPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Featured Maps and Apps");
        groupsPage.selectGroup("Featured Maps and Apps");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("Base Network Web App");
        groupsPage.selectGroupContent("Base Network Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Network Web App");
        webAppPage.clickOnHomeBtn();
        webAppPage.clickOnLayerListTab();
        webAppPage.expandLayer("Electric Network");
        webAppPage.expandSubLayer("Feeder");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("Distribution Subnet Line");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("Transmission Subnet Line");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("Subtransmission Subnet Line");

        webAppPage.verifyThatLayerIsDisplayedOnTheMap("Electric_Subnet_Line");
    }
}

