package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104g extends BaseTest {

    @Test(description = "An ArcGIS user can successfully measure distance in a web app .")
    @Story("BRQ 104g")
    public void test_BRQ_104g_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");//Restricted
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Measuring Widget Test
         */
        webAppPage.clickOnMeasurementTab();
        webAppPage.selectTypeOfMeasuringToPerform("Distance");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Kilometers");
        webAppPage.drawLineToMeasureDistance();
        String measurementKm = webAppPage.getMeasurementResultValue().replace(" Kilometers", "").replace("," , "");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Meters");
        String measurementM = webAppPage.getMeasurementResultValue().replace(" Meters", "").replace("," , "");
        webAppPage.verifyMeasurementKmToM(measurementKm, measurementM);

        webAppPage.selectTypeOfMeasuringToPerform("Location");
        webAppPage.drawPointOnTheMap();
        webAppPage.verifyLatitudeAndLongitudeOfThePoint();

        webAppPage.selectTypeOfMeasuringToPerform("Area");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Sq Kilometers");
        webAppPage.drawAreaToMeasureDistance();
        measurementKm = webAppPage.getMeasurementResultValue().replace(" Sq Kilometers", "").replace("," , "");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Sq Meters");
        measurementM = webAppPage.getMeasurementResultValue().replace(" Sq Meters", "").replace("," , "");
        webAppPage.verifyMeasurementSqKmToM(measurementKm, measurementM);
    }
}
