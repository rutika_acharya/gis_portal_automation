package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.*;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_122 extends BaseTest {

    @Test(description = "Verify that An ArcGIS user can successfully add QLD and NSW Cadastre services to a map using ArcGIS Web App.")
    @Story("BRQ 122")
    public void  successfully_add_QLD_and_NSW_Cadastre_services_to_a_new_map_using_ArcGIS_WebApp() {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        webAppPage.clickOnAddDataTab();
        webAppPage.searchLayerToAddToTheWebApp("Managed Cadastre");
        webAppPage.addLayerToWebApp("Managed Cadastre", "Feature Service");

        webAppPage.searchLayerToAddToTheWebApp("NSW Cadastre");
        webAppPage.addLayerToWebApp("NSW Cadastre", "Map Service");

        webAppPage.searchOnMap2("Jubilee Park, Alfred St, Mackay");
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Easement Parcel");
        webAppPage.verifyVisibilityIsInRangeForLayer("Easement Parcel");
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Strata Parcel");
        webAppPage.verifyVisibilityIsInRangeForLayer("Strata Parcel");
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Volumetric Parcel");
        webAppPage.verifyVisibilityIsInRangeForLayer("Volumetric Parcel");
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Roads and Waterways");
        webAppPage.verifyVisibilityIsInRangeForLayer("Roads and Waterways");
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Lots");
        webAppPage.verifyVisibilityIsInRangeForLayer("Lots");

        webAppPage.verifyThatLayerIsDisplayedOnTheMap("ManagedCadastre");

        webAppPage.searchOnMap2("Sydney Observatory");
        webAppPage.expandLayer("NSW Cadastre");
        webAppPage.expandSubLayer("Cadastre_Labels");
        webAppPage.expandSubLayer("Cadastre");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("PlanExtent_Labels");
        webAppPage.verifyVisibilityIsInRangeForLayer("PlanExtent_Labels");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("SectionExtent_Labels");
        webAppPage.verifyVisibilityIsInRangeForLayer("SectionExtent_Labels");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("Lot_Labels");
        webAppPage.verifyVisibilityIsInRangeForLayer("Lot_Labels");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("SectionExtent");
        webAppPage.verifyVisibilityIsInRangeForLayer("SectionExtent");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("PlanExtent");
        webAppPage.verifyVisibilityIsInRangeForLayer("PlanExtent");
        webAppPage.verifyThatSubLayerIsDisplayedOnTheWidgetPane("Lot");
        webAppPage.verifyVisibilityIsInRangeForLayer("Lot");

        webAppPage.verifyThatLayerIsDisplayedOnTheMap("NSW_Cadastre");
    }
}
