package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_105a extends BaseTest {

    @Test(description = "An ArcGIS user can successfully create, modify, delete, save, locate and select a feature in web apps and view who has made the edit")
    @Story("BRQ 104d")
    public void test_BRQ_105a_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");//Restricted
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        webAppPage.clickOnEditWidget();
        webAppPage.selectEditWidgetFeatureLayersInDrpDwn("Points Notes");
        webAppPage.selectFeatureLayerInEditWidget();
        webAppPage.drawPointOnTheMap();
        webAppPage.inputEditPopupDetails("Maintenances needed", "Lose connection");
        webAppPage.clickOnEditPopupSaveBtn();
        webAppPage.clickOnEditPopupCloseBtn();
        webAppPage.clickOnPointLayerOnWebApp();
        webAppPage.verifyTheEditorTrackingInfo();
        webAppPage.refresh();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");//Restricted
        webAppPage.clickOnEditWidget();
        webAppPage.clickOnPointLayerOnWebApp();
        webAppPage.deleteEditPointOnWebApp();
    }
}
