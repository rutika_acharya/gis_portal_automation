package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_110a extends BaseTest {

    @Test(description = "An ArcGIS user can successfully perform the Geospatial analysis Buffer tool in the web app Screen Widget - Network Report")
    @Story("BRQ 110a")
    public void test_BRQ_110a_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        webAppPage.searchOnMap2("Aspley");
        webAppPage.closeSearchPopup();

        webAppPage.cleanDownloadDirectory();
        webAppPage.clickOnScreeningWidget();

        webAppPage.screeningWidgetPopup.clickOnDrawMode("Polyline");
        webAppPage.drawLineToMeasureDistance();
        webAppPage.screeningWidgetPopup.inputBufferDistance("50");
        webAppPage.screeningWidgetPopup.selectBufferUnit("Meters");
        webAppPage.screeningWidgetPopup.clickOnReportBtn();
        webAppPage.screeningWidgetPopup.clickOnDownloadBtn();
        webAppPage.verifyThatFileIsDownloaded("UNM_Report - Electric Subnet Line.csv");


        /**/

        webAppPage.screeningWidgetPopup.clickOnSettingsForLayerInPopup("Non-Isolating Device");
        webAppPage.screeningWidgetPopup.selectFieldsForTheLayer("Asset type, Construction Code");
        webAppPage.screeningWidgetPopup.verifyTheFieldsDisplayedForSelectedLayer("Asset type, Construction Code", "Non-Isolating Device");

        webAppPage.screeningWidgetPopup.clickOnPrintBtn();
        webAppPage.screeningWidgetPopup.selectPrintLayout("A4 Landscape");
        webAppPage.screeningWidgetPopup.clickOnPopupPrintBtn();
        webAppPage.screeningWidgetPopup.verifyThatPrintReportIsOpenedInNewWindow();

    }

    @Test(description = "An ArcGIS user can successfully search for features and view search results in Web Applications using the Query Widget and export search results to CSV.")
    @Story("BRQ 110a")
    public void test2_BRQ_110a_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        ContentPage contentPage = new ContentPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        webAppPage.cleanDownloadDirectory();

        webAppPage.clickOnQueryWidget();
        webAppPage.queryWidgetPopup.selectLayerInQueryWidget("Electric Line");
        //webAppPage.queryWidgetPopup.selectQueryCriteriaAssetType("Isolating Switch Low Voltage");
        webAppPage.queryWidgetPopup.provideInputForCriteria("Subnetwork name", "SP1919-K/DLL1");
        webAppPage.queryWidgetPopup.clickOnApplyBtn();
        webAppPage.queryWidgetPopup.clickOnActionBtnOnQueryResultsPopup();
        webAppPage.queryWidgetPopup.selectActionFromPopupMenu("View in Attribute Table");
        webAppPage.verifyThatAttributeTableIsDisplayed();
        int expectedNoOfFeatures = webAppPage.queryWidgetPopup.getNoOfFeaturesDisplayedInQueryResult();
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesDisplayedInAttributeTableIsEqualTo(expectedNoOfFeatures);
        webAppPage.webAppAttributeTable.clickOnHideAttributeTable();

        webAppPage.queryWidgetPopup.clickOnActionBtnOnQueryResultsPopup();
        webAppPage.queryWidgetPopup.selectActionFromPopupMenu("Export to feature collection");
        webAppPage.verifyThatFileIsDownloaded("features.json");

        webAppPage.queryWidgetPopup.clickOnActionBtnOnQueryResultsPopup();
        webAppPage.queryWidgetPopup.selectActionFromPopupMenu("Export to CSV file");
        webAppPage.verifyThatFileIsDownloaded("features.csv");
        webAppPage.verifyTheCount(expectedNoOfFeatures+1, webAppPage.getNoOfRecordsInCSVFile("features.csv"));

        webAppPage.queryWidgetPopup.clickOnActionBtnOnQueryResultsPopup();
        webAppPage.queryWidgetPopup.selectActionFromPopupMenu("Save to My Content");
        webAppPage.queryWidgetPopup.inputTitleInSaveToMyContentPopup("Test");
        webAppPage.queryWidgetPopup.clickOnOkInSaveToMyContentPopup();
        webAppPage.switchToPreviousWindow();
        headerPage.clickOnContentTab();
        contentPage.verifyThatLayerIsPresentOnContentPage("Test", "1");
        contentPage.searchAndDeleteMapOnContentPage("Test");

        webAppPage.switchToNewWindow();
        webAppPage.queryWidgetPopup.clickOnActionBtnOnQueryResultsPopup();
        webAppPage.queryWidgetPopup.selectActionFromPopupMenu("Remove this result");
        webAppPage.queryWidgetPopup.verifyThatNoQueryResultsAreDisplayed();

    }

    @Test(description = "An ArcGIS user can successfully perform the Geospatial analysis Buffer tool in the web app Screen Widget - Network Report")
    @Story("BRQ 110a")
    public void test3_BRQ_110a_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        webAppPage.searchOnMap2("Aspley");
        webAppPage.closeSearchPopup();

        webAppPage.cleanDownloadDirectory();
        webAppPage.clickOnScreeningWidget();

        webAppPage.screeningWidgetPopup.clickOnDrawMode("Polyline");
        webAppPage.drawLineToMeasureDistance();
        webAppPage.screeningWidgetPopup.inputBufferDistance("50");
        webAppPage.screeningWidgetPopup.selectBufferUnit("Meters");
        webAppPage.screeningWidgetPopup.clickOnReportBtn();
        webAppPage.screeningWidgetPopup.clickOnDownloadBtn();
        webAppPage.verifyThatFileIsDownloaded("UNM_Report - Electric Subnet Line.csv");


        webAppPage.screeningWidgetPopup.clickOnSettingsForLayerInPopup("Non-Isolating Device");
        webAppPage.screeningWidgetPopup.selectFieldsForTheLayer("Asset type, Construction Code");
        webAppPage.screeningWidgetPopup.verifyTheFieldsDisplayedForSelectedLayer("Asset type, Construction Code", "Non-Isolating Device");

        webAppPage.screeningWidgetPopup.clickOnPrintBtn();
        webAppPage.screeningWidgetPopup.selectPrintLayout("A4 Landscape");
        webAppPage.screeningWidgetPopup.clickOnPopupPrintBtn();
        webAppPage.screeningWidgetPopup.verifyThatPrintReportIsOpenedInNewWindow();

        webAppPage.screeningWidgetPopup.clickOnBackBtn();

        webAppPage.searchOnMap2("Ipswich");
        webAppPage.closeSearchPopup();

        webAppPage.screeningWidgetPopup.clickOnDrawMode("Point");
        webAppPage.drawPointOnTheMap();
        webAppPage.screeningWidgetPopup.inputBufferDistance("50");
        webAppPage.screeningWidgetPopup.selectBufferUnit("Meters");
        webAppPage.screeningWidgetPopup.clickOnReportBtn();

        webAppPage.screeningWidgetPopup.clickOnBackBtn();

        webAppPage.clickOnHomeBtn();

        webAppPage.screeningWidgetPopup.clickOnShapeFileTab();
        webAppPage.screeningWidgetPopup.clickOnUploadBtn();
        webAppPage.screeningWidgetPopup.uploadShapeFile("C:\\Users\\ra097\\IdeaProjects\\GIS_Automation_Allure\\src\\test\\resources\\data\\BRQ_18_data\\Depot_final.zip");

    }
}
