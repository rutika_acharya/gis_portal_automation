package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_109 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully draw graphics and text on the map using an ArcGIS Web App.")
    @Story("BRQ 109")
    public void test_BRQ_9_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();

        /**
         * Draw Point
         */
        //.clickOnSplashScreenOkBtn();
        webAppPage.clickOnDrawTab();
        webAppPage.selectDrawMode("Point");
        webAppPage.setSymbolSizeAs("30");
        webAppPage.changeColorOfTheSymbolTo("214", "23", "106");
        //webAppPage.setTransparency(25);
        webAppPage.setOutlineWidth("2");
        webAppPage.setTransparency(25);
        webAppPage.changeOutlineColorOfTheSymbolTo("27", "211", "64");
        webAppPage.drawPointOnTheMap();
        webAppPage.verifyThatItemIsAddedOnTheMap();
        webAppPage.verifyTheColorOfTheItem("214, 23, 106");
        webAppPage.verifyTheOutlineColorOfTheItem("27, 211, 64");
        webAppPage.verifyTheOutlineWidthOfTheItem("2");
        webAppPage.verifyTheSizeOfPointNote("30");
        webAppPage.clickOnUndoBtn();
        webAppPage.verifyThatItemIsRemovedFromTheMap();
        webAppPage.clickOnRedoBtn();
        webAppPage.verifyThatItemIsAddedOnTheMap();
        webAppPage.clickOnClearBtn();
        webAppPage.verifyThatItemIsRemovedFromTheMap();

        /**
         * Draw Text
         */
        webAppPage.selectDrawMode("Text");
        webAppPage.inputTextForTextNote("Test");
        webAppPage.changeColorOfTheTextTo("200", "30", "100");
        webAppPage.setFontSizeOfTheText("30");
        webAppPage.drawPointOnTheMap();
        webAppPage.verifyThatItemIsAddedOnTheMap();
        webAppPage.verifyTheColorOfTheItem("200, 30, 100");
        webAppPage.verifyTheSizeOfTextNote("30");
        webAppPage.verifyTextOfTheTextNote("Test");
        webAppPage.clickOnUndoBtn();
        webAppPage.verifyThatItemIsRemovedFromTheMap();
        webAppPage.clickOnRedoBtn();
        webAppPage.verifyThatItemIsAddedOnTheMap();
        webAppPage.clickOnClearBtn();
        webAppPage.verifyThatItemIsRemovedFromTheMap();
    }
}
