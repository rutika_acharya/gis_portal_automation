package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104h extends BaseTest {

    @Test(description = "An ArcGIS user can successfully save the layer in the content page or as a layer, find statistics and locate the attribute of list of the the highlighted search results through Select Widget .")
    @Story("BRQ 104h")
    public void test_BRQ_104h_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");//Restricted
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        webAppPage.clickOnSelectTab();
        webAppPage.clickOnMakeAllLayersSelectable();
        webAppPage.clickOnSelectBtn();
        webAppPage.drawAnAreaOnTheMap(-250,-250,200,300);
        String layerWithHighestCount = webAppPage.getSelectWidgetLayerWithHighestCount();
        webAppPage.clickOnMoreOptionsForLayerInSelectWidget(layerWithHighestCount);
        webAppPage.selectOptionToExportInSelectWidget("Statistics...");
        webAppPage.verifyThatPopupIsDisplayed("Statistics");
        webAppPage.closePopup();
        webAppPage.clickOnMoreOptionsForLayerInSelectWidget(layerWithHighestCount);
        webAppPage.selectOptionToExportInSelectWidget("Create layer");
        webAppPage.inputLayerNameInCreateLayerPopup("test layer");
        webAppPage.clickOnCreateOrSaveLayerPopupOkBtn();
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("test layer");
        webAppPage.closePanelForWidget("Layer List");
        webAppPage.clickOnSelectTab();
        webAppPage.clickOnMoreOptionsForLayerInSelectWidget(layerWithHighestCount);
        webAppPage.selectOptionToExportInSelectWidget("Save to My Content");
        webAppPage.verifyThatPopupIsDisplayed("Save to My Content");
        webAppPage.inputTitleInSaveToMyContentPopup("test");
        webAppPage.clickOnCreateOrSaveLayerPopupOkBtn();
        int expectedCount = webAppPage.getTheCountForLayerInSelectWidget(layerWithHighestCount);
        webAppPage.clickOnMoreOptionsForLayerInSelectWidget(layerWithHighestCount);
        webAppPage.selectOptionToExportInSelectWidget("View in Attribute Table");
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesDisplayedInAttributeTableIsEqualTo(expectedCount);

    }
}
