package tests.webapp;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.GroupsPage;
import pages.HeaderPage;
import pages.WebAppPage;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_12 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully search in Web Applications and Export to CSV.")
    @Story("BRQ 12")
    public void test_BRQ_12_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         *
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Managed Environment"); //"Managed Environment" Biosecurity
        //webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        //webAppPage.clickOnZoomToForLayer();
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnViewInAttributeTableForLayer();
        webAppPage.verifyThatAttributeTableIsDisplayed();
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Filter");
        webAppPage.webAppAttributeTable.addFilterExpression();
        webAppPage.webAppAttributeTable.selectFilterField("Township"); // Biosecurity zone
        webAppPage.webAppAttributeTable.provideFieldValue("CLERMONT 4721"); // MONTO 4630 // Electric ant biosecurity zone EA02
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesAreOne();
        String zoomLevel = webAppPage.getMapZoomLevel();
        webAppPage.webAppAttributeTable.doubleClickOnTheAttributeRow();
        //webAppPage.verifyThatMapIsZoomedIn(zoomLevel);
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Export selected to CSV");
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.clickOnHomeBtn();
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Filter");
        webAppPage.webAppAttributeTable.deleteFilter();
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.verifyThatFilterIsDeletedByCheckingNoOfFeatures();
    }

    @Test(description = "An ArcGIS user can Successfully search for features and view search results in Web Applications using the Query Widget.")
    @Story("BRQ 12")
    public void test2_BRQ_12_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        String previousZoomLevel = webAppPage.getMapZoomLevel();
        webAppPage.clickOnQueryTab();
        webAppPage.clickOnQueryFeature("EMP Boundary"); //Access Track
        webAppPage.inputValuesInQueryCriteriaForAccessTrack("Tuan", ""); //76
        webAppPage.clickOnQueryWidgetApplyBtn();
        webAppPage.verifyThatMapIsZoomedIn(previousZoomLevel);
        webAppPage.verifyThatResultsSectionIsDisplayed();
        webAppPage.clickOnQueryResultActionBtn();
        webAppPage.selectQueryResultAction("View in Attribute Table");
        webAppPage.verifyThatQueryFilterIsAppliedInAttributeTable2("Tuan"); //76
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Export all to CSV");
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.clickOnHideAttributeTable();
        webAppPage.clickOnQueryResultActionBtn();
        webAppPage.selectQueryResultAction("Remove this result");
        webAppPage.verifyThatResultsAreRemovedFromResultSection();
    }

    @Test(description = "An ArcGIS user can Successfully search for features and view search results in Web Applications using the Query Widget.")
    @Story("BRQ 12")
    public void test3_BRQ_12_Web_App () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        headerPage.clickOnGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");
        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         *
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Managed Environment"); //"Managed Environment" Biosecurity
        //webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        //webAppPage.clickOnZoomToForLayer();
        webAppPage.clickOnMoreOptionsForLayer("Managed Environment");
        webAppPage.clickOnViewInAttributeTableForLayer();
        webAppPage.verifyThatAttributeTableIsDisplayed();
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Filter");
        webAppPage.webAppAttributeTable.addFilterExpression();
        webAppPage.webAppAttributeTable.selectFilterField("Township"); // Biosecurity zone
        webAppPage.webAppAttributeTable.provideFieldValue("CLERMONT 4721"); // MONTO 4630 // Electric ant biosecurity zone EA02
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.verifyThatNoOfFeaturesAreOne();
        String zoomLevel = webAppPage.getMapZoomLevel();
        webAppPage.webAppAttributeTable.doubleClickOnTheAttributeRow();
        //webAppPage.verifyThatMapIsZoomedIn(zoomLevel);
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Export selected to CSV");
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.clickOnHomeBtn();
        webAppPage.webAppAttributeTable.clickOnOptions();
        webAppPage.webAppAttributeTable.selectOption("Filter");
        webAppPage.webAppAttributeTable.deleteFilter();
        webAppPage.webAppAttributeTable.clickOnOKBtn();
        webAppPage.webAppAttributeTable.verifyThatFilterIsDeletedByCheckingNoOfFeatures();
        webAppPage.webAppAttributeTable.clickOnHideAttributeTable();
        webAppPage.closePanelForWidget("Layer List");

        webAppPage.clickOnPrintWidgetTab();
        webAppPage.clickOnPrintBtn();
        webAppPage.verifyThatPrintProgressBarIsDisplayed();
        webAppPage.verifyThatLinkForThePrintJobIsDisplayed();
        webAppPage.clickOnPrintJobLink();
        webAppPage.verifyThatPDFFileIsOpenedInNewWindow();
        webAppPage.clickOnClearPrints();
        webAppPage.verifyThatLinkForThePrintJobIsNotDisplayed();

    }
}
