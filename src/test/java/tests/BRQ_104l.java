package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import pages.OrganizationPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104l extends BaseTest {

    /*@Test(description = "UGIS-2930 Validate that system can consume data from web data service and defined dataset in Portal")
    @Story("BRQ 104l")
    public void UGIS_2930_validate_system_can_consume_data_from_web_data_service_in_Portal() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.clickOnAddItem();
        contentPage.clickOnAddItemFromWeb();
        contentPage.clickOnTypeWMSOGC();
        contentPage.inputItemURL("https://gisservices.information.qld.gov.au/arcgis/services/Transportation/Roads/MapServer/WMSServer?request=GetCapabilities&service=WMS");
        contentPage.inputItemTitle("QLD Roads");
        contentPage.inputItemTags("#Roads");
        contentPage.clickOnAddItemBtn();
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.goToContentPage();
        contentPage.searchAndDeleteMapOnContentPage("QLD Roads");
        //mapPage.logout();
    }*/

    @Test(description = "An creator user can successfully add in data from the web for future use in the contents")
    @Story("BRQ 104l")
    public void test_104la() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.clickOnAddItem();
        contentPage.clickOnAddItemFromWeb();
        //contentPage.clickOnTypeWMSOGC();
        contentPage.inputItemURL("https://gisservices.information.qld.gov.au/arcgis/rest/services/Farming/Biosecurity/MapServer");
        contentPage.inputItemTitle("104l Test1");
        contentPage.clickOnAssignCategoryDrpDwn();
        contentPage.selectCategory("Natural Environment");
        contentPage.inputItemTags("#test");
        contentPage.clickOnAddItemBtn();
        headerPage.clickOnContentTab();
        //contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.goToContentPage();
        contentPage.verifyThatMapIsPresentOnContentPage("104l Test1");
        contentPage.searchAndDeleteMapOnContentPage("104l Test1");
        contentPage.verifyThatSuccessMsgIsDisplayed("Item successfully deleted.");
        contentPage.logout();
    }

    @Test(description = "An creator user can successfully add in data from the web for future use in the map.")
    @Story("BRQ 104l")
    public void test_104laa() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnAddLayerFromWeb();
        mapPage.inputWebLayerURL("https://gisservices.information.qld.gov.au/arcgis/rest/services/Farming/Biosecurity/MapServer");
        mapPage.clickOnAddLayerBtn();
        mapPage.verifyLayerIsDisplayedInContentPane("Biosecurity");
        mapPage.clickOnZoomToLayer("Biosecurity");
        mapPage.verifyLayerIsDisplayedOnTheMap("Biosecurity");
        mapPage.logout();
    }

    @Test(description = "A creator user can successfully add in  a portal item into the a map.")
    @Story("BRQ 104l")
    public void test_104lac() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Organization");
        mapPage.inputLayerNameInSearchBox("Biosecurity");
        mapPage.selectAndAddLayerToMap("Biosecurity");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.verifyLayerIsDisplayedInContentPane("Biosecurity");
        mapPage.clickOnZoomToLayer("Biosecurity");
        mapPage.verifyLayerIsDisplayedOnTheMap("Biosecurity");
        mapPage.logout();
    }
}
