package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_9b extends BaseTest {

    @Test(description = "UGIS-2895 Verify that system can automatically pan/zoom to searched criteria and Use can manually zoom in/out and pan using Portal")
    @Story("BRQ 9b")
    public void UGIS_2895_Validate_system_automatically_pan_zoom_to_searched_criteria_Portal() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        /*headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        //contentPage.myOrganization.selectFilterCategory("Network and Energy");
        contentPage.searchAndSelectMap("Test User Test Map"); //Test Map 82
        contentPage.webMapPage.clickOnOpenInMapViewer();
        mapPage.closeDetails();*/

        headerPage.clickOnMapTab();

        mapPage.findPlaceOnMap("Cairns");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Cairns");
        mapPage.cleanDirectory();
        mapPage.takeScreenshot("Expected");

        mapPage.findPlaceOnMap("Brisbane");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Brisbane");

        String currentZoomValue = mapPage.getMapZoomData();
        mapPage.clickOnZoomOut(2);
        mapPage.verifyThatMapZoomedOutBy(2, currentZoomValue);

        mapPage.pan();
        currentZoomValue = mapPage.getMapZoomData();
        //mapPage.clickOnZoomIn(2);
        /*mapPage.verifyThatMapZoomedInBy(2, currentZoomValue);
        mapPage.pan2();
        mapPage.clickOnZoomIn(1);
        mapPage.pan3();
        mapPage.clickOnZoomIn(2);*/
        mapPage.takeScreenshot("Actual");

        mapPage.compareImagesWithDiff("Expected", "Actual", 60000);

        currentZoomValue = mapPage.getMapZoomData();
        mapPage.clickOnZoomOut(3);
        mapPage.verifyThatMapZoomedOutBy(3, currentZoomValue);

        //mapPage.clickOnStickPin();
        //mapPage.clickOnZoomOut(3);
        //mapPage.findPlaceOnMap("Cairns");
        //currentZoomValue = mapPage.getMapZoomData();
        //mapPage.clickOnZoomIn(2);
        //mapPage.verifyThatMapZoomedInBy(2, currentZoomValue);
        mapPage.logout();
    }
}
