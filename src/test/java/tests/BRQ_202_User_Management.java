package tests;

import com.relevantcodes.extentreports.ExtentTest;
import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.Test;
import pages.HeaderPage;
import pages.OrganizationPage;
import pages.ProfilePage;
import utils.API.APIUtils;
import utils.DataProvider.MapDataProviderClass;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class BRQ_202_User_Management extends BaseTest {

    @Test(testName="UGIS-2042", description = "UGIS_2042 Verify that Admin User is able to add two new members from a csv file")
    public void UGIS_2042_Validate_Administrator_can_add_new_users_test() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on New members from a file button", test);
        organizationPage.clickOnNewMembersFromFileBtn();

        // reportLog("Upload File", test);
        organizationPage.uploadFile("Valid Data");

        //  reportLog("Click on Add file button", test);
        organizationPage.clickOnAddFile();

        List<String> username = organizationPage.getUserNames();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();

        organizationPage.clickOnGroupsBtn();
        organizationPage.searchAndSelectGroup("Network Data");
        organizationPage.searchAndSelectGroup("Network Data Viewer");
        organizationPage.searchAndSelectGroup("Non Network Data");
        organizationPage.searchAndSelectGroup("Environment & Cultural Heritage Editor");
        organizationPage.searchAndSelectGroup("Featured Maps and Apps");

        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        // reportLog("Verify success message", test);
        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 2");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(username.get(0));
        organizationPage.selectFirstMemberSearchResult();
        organizationPage.openUserProfile();
        //organizationPage.selectMemberOption("View groups");
        //organizationPage.getSelectedUserGroups();
        headerPage.logout();
        username.stream().forEach(user -> APIUtils.deleteUser(user));
    }

    @Test(description = "UGIS_2045 Verify that an error 'invalid type' is displayed when an Administrator attempts to add a new user with an invalid user type")
    public void UGIS_2045_Validate_Error_InvalidType_Administrator_add_new_users_test() {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on New members from a file button", test);
        organizationPage.clickOnNewMembersFromFileBtn();

        // reportLog("Upload File", test);
        organizationPage.uploadFile("Invalid User");

        //  reportLog("Click on Add file button", test);
        organizationPage.verifyErrorMessage("Some of the required fields in this file are invalid. Choose a different file or click next to fix them.");
        organizationPage.verifyErrorField("User Type");
        organizationPage.closeAddMembersPage();
        headerPage.logout();
    }

    @Test(description = "UGIS-2056 Validate Administrator can modify a user profile - First Name", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2056_Validate_Administrator_can_modify_user_profile_FirstName(Map<String, String> input) {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.selectMemberOption("View Profile");
        profilePage.clickOnEditProfile();
        profilePage.updateFirstName(input.get("New First Name"));
        profilePage.clickOnSaveBtn();
        profilePage.verifyThatFirstNameIsUpdated(input.get("New First Name"));

        headerPage.logout();
    }

    @Test(description = "UGIS-2058 Validate Administrator can modify a user profile - Last Name", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2058_Validate_Administrator_can_modify_user_profile_LastName(Map<String, String> input) {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.selectMemberOption("View Profile");
        profilePage.clickOnEditProfile();
        profilePage.updateLastName(input.get("New Last Name"));
        profilePage.clickOnSaveBtn();
        profilePage.verifyThatLastNameIsUpdated(input.get("New Last Name"));

        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }

    @Test(description = "UGIS-2069 Validate Administrator and can delete a user profile", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2069_Validate_Administrator_can_delete_user_profile(Map<String, String> input) {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.selectMemberOption("Delete Member");
        organizationPage.clickOnDeleteMemberBtn();
        organizationPage.verifyThatSuccessMsgIsDisplayed("The member '" + selectedUserName + "' has been deleted.");

        headerPage.logout();
    }

    @Test(description = "UGIS-2074 Validate Administrator can update a user type viewer to a creator user type")
    public void UGIS_2074_Validate_Administrator_can_update_userType_viewer_to_creator() {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnNewMembersFromFileBtn();

        // reportLog("Upload File", test);
        organizationPage.uploadFile("Valid Data_Viewer");

        //  reportLog("Click on Add file button", test);
        organizationPage.clickOnAddFile();
        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.selectMemberOption("Manage user type");
        organizationPage.updateUserType("Creator");
        organizationPage.verifyThatSuccessMsgIsDisplayed("User type and role updated successfully");
        organizationPage.selectMemberOption("View profile");
        profilePage.verifyUserType("Creator");

        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }

    @Test(description = "UGIS-2055 Validate Administrator can modify a user profile - role")
    public void UGIS_2055_Validate_Administrator_can_modify_user_profile_role() {
        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnNewMembersFromFileBtn();

        // reportLog("Upload File", test);
        organizationPage.uploadFile("Valid Data_Viewer");

        //  reportLog("Click on Add file button", test);
        organizationPage.clickOnAddFile();
        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);

        organizationPage.updateUserRole("Publisher"); //EQL
        organizationPage.selectMemberOption("View profile");
        profilePage.verifyUserRole("Publisher"); //EQL

        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }

    @Test(description = "UGIS-2075 Verify that default role is EQL User when the upload file contains an invalid role")
    public void UGIS_2075_validate_default_role_is_EQLUser_when_upload_file_contains_invalid_role() {

        HeaderPage headerPage = new HeaderPage();
        SoftAssertions softly = new SoftAssertions();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on New members from a file button", test);
        organizationPage.clickOnNewMembersFromFileBtn();

        // reportLog("Upload File", test);
        organizationPage.uploadFile("Invalid Role");

        //  reportLog("Click on Add file button", test);
        organizationPage.clickOnAddFile();

        List<String> usernames = organizationPage.getUserNames();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();

        organizationPage.clickOnGroupsBtn();
        organizationPage.searchAndSelectGroup("Enterprise_Test");

        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        // reportLog("Verify success message", test);
        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 2");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(usernames.get(0));
        organizationPage.selectFirstMemberSearchResult();
        organizationPage.openUserProfile();
        profilePage.verifyDefaultUserRole(softly, profilePage.getExpectedUserRole());

        organizationPage.clickOnOrgTab();
        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(usernames.get(1));
        organizationPage.selectFirstMemberSearchResult();
        organizationPage.openUserProfile();
        profilePage.verifyDefaultUserRole(softly, profilePage.getExpectedUserRole());

        headerPage.logout();
        usernames.stream().forEach(user -> APIUtils.deleteUser(user));
        softly.assertAll();
    }

    @Test(description = "UGIS-2044 Verify that Non Administrator role can not add new users", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2044_Validate_Non_Administrator_can_NOT_add_new_users(Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseBuiltInMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        organizationPage.inputPassword(input.get("Password"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");
        headerPage.logout();

        headerPage.login(selectedUserName, input.get("Password"));
        headerPage.inputSecurityAnswer(input.get("Security Answer"));
        headerPage.clickOnOrgTab();
        organizationPage.verifyThatAddMembersLinkNotDisplayed();
        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }

    @Test(description = "UGIS-2059 Verify that a User can modify their own user profile - First Name", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2059_Validate_User_can_modify_their_own_profile_FirstName(Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseBuiltInMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        organizationPage.inputPassword(input.get("Password"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");
        headerPage.logout();

        headerPage.login(selectedUserName, input.get("Password"));
        headerPage.inputSecurityAnswer(input.get("Security Answer"));
        headerPage.clickOnUserProfileIcon();
        headerPage.clickOnMyProfile();
        profilePage.clickOnEditProfile();
        profilePage.updateFirstName(input.get("New First Name"));
        profilePage.clickOnSaveBtn();
        profilePage.verifyThatFirstNameIsUpdated(input.get("New First Name"));
        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }

    @Test(description = "UGIS-2063 Verify that a User can modify their own user profile - Last Name", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void UGIS_2063_Validate_User_can_modify_their_own_profile_LastName(Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        ProfilePage profilePage = new ProfilePage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Members from a file 2");

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseBuiltInMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        organizationPage.inputPassword(input.get("Password"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");
        headerPage.logout();

        headerPage.login(selectedUserName, input.get("Password"));
        headerPage.inputSecurityAnswer(input.get("Security Answer"));
        headerPage.clickOnUserProfileIcon();
        headerPage.clickOnMyProfile();
        profilePage.clickOnEditProfile();
        profilePage.updateLastName(input.get("New Last Name"));
        profilePage.clickOnSaveBtn();
        profilePage.verifyThatLastNameIsUpdated(input.get("New Last Name"));
        headerPage.logout();
        APIUtils.deleteUser(selectedUserName);
    }
}
