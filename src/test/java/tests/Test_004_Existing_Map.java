package tests;

import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class Test_004_Existing_Map extends BaseTest {

    @Test(description = "Verify that Admin User is able to Zoom In and Zoom Out a map")
    public void verify_that_user_is_able_to_zoom_in_zoom_out() throws InterruptedException {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();
        //ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Create new map with layers and notes");

        // reportLog("Click on Organization Tab", test);
        headerPage.clickOnContentTab();
        contentPage.clickOnMyOrganizationTab();
        //contentPage.myOrganization.selectFilterCategory("Network and Energy");
        contentPage.searchAndSelectMap("QldBase_QgovSispUsers"); //Test Map 82
        contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnShowMapContentBtn();
        //mapPage.clickOnMoreOptionsForLayer("QLD Bushfire Current Incidents");
        //mapPage.clickOnTransparency();
        //mapPage.setSlidebar();
       /* mapPage.selectLayers();
        mapPage.deselectLayers();*/
        mapPage.findPlaceOnMap("Cairns");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Cairns");
        mapPage.cleanDirectory();
        mapPage.takeScreenshot("Expected");
        mapPage.findPlaceOnMap("Brisbane");
        mapPage.verifyThatSearchedPlaceIsDisplayed("Brisbane");
        //String currentZoomValue = mapPage.getMapZoomData();
        mapPage.clickOnZoomOut(2);
        //mapPage.verifyThatMapZoomedOutBy(2, currentZoomValue);
        mapPage.pan();
        mapPage.clickOnZoomIn(4);
        mapPage.pan2();
        mapPage.pan3();
        mapPage.takeScreenshot("Actual");
        mapPage.compareImage();
        mapPage.clickOnZoomOut(3);
        //mapPage.clickOnStickPin();
        //mapPage.clickOnZoomOut(3);
        //mapPage.findPlaceOnMap("Cairns");
        //currentZoomValue = mapPage.getMapZoomData();
        //mapPage.clickOnZoomIn(2);
        //mapPage.verifyThatMapZoomedInBy(2, currentZoomValue);
        mapPage.logout();
    }
}
