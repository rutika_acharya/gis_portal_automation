package tests.smoke;
import org.testng.annotations.Test;
import pages.*;
import tests.BaseTest;
import utils.LoadProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class WebApp_Smoke_Test extends BaseTest {

    @Test(description = "Access Environmental Restricted Web App – Successfully access the Environmental Restricted Web App through ArcGIS Portal ")
    public void accessing_Environmental_Restricted_Web_App_test () {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();
        //webAppPage.clickOnPopupOkBtn();
        /**
         * Testing if layers are present on the map
         */
        webAppPage.clickOnZoomIn();
        webAppPage.clickOnLayerListTab();
        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Electric Network");
        webAppPage.verifyThatLayerIsDisplayedOnTheMap("Base_Network");

        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("DCDB");
        webAppPage.verifyThatLayerIsDisplayedOnTheMap("LandParcelPropertyFramework");

        webAppPage.verifyThatLayerIsDisplayedOnTheWidgetPane("Managed Environment");
        webAppPage.verifyThatLayerIsDisplayedOnTheMap("Managed_Environment");
    }

    @Test
    public void environmental_Restricted_Web_App_bookmarks_test(){

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Turn all layers off first
         */
        webAppPage.clickOnLayerListTab();
        webAppPage.turnOffAllLayersOfTheWebApp();

        /**
         * Testing bookmark
         */
        webAppPage.clickOnBookmarkTab();
        List<String> bookmarks = new ArrayList<>();
        bookmarks.add("Mackay");
        bookmarks.add("Cairns");
        bookmarks.add("Barron Gorge");
        webAppPage.verifyTheBookmarksArePresent(bookmarks);
        webAppPage.cleanScreenshotDirectory();
        webAppPage.selectBookMark("Mackay");
        webAppPage.takeScreenshot("Mackay Actual");
        webAppPage.compareImage("Mackay", "Mackay Actual");
    }

    @Test
    public void environmental_Restricted_Web_App_measurement_test() {

        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Testing measurement
         */
        webAppPage.clickOnMeasurementTab();
        webAppPage.selectTypeOfMeasuringToPerform("Distance");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Kilometers");
        webAppPage.drawLineToMeasureDistance();
        String measurementKm = webAppPage.getMeasurementResultValue().replace(" Kilometers", "").replace("," , "");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Meters");
        String measurementM = webAppPage.getMeasurementResultValue().replace(" Meters", "").replace("," , "");
        webAppPage.verifyMeasurementKmToM(measurementKm, measurementM);

        webAppPage.selectTypeOfMeasuringToPerform("Location");
        webAppPage.drawPointOnTheMap();
        webAppPage.verifyLatitudeAndLongitudeOfThePoint();

        webAppPage.selectTypeOfMeasuringToPerform("Area");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Sq Kilometers");
        webAppPage.drawAreaToMeasureDistance();
        measurementKm = webAppPage.getMeasurementResultValue().replace(" Sq Kilometers", "").replace("," , "");
        webAppPage.clickOnMeasurementDropDwn();
        webAppPage.selectMeasurementUnit("Sq Meters");
        measurementM = webAppPage.getMeasurementResultValue().replace(" Sq Meters", "").replace("," , "");
        webAppPage.verifyMeasurementSqKmToM(measurementKm, measurementM);
    }

    @Test
    public void environmental_Restricted_Web_App_search_test(){
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        WebAppPage webAppPage = new WebAppPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web App");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web App");
        webAppPage.clickOnViewApplication();
        webAppPage.verifyThatWebAppIsOpenedInNewWindow();
        webAppPage.verifyTheTitleOfWebApp("Environment and Cultural Heritage Web App");

        //webAppPage.clickOnPopupOkBtn();
        //webAppPage.clickOnSplashScreenOkBtn();

        /**
         * Testing default search in options
         */
        webAppPage.clickOnSearchInDrpDwn();
        List<String> expectedSearchInOptions = new ArrayList<>();

        /**
         * Sandpit
         */
        /*
        expectedSearchInOptions.add("All");
        expectedSearchInOptions.add("Land parcel");
        expectedSearchInOptions.add("Suburb Localities");
        expectedSearchInOptions.add("Property Address");
        expectedSearchInOptions.add("Esri Geocoder");
        expectedSearchInOptions.add("Queensland Homestead");*/

        /**
         * SIT UGIS-6127
         */
        expectedSearchInOptions.add("All");
        expectedSearchInOptions.add("QLD Lot Plan");
        expectedSearchInOptions.add("NSW Lot Plan");
        expectedSearchInOptions.add("Property Address");
        expectedSearchInOptions.add("Suburb");
        expectedSearchInOptions.add("Esri Geocoder");
        expectedSearchInOptions.add("Queensland Homestead");
        webAppPage.verifyTheBuiltInSearchOptions(expectedSearchInOptions);


        /**
         * Testing searching an address and verifying address popup
         */
        webAppPage.selectSearchInOption("Esri Geocoder");
        webAppPage.searchOnMap2("Shute Harbour");
        webAppPage.closeSearchPopup();

        webAppPage.clickOnHomeBtn();

        webAppPage.clickOnSearchInDrpDwn();
        webAppPage.selectSearchInOption("QLd Lot Plan");
        webAppPage.searchOnMap2("486RP905763");
        webAppPage.verifyThatAddressPopupIsDisplayed();
        webAppPage.closeSearchPopup();

        webAppPage.clickOnHomeBtn();

        webAppPage.clickOnSearchInDrpDwn();
        webAppPage.selectSearchInOption("Suburb");
        webAppPage.searchOnMap2("Karana Downs");
        webAppPage.closeSearchPopup();

        webAppPage.clickOnHomeBtn();

        webAppPage.clickOnSearchInDrpDwn();
        webAppPage.selectSearchInOption("Property Address");
        webAppPage.searchOnMap2("18 Yarraman Street");
        webAppPage.closeSearchPopup();

        webAppPage.clickOnHomeBtn();

        webAppPage.clickOnSearchInDrpDwn();
        webAppPage.selectSearchInOption("Queensland Homestead");
        webAppPage.searchOnMap2("Breedon");
        webAppPage.closeSearchPopup();

    }

    @Test
    public void environmental_Restricted_Web_Maps_test() {
        HeaderPage headerPage = new HeaderPage();
        GroupsPage groupsPage = new GroupsPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.corporateLogin(properties.getProperty("corp_login_id"));
        headerPage.verifyUsername(properties.getProperty("corp_login_username"));
        //headerPage.login();
        headerPage.clickOnGroupsTab();
        //groupsPage.clickOnMyOrgGroupsTab();
        groupsPage.searchForGroupOrContent("Env");
        groupsPage.selectGroup("Environment and Cultural Heritage - Restricted");
        groupsPage.clickOnContentTab();
        groupsPage.searchForGroupOrContent("ENV_CH Restricted Data Web Map");
        groupsPage.selectGroupContent("ENV_CH Restricted Data Web Map");

        contentPage.webMapPage.clickAndOpenLayerFromList("Electric Network");
        contentPage.webMapPage.verifyThatLayerIsOpenedInNewWindow();
        contentPage.webMapPage.verifyTitleOfTheLayerOpenedInNewWindow("Base Network");
        contentPage.webMapPage.verifyThatLayerIsPointingToCorrectEnv(properties.getProperty("environment"));
        contentPage.webMapPage.closeTheLayerTab();

        contentPage.webMapPage.clickAndOpenLayerFromList("Managed Environment");
        contentPage.webMapPage.verifyThatLayerIsOpenedInNewWindow();
        contentPage.webMapPage.verifyTitleOfTheLayerOpenedInNewWindow("Managed Environment");
        contentPage.webMapPage.verifyThatLayerIsPointingToCorrectEnv(properties.getProperty("environment"));
        contentPage.webMapPage.closeTheLayerTab();

        contentPage.webMapPage.clickAndOpenLayerFromList("Powerlink");
        contentPage.webMapPage.verifyThatLayerIsOpenedInNewWindow();
        contentPage.webMapPage.verifyTitleOfTheLayerOpenedInNewWindow("Powerlink");
        contentPage.webMapPage.verifyThatLayerIsPointingToCorrectEnv(properties.getProperty("environment"));
        contentPage.webMapPage.closeTheLayerTab();

        contentPage.webMapPage.clickAndOpenLayerFromList("Native Title");
        contentPage.webMapPage.verifyThatLayerIsOpenedInNewWindow();
        //contentPage.webMapPage.verifyTitleOfTheLayerOpenedInNewWindow("NNTT_Custodial_AGOL");
        contentPage.webMapPage.verifyThatNativeTitleLayerIsPointingToArcGISOnlineService();
        contentPage.webMapPage.closeTheLayerTab();

        contentPage.webMapPage.clickOnOpenInMapViewer();
        //mapPage.clickOnErrorPopupOkBtn();
        mapPage.clickOnShowMapContentBtn();
        mapPage.waitUntilIndigenousLayerIsDisplayed();
        mapPage.selectLayers();
        mapPage.verifyLayerIsDisplayedInContentPane("Indigenous Land Use Agreements");
        //mapPage.selectLayer("Indigenous Land Use Agreements");
        //mapPage.selectLayer("Electric Network");
        mapPage.clickOnMapToShowPopup();
        mapPage.verifyThatPopupIsPresentOnMap();
    }
}
