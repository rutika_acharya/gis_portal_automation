package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_122 extends BaseTest {

    @Test(description = "Verify that An ArcGIS user can successfully add QLD and NSW Cadastre services to a new map using ArcGIS Portal.")
    @Story("BRQ 122")
    public void  successfully_add_QLD_and_NSW_Cadastre_services_to_a_new_map_using_ArcGIS_Portal() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnMapTab();

        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectOptionToSearchLayersIn("My Organization");
        mapPage.inputLayerNameInSearchBox("Managed Cadastre");
        mapPage.selectAndAddFeatureLayerToMap("Managed Cadastre");

        mapPage.inputLayerNameInSearchBox("NSW Cadastre");
        mapPage.selectAndAddLayerToMap("NSW Cadastre");

        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();

        mapPage.findPlaceOnMap("Jubilee Park, Alfred St, Mackay");
        mapPage.verifyVisibilityIsInRangeForLayer("Easement Parcel");
        mapPage.verifyVisibilityIsInRangeForLayer("Strata Parcel");
        mapPage.verifyVisibilityIsInRangeForLayer("Volumetric Parcel");
        mapPage.verifyVisibilityIsInRangeForLayer("Roads and Waterways");
        mapPage.verifyVisibilityIsInRangeForLayer("Lots");

        mapPage.expandLayerInContentPane("NSW Cadastre");
        mapPage.findPlaceOnMap("Sydney Observatory");
        mapPage.verifyVisibilityIsInRangeForSubLayer("PlanExtent");
        mapPage.verifyVisibilityIsInRangeForSubLayer("SectionExtent");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Lot");
        mapPage.verifyVisibilityIsInRangeForSubLayer("PlanExtent Labels");
        mapPage.verifyVisibilityIsInRangeForSubLayer("SectionExtent Labels");
        mapPage.verifyVisibilityIsInRangeForSubLayer("Lot Labels");

        mapPage.verifyVisibilityIsNotInRangeForSubLayer("Large Rural PlanExtent");
        mapPage.verifyVisibilityIsNotInRangeForSubLayer("Rural PlanExtent");
    }
}
