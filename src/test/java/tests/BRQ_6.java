package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.ContentPage;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_6 extends BaseTest {

    @Test(description = "An ArcGIS user can successfully create and modify a feature using Portal for ArcGIS.")
    @Story("BRQ 6")
    public void test_aa_create_and_modify_a_feature_layer() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.inputTitleOfTheLayer("Test Feature Layer Create");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer Create");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer Create");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");
        mapPage.clickOnEditBtn();
        mapPage.editAndChangeFieldValue("field value 2", "Test_Feature_Layer_Create");
        mapPage.closeMapNotePopup();
        mapPage.goToContentPage();
        contentPage.searchAndCheckCheckboxFor("Test Feature Layer Create");
        contentPage.clickOnDeleteBtn();
        contentPage.clickOnDeleteBtnInConfirmationPopup();
    }

    @Test(description = "An ArcGIS user can successfully create and delete a feature using Portal for ArcGIS.")
    @Story("BRQ 6")
    public void test_ab_create_and_delete_a_feature_layer() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();

        contentPage.inputTitleOfTheLayer("Test Feature Layer Delete");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer Delete");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        /*contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        contentPage.webMapPage.clickOnAddToNewMap();*/
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 100);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer Delete");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");

        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, -200);
        mapPage.closeMapNotePopup();

        mapPage.verifyNoOfFeaturesForAFeatureLayer(2, "Test_Feature_Layer_Delete");
        mapPage.deleteAFeatureForLayer("Test_Feature_Layer_Delete");
        mapPage.verifyNoOfFeaturesForAFeatureLayer(1, "Test_Feature_Layer_Delete");

        mapPage.goToContentPage();
        contentPage.searchAndCheckCheckboxFor("Test Feature Layer Delete");
        contentPage.clickOnDeleteBtn();
        contentPage.clickOnDeleteBtnInConfirmationPopup();
    }

    @Test(description = "An ArcGIS user can successfully select and locate a feature using Portal for ArcGIS.")
    @Story("BRQ 6")
    public void test_ac_select_and_locate_a_feature_layer() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        /**
         * Create a Feature Layer
         */

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();

        contentPage.inputTitleOfTheLayer("Test Feature Layer Locate2");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer Locate2");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        //contentPage.webMapPage.clickOnOpenMenu();
        contentPage.sleep(2000);
        //contentPage.webMapPage.clickOnAddToNewMap();

        /**
         * Create a Feature
         */
        mapPage.clickOnEditBtn();
        mapPage.clickOnNewFeature();
        mapPage.drawPoint(100, 200);
        mapPage.closeMapNotePopup();
        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer Locate2");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuAddField();
        mapPage.inputFieldNameInAddNewFieldPopup("FieldName");
        mapPage.inputDisplayNameInAddNewFieldPopup("Field Display Name");
        mapPage.clickOnAddNewFieldBtn();
        mapPage.verifyThatSavingLayerPopupIsDisplayed();
        mapPage.verifyThatFieldNameIsDisplayedInAttributeTable("Field Display Name");
        mapPage.editAndAddInfoToTheField("field value 1");
        mapPage.verifyThatInfoIsAddedToTheField("Field Display Name", "field value 1");

        /**
         * To Locate a Feature
         */

        mapPage.clickOnDetails();
        mapPage.clickOnShowAttributeTableForLayer("Test Feature Layer Locate2");
        mapPage.clickOnFirstRowOfAttributeTable();
        mapPage.verifyThatFeatureIsSelected("Test Feature Layer Locate2");
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuCenterOnSelection();
        mapPage.clickOnAFeatureForALayer("Test_Feature_Layer_Locate2");
        mapPage.verifyThatPopupIsPresentOnMap();
        mapPage.clickOnOption();
        mapPage.clickOnOptionMenuClearSelection();
        mapPage.verifyThatFeatureIsNotSelected("Test Feature Layer Locate2");

        mapPage.goToContentPage();
        contentPage.searchAndCheckCheckboxFor("Test Feature Layer Locate2");
        contentPage.clickOnDeleteBtn();
        contentPage.clickOnDeleteBtnInConfirmationPopup();
    }

    @Test(description = "An ArcGIS user can successfully create and save a feature using Portal for ArcGIS.")
    @Story("BRQ 6")
    public void test_ad_create_and_save_a_feature_layer() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        ContentPage contentPage = new ContentPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        /**
         * Create a Feature Layer
         */

        headerPage.clickOnContentTab();
        contentPage.clickOnCreate();
        contentPage.selectOptionToCreate("Feature Layer");
        contentPage.clickOnCreateFromTemplateTab();
        contentPage.selectOptionToCreateFromTemplate("Build a layer");
        contentPage.selectTemplate("Points");
        contentPage.clickOnCreateBtnInPopup();
        contentPage.clickOnNextBtnInPopup();
        contentPage.clickOnNextBtnInPopup();

        contentPage.inputTitleOfTheLayer("Test Feature Layer Save");
        contentPage.inputTagsOfTheLayer("#test");
        contentPage.clickOnDoneBtnInPopup();
        contentPage.webMapPage.verifyThatOverviewTabIsOpened();
        headerPage.clickOnMapTab();
        mapPage.clickOnAddBtn();
        mapPage.clickOnSearchForLayers();
        mapPage.selectAndAddLayerToMap("Test Feature Layer Save");
        mapPage.clickOnDetails();
        mapPage.clickOnShowMapContentBtn();
        mapPage.clickOnSaveLayer("Test Feature Layer Save");
        mapPage.verifyThatLayerIsSaved("Test Feature Layer Save");

        mapPage.goToContentPage();
        contentPage.searchAndCheckCheckboxFor("Test Feature Layer Save");
        contentPage.clickOnDeleteBtn();
        contentPage.clickOnDeleteBtnInConfirmationPopup();
    }
}
