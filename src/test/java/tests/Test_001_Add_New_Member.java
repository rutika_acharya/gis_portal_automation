package tests;

import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.Test;
import pages.HeaderPage;
import pages.OrganizationPage;
import utils.API.APIUtils;
import utils.DataProvider.MapDataProviderClass;
import utils.ExtentReports.ExtentTestManager;
import utils.LoadProperties;

import java.util.Map;
import java.util.Properties;

//import org.testng.annotations.Test;

public class Test_001_Add_New_Member extends BaseTest {

    //HomePage homePage;
    //OrganizationPage organizationPage;

    @Test(description = "Verify that Admin User is able to add new member", dataProvider = "dp", dataProviderClass = MapDataProviderClass.class)
    public void test(Map<String, String> input) {

        HeaderPage headerPage = new HeaderPage();
        OrganizationPage organizationPage = new OrganizationPage();
        Properties properties = LoadProperties.getProperties();
        ExtentTest test = ExtentTestManager.startTest(this.getClass().getSimpleName(), "Add New Member.");

        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        reportLog("Click on Organization Tab", test);
        headerPage.clickOnOrgTab();

        //reportLog("Click on Add Members", test);
        organizationPage.clickOnAddMembers();

        //reportLog("Choose SAML method", test);
        organizationPage.chooseSAMLMethod();

        // reportLog("Click on Next button", test);
        organizationPage.clickOnNextBtn();

        // reportLog("Click on Add Members button", test);
        organizationPage.clickOnAddMemberBtn();

        // reportLog("Input First Name", test);
        organizationPage.inputFirstName(input.get("First Name"));

        // reportLog("Input Last Name", test);
        organizationPage.inputLastName(input.get("Last Name"));

        //  reportLog("Input Email", test);
        organizationPage.inputEmail(input.get("Email"));

        // reportLog("Input Username", test);
        organizationPage.inputUsername(input.get("Username"));

        // reportLog("Click on Add button", test);
        organizationPage.clickOnAddBtn();

        String selectedUserName = organizationPage.getUserName();

        // reportLog("Click Next", test);
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();
        organizationPage.clickOnNextBtn();

        organizationPage.verifyThatSuccessMsgIsDisplayed("Members added: 1");

        organizationPage.clickOnMembersTab();
        organizationPage.selectSearchByUsername();
        organizationPage.inputSearchText(selectedUserName);
        organizationPage.selectFirstMemberSearchResult();
        headerPage.logout();

        APIUtils.deleteUser(selectedUserName);
        //homePage.closeBrowser();
    }
}
