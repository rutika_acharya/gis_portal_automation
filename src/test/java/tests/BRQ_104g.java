package tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import pages.HeaderPage;
import pages.MapPage;
import utils.LoadProperties;

import java.util.Properties;

public class BRQ_104g extends BaseTest {

    @Test(description = "UGIS-2908 Validate user is able to measure straight lines, paths, and areas in Portal")
    @Story("BRQ 104g")
    public void UGIS_2908_Validate_user_is_able_to_measure_straight_lines_paths_and_areas_in_Portal() {

        HeaderPage headerPage = new HeaderPage();
        MapPage mapPage = new MapPage();
        Properties properties = LoadProperties.getProperties();
        headerPage.openPage(properties.getProperty(properties.getProperty("environment") + ".app.link"));
        headerPage.login();

        headerPage.clickOnMapTab();
        mapPage.clickOnBaseMapBtn();
        mapPage.selectBaseMap("Dark Gray Canvas");

        mapPage.clickOnAddBtn();

        mapPage.clickOnBrowseLivingAtlasLayers();

        mapPage.selectAndAddLayerToMap("World Street Map");

        mapPage.clickOnMeasureBtn();
        mapPage.selectTypeOfMeasuringToPerform("Distance");
        mapPage.clickOnMeasurementDropDwn();
        mapPage.selectMeasurementUnit("Kilometers");
        mapPage.drawLineToMeasureDistance();
        String measurementKm = mapPage.getMeasurementResultValue().replace(" Kilometers", "").replace("," , "");
        mapPage.clickOnMeasurementDropDwn();
        mapPage.selectMeasurementUnit("Meters");
        String measurementM = mapPage.getMeasurementResultValue().replace(" Meters", "").replace("," , "");
        mapPage.verifyMeasurementKmToM(measurementKm, measurementM);

        mapPage.selectTypeOfMeasuringToPerform("Location");
        mapPage.drawPointToMeasureLocation();
        mapPage.verifyLatitudeAndLongitudeOfThePoint();

        mapPage.selectTypeOfMeasuringToPerform("Area");
        mapPage.clickOnMeasurementDropDwn();
        mapPage.selectMeasurementUnit("Sq Kilometers");
        mapPage.drawAreaToMeasureDistance();
        measurementKm = mapPage.getMeasurementResultValue().replace(" Sq Kilometers", "").replace("," , "");
        mapPage.clickOnMeasurementDropDwn();
        mapPage.selectMeasurementUnit("Sq Meters");
        measurementM = mapPage.getMeasurementResultValue().replace(" Sq Meters", "").replace("," , "");
        mapPage.verifyMeasurementSqKmToM(measurementKm, measurementM);
    }
}
