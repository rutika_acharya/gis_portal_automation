package utils.API;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import utils.LoadProperties;

import java.util.Optional;
import java.util.Properties;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;

public class APIUtils {

    public static final Properties properties = LoadProperties.getProperties();
    public static final String environment = properties.getProperty("environment");
    public static final String base_URI = "https://" + environment + ".gis.energyqonline.com.au/arcgis/sharing/rest";

    public static String getToken() {
        RestAssured.useRelaxedHTTPSValidation();
        Response response = given()
                .contentType("multipart/form-data")
                .multiPart("username", properties.getProperty(properties.getProperty("environment") + "_"+ "admin_username"))
                .multiPart("password", properties.getProperty(properties.getProperty("environment") + "_"+ "admin_password"))
                .multiPart("f", "json")
                .multiPart("referer", "https")
                .when()
                .post(base_URI + "/generateToken");
        String token = response.path("token");
        System.out.println(token);
        return token;
    }

    public static void deleteUser(String username) {
        RestAssured.useRelaxedHTTPSValidation();
        Response response3 = given()
                //.contentType("multipart/form-data")
                .multiPart("token", getToken())
                .multiPart("f", "json")
                .when()
                .post(base_URI + "/community/users/" + username + "/delete");
        System.out.println(response3.asString());
    }

    public static void main(String args[]){
       /* //deleteUser("rutikaacharya9");
        Stream.iterate(1, x->++x).limit(5).map(x -> "" + x).collect(Collectors.joining());
      *//*  Integer intVar1 = null;
        Integer intVar2 = new Integer(9);
        Optional var1= Optional.ofNullable(intVar1);
        Optional var2 = Optional.ofNullable(intVar2);
        System.out.println(var1.isPresent() +","+ var2.isPresent());*//*

        //IntStream.rangeClosed(-1, -5).forEach(System.out::println);
        IntStream.range(1, 5).forEach(System.out::println);*/

        RestAssured.useRelaxedHTTPSValidation();
        Response response3 = given()
                //.contentType("multipart/form-data")
                .multiPart("token", getToken())
                .multiPart("f", "json")
                .when()
                .post(base_URI + "/content/items/3dc53164400c4ad78df2f0924e0d911d/data");
        System.out.println(response3.asString());
    }

}
