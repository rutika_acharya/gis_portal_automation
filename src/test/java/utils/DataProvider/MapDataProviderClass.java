package utils.DataProvider;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDataProviderClass {
    private static final String FILE_PATH = "src/test/resources/data/";
    private static final DataFormatter dataFormatter = new DataFormatter();
    private static final String SHEET_NAME = "Sheet1";

    @DataProvider(name = "dp")
    public Object[][] getData(Method test) throws IOException, InvalidFormatException {
        System.out.println("!!!!!!!!!!!!!!!!!!!! " + test.getDeclaringClass().getSimpleName() + " !!!!!!!!!!!!!!!!!!!!");
        String FILE = FILE_PATH + test.getDeclaringClass().getSimpleName() + "_" + test.getName() + ".xlsx";
        Workbook workbook = WorkbookFactory.create(new File(FILE));
        Sheet sheet = workbook.getSheet(SHEET_NAME);
        Iterable<Row> rows = sheet::rowIterator;
        List<Map<String, String>> results = new ArrayList<>();
        boolean header = true;
        List<String> keys = null;
        for (Row row : rows) {
            List<String> values = getValuesInEachRow(row);
            if (header) {
                header = false;
                keys = values;
                continue;
            }
            results.add(transform(keys, values));
        }
        return asTwoDimensionalArray(results);
    }

    private static Object[][] asTwoDimensionalArray(List<Map<String, String>> interimResults) {
        Object[][] results = new Object[interimResults.size()][1];
        int index = 0;
        for (Map<String, String> interimResult : interimResults) {
            results[index++] = new Object[]{interimResult};
        }
        return results;
    }

    private static Map<String, String> transform(List<String> names, List<String> values) {
        Map<String, String> results = new HashMap<>();
        for (int i = 0; i < names.size(); i++) {
            String key = names.get(i);
            String value = values.get(i);
            results.put(key, value);
        }
        return results;
    }

    private static List<String> getValuesInEachRow(Row row) {
        List<String> data = new ArrayList<>();
        Iterable<Cell> columns = row::cellIterator;
        for (Cell column : columns) {
            data.add(dataFormatter.formatCellValue(column));
        }
        return data;
    }
}
