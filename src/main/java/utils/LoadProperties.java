package utils;

import java.io.IOException;
import java.util.Properties;

public class LoadProperties {

    public static Properties getProperties() {
        String fileName = "application.properties";
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try {
            properties.load(loader.getResourceAsStream(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

}
