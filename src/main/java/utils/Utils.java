package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Comparators;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;

public class Utils {

    public static int getRandomNumberInRange(int min, int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }

    public static double round (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static File getLatestFileInDirectory(String directoryPath) throws IOException {
        Path dir = Paths.get(directoryPath);  // specify your directory

        Optional<Path> lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
                .filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
                .max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

        if ( lastFilePath.isPresent() ) // your folder may be empty
        {
            // do your code here, lastFilePath contains all you need
            return new File(String.valueOf(lastFilePath));
        }
        else
            return null;
    }

    public static int getCountOfRecordsInTheLatestDownloadedFile(String filePath) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(filePath));
        try {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } finally {
            is.close();
        }
    }

    /*public static boolean isSortedAscending(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.<String> naturalOrder());
    }*/

    public static boolean isSortedAscendingIntegers(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.comparingInt(x -> Integer.parseInt((String) x)));
    }

    public static boolean isSortedDescendingIntegers(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.comparingInt(x -> Integer.parseInt((String) x)).reversed());
    }

    public static boolean isSortedAscending(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.naturalOrder());
    }

    public static boolean isSortedDescending(List<String> listOfStrings) {
        return Comparators.isInOrder(listOfStrings, Comparator.reverseOrder());
    }

    public static Map<String, Object> getMapFromJSONString(String json) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = null;
        try {
            map = mapper.readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static boolean doesFileExist(String filePath){

        // Get the file
        File f = new File(filePath);
        // Check if the specified file
        // Exists or not
        if (f.exists())
            return true;
        else
            return false;
    }

    public static void provideFileNameInUploadFileDialog(String filepath) throws AWTException {
        StringSelection ss = new StringSelection(filepath);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

        Robot robot = new Robot();

        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
    }

}
