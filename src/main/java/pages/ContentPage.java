package pages;

import io.qameta.allure.Step;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.components.ChangeStyleMenu;
import pages.components.MyOrganization;
import pages.components.WebMapPage;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ContentPage extends HeaderPage {

    public static final String BRQ_18_DATA_FILES_PATH = System.getProperty("user.dir") + "\\src\\test\\resources\\data\\BRQ_18_data\\";

    @FindBy(css = "a[href='#content']")
    WebElement myContentTab;

    @FindBy(css = "a[href='#favorites']")
    WebElement myFavouriteTab;

    @FindBy(css = "a[href='#groups']")
    WebElement myGroupsTab;

    @FindBy(css = "a[href='#organization']")
    WebElement myOrganizationTab;

    @FindBy(css = "a[href='#livingAtlas']")
    WebElement livingAtlasTab;

    @FindBy(css = "input[id*=dijit__TemplatedMixin_]")
    WebElement searchBox;

    @FindBy(xpath = "//span[contains(text(),'Add Item')]/..")
    WebElement addItemBtn;

    @FindBy(xpath = "//span[contains(text(),'Create')]/..")
    WebElement createBtn;

    @FindBy(css = ".dropdown-menu.js-create>button")
    List<WebElement> createOptions;

    @FindBy(css = "#fromTemplate")
    WebElement createFromTemplateTab;

    @FindBy(css = ".gallery .gallery-left li")
    List<WebElement> templateActions;

    @FindBy(id = "buildlayer")
    WebElement buildALayer;

    @FindBy(css = ".templates.gallery-right .grid-item")
    List<WebElement> availableTemplates;

    @FindBy(css = ".btn-main")
    WebElement createPopupCreateBtn;

    @FindBy(xpath = "//span[contains(text(),'Next')]")
    WebElement createPopupNxtBtn;

    @FindBy(css = "input[name='title']")
    WebElement createPopupTitleTxtBox;

    @FindBy(css = ".esriTags .dijitInputField input")
    WebElement inputTags;

    @FindBy(xpath = "//span[contains(text(),'Done')]")
    WebElement createPopupDoneBtn;

    @FindBy(css = "input#ogc")
    WebElement itemTypeOGC;

    @FindBy(css = "input[id=file]")
    WebElement chooseFileBtn;

    @FindBy(css = "input#itemUrl")
    WebElement itemURL;

    @FindBy(css = "input#title")
    WebElement itemTitle;

    @FindBy(css = "input[id*=filter-]")
    WebElement itemTags;

    @FindBy(css = "input#tableOnly")
    WebElement locateFeaturesByOnlyTableRadioBtn;

    @FindBy(id = "addItem-btn")
    WebElement addItemPopupBtn;

    @FindBy(css = ".js-add-category-dropdown-button")
    WebElement assignCategoryDrpDwn;

    @FindBy(css = ".category-picker-search-wrap input")
    WebElement categorySearchInput;

    @FindBy(css = ".content-table-view")
    WebElement contentTable;

    @FindBy(css = "input[aria-label='Select all']")
    WebElement selectAllContentChkBox;

    @FindBy(css = "button.js-delete")
    WebElement deleteBtn;

    @FindBy(css = "span[widgetid='button_delete-warning-submit'] .dijitButtonNode")
    WebElement deleteItemConfirmationBtn;

    //@FindBy(css = ".table-select-title a")
    @FindBy(css = ".table-select-title a")
    List<WebElement> mapList;

    @FindBy(css = ".table-select-row .table-select-cell .flex>a")
    List<WebElement> groupsPageMapList;

    @FindBy(css = ".js-filters section:not(.hide) .accordion-title")
    List<WebElement> filters;

    @FindBy(css = ".allow-categorization .js-empty-wrap")
    WebElement myContentNoItemsElem;

    public MyOrganization myOrganization;
    public WebMapPage webMapPage;

    public ContentPage() {
        PageFactory.initElements(driver, this);
        myOrganization = new MyOrganization(driver);
        webMapPage = new WebMapPage(driver);
    }

    @Step("Search and select the map")
    public void searchAndSelectMap(String map) {
        searchMapOnContentPage(map);
        driver.findElement(By.xpath("//a[text()='" + map + "']")).click();
        sleep(3000);
    }

    @Step("Search map {mapName} on content page")
    public void searchMapOnContentPage(String mapName){
        wait.until(ExpectedConditions.elementToBeClickable(searchBox));
        searchBox.clear();
        searchBox.sendKeys(mapName);
        sleep(5000);
    }

    public void selectFilter(String filterType, String filterValue) {
        WebElement itemType = filters.stream().filter(filterOption -> filterOption.getText().contains(filterType)).findFirst().get();
        if (itemType.getAttribute("aria-expanded") == null){
            System.out.println("Clicking on filter type");
            action.moveToElement(itemType.findElement(By.cssSelector(".icon-inline"))).click().perform();
        }
        if (filterValue.contains("->")){
            String[] values = filterValue.split("->");
            driver.findElement(By.cssSelector(".filter-tree-item[data-label='"+values[0]+"']>a")).click();
            sleep(500);
            driver.findElement(By.cssSelector(".filter-tree-item[data-label='"+values[1]+"']>a")).click();
        }
        else
            driver.findElement(By.cssSelector(".filter-tree-item[data-label='"+filterValue+"']>a")).click();
    }

    @Step("Search and check checkbox for the map {mapName}")
    public void searchAndCheckCheckboxFor(String mapName) {
        searchMapOnContentPage(mapName);
        List<WebElement> list = driver.findElements(By.xpath("//a[text()='" + mapName + "']/../../../..//input[@type='checkbox']"));
        list.stream().forEach(WebElement::click);
        sleep(3000);
    }

    @Step("Select all content on content page")
    public void selectAllContentOnContentPage(){
        selectAllContentChkBox.click();
    }

    public void deleteAllOfMyContent(){
        if(!contentTable.getAttribute("class").contains("hide")) {
            selectAllContentOnContentPage();
            clickOnDeleteBtn();
            clickOnDeleteBtnInConfirmationPopup();
        }
    }

    @Step("Click on delete button on content page")
    public void clickOnDeleteBtn(){
        sleep(100);
        deleteBtn.click();
    }

    @Step("Click on Delete button in delete confirmation popup")
    public void clickOnDeleteBtnInConfirmationPopup(){
        deleteItemConfirmationBtn.click();
    }

    @Step("Verify that feature layer is visible in the My Content Tab")
    public void verifyThatMapIsPresentOnContentPage(String mapName){
        searchMapOnContentPage(mapName);
        assertThat(mapList.stream().filter(map -> map.getText().equals(mapName)).count()).isEqualTo(1);
    }

    @Step("Verify that layer is visible in the My Content- My Groups Tab")
    public void verifyThatMapIsPresentOnContentPageMyGroupsTab(String mapName){
        sleep(500);
        assertThat(groupsPageMapList.stream().filter(map -> map.getText().equals(mapName)).count()).isEqualTo(1);
    }

    @Step("Click to open the map {mapName}")
    public void openMapOnMyGroupsTab(String mapName) {
        sleep(500);
        groupsPageMapList.stream().filter(map -> map.getText().equals(mapName)).findFirst().get().click();
    }

    @Step("Verify that feature layer is visible in the My Content Tab")
    public void verifyThatLayerIsPresentOnContentPage(String layerName, String expectedContentCount){
        searchMapOnContentPage(layerName);
        assertThat(mapList.stream().filter(map -> map.getText().equals(layerName)).count()).isEqualTo(Integer.parseInt(expectedContentCount));
    }

    @Step("Search and delete map {map}")
    public void searchAndDeleteMapOnContentPage(String map){
        sleep(8000);
        searchAndCheckCheckboxFor(map);
        clickOnDeleteBtn();
        clickOnDeleteBtnInConfirmationPopup();
        sleep(1000);
    }

    @Step("Click on My Content")
    public void clickOnMyContentTab() {
        myContentTab.click();
    }

    @Step("Click on My Favorites")
    public void clickOnMyFavoritesTab() {
        myFavouriteTab.click();
    }

    @Step("Click on My Groups")
    public void clickOnMyGroupsTab() {
        myGroupsTab.click();
    }

    @Step("Click on My Organization")
    public void clickOnMyOrganizationTab() {
        wait.until(ExpectedConditions.elementToBeClickable(myOrganizationTab));
        myOrganizationTab.click();
        sleep(1000);
    }

    @Step("Click on Living Atlas")
    public void clickOnLivingAtlasTab() {
        livingAtlasTab.click();
    }

    @Step("Click on Add Item")
    public void clickOnAddItem() {
        addItemBtn.click();
    }

    @Step("Click on Create")
    public void clickOnCreate() {
        createBtn.click();
    }

    @Step("Select option {optionToSelect}")
    public void selectOptionToCreate(String optionToSelect) {
        createOptions.stream().filter(option -> option.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
    }

    @Step("Click on Create From Template tab")
    public void clickOnCreateFromTemplateTab() {
        createFromTemplateTab.click();
    }

    @Step("Select option {optionToSelect} to create from template")
    public void selectOptionToCreateFromTemplate(String optionToSelect) {
        templateActions.stream().filter(option -> option.getText().equalsIgnoreCase(optionToSelect)).findFirst().get().click();
        sleep(1000);
    }

    @Step("Select template {templateToSelect}")
    public void selectTemplate(String templateToSelect) {
        availableTemplates.stream().filter(option -> option.getText().equalsIgnoreCase(templateToSelect)).findFirst().get().click();
    }

    public void selectOptionToAddItem(String option) {
        driver.findElement(By.cssSelector("button[data-title='" + option + "']")).click();
    }

    @Step("Click on Create button")
    public void clickOnCreateBtnInPopup(){
        createPopupCreateBtn.click();
        sleep(2000);
    }

    @Step("Click on Next button")
    public void clickOnNextBtnInPopup(){
        createPopupNxtBtn.click();
        sleep(2000);
    }

    @Step("Click on Done button")
    public void clickOnDoneBtnInPopup(){
        createPopupDoneBtn.click();
        sleep(10000);
    }

    @Step("Input tile of the feature layer as {title}")
    public void inputTitleOfTheLayer(String title){
        createPopupTitleTxtBox.clear();
        createPopupTitleTxtBox.sendKeys(title);
    }

    @Step("Input tag {tag}")
    public void inputTagsOfTheLayer(String tag){
        inputTags.clear();
        inputTags.sendKeys(tag);
    }

    @Step("Click on From the web")
    public void clickOnAddItemFromWeb() {
        selectOptionToAddItem("Add an item from the web");
        WebElement errorPopupOkBtn = driver.findElement(By.id("button_general-close"));
        //wait.until(ExpectedConditions.elementToBeClickable(errorPopupOkBtn));
        //errorPopupOkBtn.click();
        sleep(1000);
    }

    @Step("Click on From my computer")
    public void clickOnAddItemFromComputer() {
        selectOptionToAddItem("Add an item from my computer");
        sleep(1000);
    }

    @Step("Choose file to upload as a layer")
    public void chooseFileToUpload(String fileName) {
//        chooseFileBtn.click();
//        clickOnElementWithJS(chooseFileBtn);
//        chooseFileBtn.sendKeys("\\\\QBNEWGP01\\ra097$\\RedirectedDocuments\\Work Docs\\BRQ 18a Files\\Depot_final.zip");
        chooseFileBtn.sendKeys(BRQ_18_DATA_FILES_PATH + fileName);
        sleep(1000);
    }

    @Step("Select Type WMS(OGC)")
    public void clickOnTypeWMSOGC() {
        itemTypeOGC.click();
    }

    @Step("Input Item URL {url}")
    public void inputItemURL(String url) {
        itemURL.clear();
        itemURL.sendKeys(url);
        sleep(2000);
    }

    @Step("Input Item Title {title}")
    public void inputItemTitle(String title) {
        wait.until(ExpectedConditions.elementToBeClickable(itemTitle));
        action.moveToElement(itemTitle).click().perform();
        sleep(2000);
        itemTitle.clear();
        sleep(1000);
        itemTitle.sendKeys(title);
        sleep(1000);
    }

    public void clickOnAssignCategoryDrpDwn(){
        assignCategoryDrpDwn.click();
    }

    public void searchForCategory(String category){
        categorySearchInput.sendKeys(category);
    }

    public void selectCategory(String category){
        searchForCategory(category);
        driver.findElement(By.cssSelector(".category-picker a[data-category-name='"+category+"']")).click();
    }

    @Step("Input Item tags {tags}")
    public void inputItemTags(String tags) {
        itemTags.clear();
        itemTags.sendKeys(tags);
        itemTags.sendKeys(Keys.ENTER);
        sleep(1000);
    }

    @Step("Select 'None, add as table' option for Locate Features By")
    public void clickOnTableOnlyOptionForLocateFeatureBy(){
        locateFeaturesByOnlyTableRadioBtn.click();
    }

    @Step("Click on Add Item")
    public void clickOnAddItemBtn() {
        sleep(1000);
        action.moveToElement(addItemPopupBtn).build().perform();
        addItemPopupBtn.click();
        sleep(8000);
    }

    @Step("Verify that filters {expectedFilters} are displayed")
    public void verifyThatFiltersAreDisplayed(List<String> expectedFilters) {
        wait.until(ExpectedConditions.visibilityOfAllElements(filters));
        assertThat(filters.stream().map(filter -> filter.getText()).collect(Collectors.toList())).containsExactlyElementsOf(expectedFilters);
    }

    public boolean doesMyContentPageHaveItems() {
            if(myContentNoItemsElem.getAttribute("class").contains("hide"))
                return false;
            return true;
    }
}
