package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.components.MyOrganization;
import pages.components.WebMapPage;

import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;

public class GalleryPage extends HeaderPage {

    @FindBy(css = "input[aria-label='Search gallery']")
    WebElement searchGalleryTxtBox;

    @FindBy(css = ".js-gallery-wrap .card")
    List<WebElement> galleryItems;

    @FindBy(css = ".gallery-card-wrap  .badge--authoritative")
    WebElement greenTick;

    public GalleryPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Search in gallery for {searchText}")
    public void searchInGalleryFor(String searchText){
        searchGalleryTxtBox.clear();
        searchGalleryTxtBox.sendKeys(searchText);
        searchGalleryTxtBox.sendKeys(Keys.ENTER);
        sleep(500);
    }

    @Step("Verify that gallery items are displayed")
    public void verifyThatGalleryItemsArePresent() {
        assertThat(galleryItems.size()).isGreaterThanOrEqualTo(1);
    }

    @Step("Verify that authoritative green tick is displayed on the item card")
    public void verifyThatGreenTickIsDisplayed() {
        boolean flag = false;
        try {
            if(greenTick.isDisplayed())
                flag = true;
        }
        catch (Exception e){
            flag = false;
        }

        assertThat(flag).isTrue();
    }
}
