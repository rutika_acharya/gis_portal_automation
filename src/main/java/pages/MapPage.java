package pages;

import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import pages.components.ChangeStyleMenu;
import pages.components.FeatureAttributeTable;
import pages.components.LayerMenu;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static pages.components.FilterMenu.*;
import static utils.Utils.round;

//import java.awt.Rectangle;

//import org.openqa.selenium.Rectangle;

public class MapPage extends HeaderPage implements LayerMenu, ChangeStyleMenu, FeatureAttributeTable {

    public static final String BRQ_18_DATA_FILES_PATH = System.getProperty("user.dir") + "\\src\\test\\resources\\data\\BRQ_18_data\\";
    public static final String SCREENSHOT_DIRECTORY = System.getProperty("user.dir") + "\\Screenshots\\Script Screenshots\\";

    @FindBy(css = "#recentMapsLabel, #newMapLabel")
    WebElement newMapBtn;

    @FindBy(css = "a#webmap-recentMap-new-link")
    WebElement createNewMapDrpDwnOption;

    @FindBy(css = "span#button_open-map-yes_label")
    WebElement clickYesOptionToDiscardCurrentMap;

    @FindBy(css = "span[widgetid='webmap-basemap'] .dijitButtonNode")
    WebElement baseMapBtn;

    //@FindBy(css = "span[widgetid='webmap-add'] .dijitButtonNode") //
    @FindBy(xpath = "//span[contains(text(),'Add')]")
    WebElement baseMapAddBtn;

    @FindBy(css = "tr[widgetid='webmap-add-esri-maplayers']")
    WebElement browseLivingAtlasLayers;

    @FindBy(css = "tr[widgetid='webmap-search-layers']")
    WebElement searchLayers;

    @FindBy(id = "section-dropdown-base-dropdown-base")
    WebElement searchLayerInDrpDwn;

    @FindBy(id = "item-browser-search-input")
    WebElement searchLayersTextBox;

    @FindBy(css = "tr[widgetid='webmap-add-layer-url']")
    WebElement addLayerFromWeb;

    @FindBy(css = "tr[widgetid='webmap-add-layer-file']")
    WebElement addLayerFromFile;

    @FindBy(css = "input#add-layer-file-file")
    WebElement fileUploadInput;

    @FindBy(css = "span[widgetid='button_add-layer-file'] .dijitButtonNode")
    WebElement importLayerBtn;

    @FindBy(id = "general-dialog-content")
    WebElement importLayerErrorMsg;

    @FindBy(id = "add-layer-url-url")
    WebElement layerWebURL;

    @FindBy(css = "#add-layer-url-form .esriFloatTrailing .primary .dijitButtonNode")
    WebElement addLayerFromWebBtn;

    @FindBy(css = "tr[widgetid='webmap-add-mapnotes']")
    WebElement addMapNotes;

    @FindBy(css = "span[widgetid='mapnotes-create']")
    WebElement getAddMapNotesBtn;

    @FindBy(xpath = "//div[contains(text(),'Stickpin')]/..")
    WebElement stickpin;

    @FindBy(css = ".galleryNode")
    List<WebElement> baseMaps;

    @FindBy(css = ".card-mc__title-container .card-mc__title")
    List<WebElement> searchedLayers;

    @FindBy(xpath = "//div/img[contains(@title,'Feature Layer')]/../../button")
    List<WebElement> searchedFeatureLayers;

    @FindBy(className = "ib-item-pane__select-btn")
    WebElement addLayerToMapBtn;

    @FindBy(id = "side-panel-close-btn")
    WebElement sidePanelCloseBtn;

    @FindBy(id ="main-back-btn")
    WebElement backBtn;

    @FindBy(css = "span[widgetid='mapnotes-create']")
    WebElement createMapNoteBtn;

    @FindBy(css = "#webmap-toolbar-center span[widgetid='webmap-save']")
    WebElement saveWebMapDrpDwn;

    @FindBy(css = ".dijitMenuItem#webmap-save-save")
    WebElement saveWebMapBtn;

    @FindBy(css = ".dijitMenuItem#webmap-save-saveas")
    WebElement saveAsWebMapBtn;

    @FindBy(id = "logged-in-navigation-link")
    WebElement portalUser;

    @FindBy(id = "logged-in-signOut")
    WebElement signOut;

    @FindBy(id = "home-navigation-dropdown-link")
    WebElement homeNavigation;

    @FindBy(css = "a.homeMyContent")
    WebElement content;

    @FindBy(css = "a.homeGroups")
    WebElement groups;

    @FindBy(css = "#webmap-details")
    WebElement details;

    //@FindBy(css = "#legendContentButtons [widgetid='webmap-details-legend-content'] .dijitButtonNode")
    //@FindBy(css = "#webmap-details-legend-content, #webmap-details-content")
    //@FindBy(css = "[widgetid='webmap-details-about-content'] span[title='Show Contents of Map']")
    @FindBy(css = "div[id*='ContentHeader'] span[title='Show Contents of Map']")
    WebElement showMapContentBtn;

    //@FindBy(css = "div[id*='ContentHeader'] span[title='Show Map Legend']")
    @FindBy(css = "span[widgetid='webmap-details-content-legend'] span.dijitButtonNode")
    WebElement showMapLegendBtn;

    @FindBy(css = "table.esriLegendLayer td[align='left']")
    List<WebElement> legendItems;

    @FindBy(css = "table.esriLegendLayer td[align='center'] img, table.esriLegendLayer td[align='center'] path") //path
    List<WebElement> legendIcons;

    @FindBy(css = "#tocContentPane .toc_layer input[type='checkbox']")
    List<WebElement> mapContents;

    @FindBy(css = "#tocContentPane .toc_layer")
    List<WebElement> mapContent_Layers;

    @FindBy(css = "#tocContentPane .toc_sublayer")
    List<WebElement> mapContent_SubLayers;

    @FindBy(css = "div[title='Zoom Out']")
    WebElement zoomOut;

    @FindBy(css = "div[title='Zoom In']")
    WebElement zoomIn;

    @FindBy(id = "geocoder_input")
    WebElement findPlaceInputTextBox;

    @FindBy(css = ".suggestionsMenu li")
    List<WebElement> suggestionsMenu;

    @FindBy(id = "geocoder_more_results")
    WebElement searchedPlace;

    @FindBy(xpath = "//div[@id='map_layers']/div[not(contains(@id,'Basemap'))]/img/..")
    List<WebElement> mapLayers;

    @FindBy(css = "span[widgetid='webmap-bookmarks'] .dijitButtonNode")
    WebElement bookmarks;

    @FindBy(css = "a#bookmarksClose")
    WebElement closeBookmarkBtn;

    @FindBy(css = ".esriAddBookmark")
    WebElement addBookmark;

    @FindBy(css = ".esriBookmarkEditBox")
    WebElement bookmarkTxtbox;

    @FindBy(css = ".esriBookmarkItem .esriBookmarkLabel")
    List<WebElement> createdBookmarks;

    @FindBy(css = ".toc_layer .iconMenu span[title='More Options']")
    WebElement layerMoreOpts;

    @FindBy(id = "map")
    WebElement map;

    @FindBy(css = "g[data-geometry-type]")
    List<WebElement> mapNoteLayers;

    @FindBy(css = "g[data-geometry-type] image,path,text")
    List<WebElement> mapNotes;

    @FindBy(css = "g[data-geometry-type] text")
    List<WebElement> mapTextNotes;

    @FindBy(css = "#webmap-measure")
    WebElement measureBtn;

    @FindBy(css = ".esriMeasurementResultValue .result")
    WebElement measurementResult;

    @FindBy(css = ".esriToggleButton")
    WebElement measurementDropDwn;

    @FindBy(css = ".esriPopup .mainSection .header")
    WebElement mapPopup;

    @FindBy(css = ".titleButton.close")
    WebElement getMapPopupCloseBtn;

    @FindBy(css = "span#webmap-edit")
    WebElement editBtn;

    @FindBy(css = ".item")
    WebElement newFeatureBtn;

    @FindBy(css = ".atiAttributes input[id*='form_ValidationTextBox']")
    WebElement popupFieldValueTxtBox;

    @FindBy(id = "button_general-close_label")
    WebElement errorPopupOkBtn;

    @FindBy(css = ".esriAGOAddItemFromLayerForm span.primary  .dijitButtonNode")
    WebElement createItemPopupBtn;

    @FindBy(css = ".esriAGOAddItemFromLayerForm")
    WebElement createItemPopup;

    @FindBy(css = "div.home")
    WebElement defaultExtentBtn;

    @FindBy(id = "webmap-analysis")
    WebElement analysisBtn;

    @FindBy(css = "div[data-dojo-attach-point='_featureAccordion'] div[data-esrihelptopic='UseProximityCategory']")
    WebElement useProximityTool;

    @FindBy(css = ".toolContainer .buffersIcon")
    WebElement createBuffersTool;

    @FindBy(css = ".esriAnalysis tr[data-dojo-attach-point='_bufDistRow'] .dijitValidationTextBox .dijitInputField input.dijitReset")
    WebElement bufferDistanceInput;

    @FindBy(css = ".esriAnalysisSelect ")
    WebElement bufferDistanceUnitDrpDwn;

    @FindBy(css = ".dijitMenu .dijitMenuItem")
    List<WebElement> dropDownOptions;

    @FindBy(css = ".esriAnalysis div.longInput [id*='ValidationTextBox'][aria-required='true']")
    WebElement resultLayerInput;

    @FindBy(css = "input[name='extent']")
    WebElement mapExtentCheckbox;

    @FindBy(css = "div[data-dojo-attach-point='_bufferToolContentButtons'] span.dijitButtonNode")
    WebElement runAnalysisBtn;

    @FindBy(css = "td.esriFloatTrailing")
    WebElement analysisLoadingElement;

    @FindBy(css = ".esriColorPicker div.dijitSliderMoveable .dijitSliderImageHandle")
    WebElement changeSymbolTransparencySlider;

    @FindBy(css = ".esriColorPicker .dijitSlider input[data-dojo-attach-point='valueNode']")
    WebElement changeSymbolDialogueTransparencyValue;

    public MapPage() {
        PageFactory.initElements(driver, this);
    }

    public String getMapZoomData() {
        return map.getAttribute("data-zoom");
    }

    public WebElement getLayer(String layerName) {
        return mapContent_Layers.stream().filter(layer -> layer.getText().contains(layerName)).collect(Collectors.toList()).get(0);
    }

    public WebElement getSubLayer(String layerName) {
        return mapContent_SubLayers.stream().filter(layer -> layer.getText().contains(layerName)).collect(Collectors.toList()).get(0);
    }

    @Step("Click on Analysis tab")
    public void clickOnAnalysisBtn() {
        analysisBtn.click();
    }

    @Step("Expand proximity tool")
    public void expandUseProximityTool() {
        useProximityTool.click();
    }

    @Step("Select Create Buffers tool")
    public void selectCreateBuffersTool() {
        createBuffersTool.click();
        sleep(2000);
    }

    @Step("Input Buffer Distance {value}")
    public void inputBufferDistance(String value){
        bufferDistanceInput.clear();
        bufferDistanceInput.sendKeys(value);
    }

    @Step("Select Buffer Distance Unit as {unitToSelect}")
    public void selectBufferDistanceUnit(String unitToSelect) {
        bufferDistanceUnitDrpDwn.click();
        dropDownOptions.stream().filter(elem-> elem.getAttribute("aria-label").trim().equalsIgnoreCase(unitToSelect)).findFirst().get().click();
    }

    @Step("Input result layer name as {name}")
    public void inputResultLayerName(String name) {
        resultLayerInput.clear();
        resultLayerInput.sendKeys(name);
    }

    @Step("Deselect Use Current Map Extent checkobx")
    public void deselectUseCurrentMapExtentCheckbox() {
        mapExtentCheckbox.click();
    }

    @Step("Click on Run Analysis button")
    public void clickOnRunAnalysisBtn() {
        runAnalysisBtn.click();
        wait.until(ExpectedConditions.invisibilityOf(analysisLoadingElement));
        //sleep(15000);
    }

    @Step("Click on Edit button")
    public void clickOnEditBtn(){
        editBtn.click();
    }

    @Step("Click on New Feature")
    public void clickOnNewFeature(){
        newFeatureBtn.click();
    }

    @Step("Draw a point on map")
    public void drawPoint(int xOffset, int yOffset){
        //action.moveByOffset(-50, -50).click().perform();
        action.moveToElement(map,xOffset, yOffset).click().perform();
        sleep(2000);
    }

    public boolean isLayerDisplayedInContentPane(String layerName) {
        return mapContent_Layers.stream().filter(layer -> layer.getText().contains(layerName)).collect(Collectors.toList()).size() == 1;
    }

    public boolean isLayerDisplayedOnMap(String layerName) {
        return mapLayers.stream().filter(layer -> layer.getAttribute("id").contains(layerName)).collect(Collectors.toList()).size() == 1;
    }

    @Step("Click on default extent button")
    public void clickOnDefaultExtentBtn() {
        defaultExtentBtn.click();
    }

    @Step("Hover over layer {layer}")
    public void hoverOverLayer(WebElement layer){
        action.moveToElement(layer).perform();
    }

    @Step("Click on 'More Options' for layer {layerName}")
    public void clickOnMoreOptionsForLayer(String layerName) {
        sleep(5000);
        WebElement layer = getLayer(layerName);
        hoverOverLayer(layer);
        wait.until(ExpectedConditions.elementToBeClickable(layer.findElement(By.cssSelector("span[title='More Options']")))).click();
    }

    @Step("Click on 'Change Style' icon for layer {layerName}")
    public void clickOnChangeStyleForLayer(String layerName){
        WebElement layer = getLayer(layerName);
        hoverOverLayer(layer);
        wait.until(ExpectedConditions.elementToBeClickable(layer.findElement(By.cssSelector(".iconChangeStyle")))).click();
        sleep(5000);
    }

    @Step("Click on 'Filter' icon for layer {layerName}")
    public void clickOnFilterForLayer(String layerName){
        WebElement layer = getLayer(layerName);
        hoverOverLayer(layer);
        wait.until(ExpectedConditions.elementToBeClickable(layer.findElement(By.cssSelector(".iconFilter")))).click();
        sleep(5000);
    }

    @Step("Click on show attribute table for layer {layerName}")
    public void clickOnShowAttributeTableForLayer(String layerName){
        WebElement layer = getLayer(layerName);
        hoverOverLayer(layer);
        wait.until(ExpectedConditions.elementToBeClickable(layer.findElement(By.cssSelector(".iconShowAttributeTable")))).click();
        sleep(5000);
    }

    @Step("Click on first row of the attribute table")
    public void clickOnFirstRowOfAttributeTable(){
        driver.findElement(getAttributeTableFirstRow).click();
    }

    @Step("Click on attribute table header 'Currency'")
    public void clickOnAttributeTableHeaderCurrency(){
        action.moveToElement(driver.findElement(attributeTableHeaderCurrency))
                .click().perform();
    }

    @Step("Click on sort {sortType}")
    public void clickOnSort(String sortType){
        WebElement sort = driver.findElement(By.cssSelector("tr[aria-label='Sort "+sortType+" '] .dijitMenuArrowCell"));
        clickOnElementWithJS(sort);
    }

    @Step("Verify that values are sorted in ascending order for field Currency")
    public void verifyThatValuesAreSortedInAscForFieldCurrency(){
        driver.findElement(attributeTableHeaderCurrencySort).getAttribute("class").contains("sort-up");
    }

    @Step("Verify that values are sorted in descending order for field Currency")
    public void verifyThatValuesAreSortedInDescForFieldCurrency(){
        driver.findElement(attributeTableHeaderCurrencySort).getAttribute("class").contains("sort-down");
    }

    @Step("Get value for field currency")
    public String getFirstValueForFieldCurrency(){
        return driver.findElement(fieldCurrencyValue).getText();
    }

    @Step("Verify that after sorting the Currency column as ASC and DESC the values for the field currency are same")
    public void verifyAscDescValuesAreSameForAttributeTableColumn(String asc_value, String desc_value){
        assertThat(asc_value).isEqualToIgnoringCase(desc_value);
    }

    @Step("Verify that after removing the filter and sorting the values for the field currency as DESC again the value is NOT same")
    public void verifyAscDescValuesAreNOTSameForAttributeTableColumn(String asc_value, String desc_value){
        assertThat(asc_value).isNotEqualToIgnoringCase(desc_value);
    }

    @Step("Click on option for attribute table")
    public void clickOnOption(){
        action.moveToElement(driver.findElement(attributeTableOptions)).click().perform();
        sleep(8000);
    }

    @Step("Click on 'Filter' option in the menu")
    public void clickOnOptionMenuFilter(){
        WebElement filter = driver.findElement(filterOption);
        clickOnElementWithJS(filter);
    }

    @Step("Click on 'Add Field' option in the menu")
    public void clickOnOptionMenuAddField(){
        WebElement filter = driver.findElement(addFieldOption);
        clickOnElementWithJS(filter);
    }

    @Step("Click on 'Center on Selection' option in the menu")
    public void clickOnOptionMenuCenterOnSelection(){
        WebElement filter = driver.findElement(centerOnSelectionOption);
        clickOnElementWithJS(filter);
        sleep(5000);
    }

    public void closeAttributeTable(){
        driver.findElement(closeBtn).click();
    }

    public void verifyThatMapIsCenteredOnTheFeature(String featureLayer){
        WebElement svg = driver.findElement(By.xpath("//div[@id='map_gc']//*[contains(@id,"+featureLayer+")]/.."));
        System.out.println(svg.getCssValue("transform"));
        System.out.println(svg.getAttribute("style"));
        assertThat(svg.getCssValue("transform")).contains("translate(-50px, -202px)");
    }

    public String getAttributeTableTitle() {
        return driver.findElement(attributeTableTitle).getText();
    }

    public int getNoOfFeaturesInAttributeTable(){
        return Integer.parseInt(getAttributeTableTitle().split("Features: ")[1].split(",")[0]);
    }

    @Step("Verify that filter is applied successfully")
    public void verifyTheNoOfFeaturesInAttributeTableAreReduced(int prevNoOfFeatures) {
        assertThat(prevNoOfFeatures).isGreaterThan(getNoOfFeaturesInAttributeTable());
    }

    public void verifyTheNoOfFeaturesInAttributeTableAreIncreased(int prevNoOfFeatures) {
        assertThat(prevNoOfFeatures).isLessThan(getNoOfFeaturesInAttributeTable());
    }

    public void verifyThatFeatureIsSelected(String layerName){
        assertThat(getAttributeTableTitle()).containsIgnoringCase(layerName +" (Features: 1, Selected: 1)");
    }

    public void verifyThatFeatureIsNotSelected(String layerName){
        assertThat(getAttributeTableTitle()).containsIgnoringCase(layerName + " (Features: 1, Selected: 0)");
    }

    @Step("Click on 'Clear Selection' option in the menu")
    public void clickOnOptionMenuClearSelection(){
        WebElement filter = driver.findElement(clearSelectionOption);
        clickOnElementWithJS(filter);
    }

    @Step("Input field name {fieldName}")
    public void inputFieldNameInAddNewFieldPopup(String fieldName){
        driver.findElement(addFieldPopupFieldNameTxtBox).clear();
        driver.findElement(addFieldPopupFieldNameTxtBox).sendKeys(fieldName);
        sleep(500);
    }

    @Step("Input display name {displayName}")
    public void inputDisplayNameInAddNewFieldPopup(String displayName){
        driver.findElement(addFieldPopupDisplayNameTxtBox).clear();
        driver.findElement(addFieldPopupDisplayNameTxtBox).sendKeys(displayName);
    }

    @Step("Click on Add New Field button")
    public void clickOnAddNewFieldBtn(){
        driver.findElement(addFieldPopupAddBtn).click();
        sleep(1000);
    }

    @Step("Verify that 'Saving Layer' popup is displayed")
    public void verifyThatSavingLayerPopupIsDisplayed(){
        assertThat(wait.until(ExpectedConditions.visibilityOfElementLocated(savingLayerPopup))).isNotNull();
    }

    @Step("Verify that field name {expectedFieldName} is displayed in attribute table")
    public void verifyThatFieldNameIsDisplayedInAttributeTable(String expectedFieldName){
        assertThat(driver.findElement(attributeTableFieldName).getText().equalsIgnoreCase(expectedFieldName)).isTrue();
    }

    @Step("Edit the field and input field value {fieldValue}")
    public void editAndAddInfoToTheField(String fieldValue) {
        action.moveToElement(driver.findElement(getAttributeTableFirstRow)).doubleClick().build().perform();
        driver.findElement(attributeTableFieldValue).sendKeys(fieldValue);
        driver.findElement(attributeTableFieldValue).sendKeys(Keys.ENTER);
        sleep(1000);
        driver.findElement(closeBtn).click();
    }

    @Step("Click on feature on the map and verify that info is added to the field {fieldName} with value as {fieldValue}")
    public void verifyThatInfoIsAddedToTheField(String fieldName, String fieldValue) {
        System.out.println("No of notes :---> " + mapNotes.size());
        WebElement point = driver.findElement(By.cssSelector("g[id*='Test_Feature_Layer'] image"));
        action.moveToElement(point).click().perform();
        assertThat(driver.findElement(attributeTablePopupAttrName).getText()).isEqualToIgnoringCase(fieldName);
        assertThat(driver.findElement(attributeTablePopupAttrValue).getText()).isEqualToIgnoringCase(fieldValue);
    }

    public void clickOnAFeatureForALayer(String featureLayerName) {
        WebElement point = driver.findElement(By.cssSelector("g[id*='"+featureLayerName+"'] image"));
        action.moveToElement(point).click().perform();
    }

    @Step("Click on feature on the map and change the field value to {newValue}")
    public void editAndChangeFieldValue(String newValue, String featureLayerName){
        clickOnAFeatureForALayer(featureLayerName);
        popupFieldValueTxtBox.clear();
        popupFieldValueTxtBox.sendKeys(newValue);
    }

    public int getNoOfFeaturesForAFeatureLayer(String featureLayer){
        return driver.findElements(By.cssSelector("g[id*='"+ featureLayer +"'] image")).size();
    }

    @Step("Verify that the no. of features for a layer {featureLayer} are now {expectedNo}")
    public void verifyNoOfFeaturesForAFeatureLayer(int expectedNo, String featureLayer){
        sleep(500);
        assertThat(getNoOfFeaturesForAFeatureLayer(featureLayer)).isEqualTo(expectedNo);
    };

    @Step("Delete a feature for layer {featureLayerName}")
    public void deleteAFeatureForLayer(String featureLayerName) {
        WebElement point = driver.findElement(By.cssSelector("g[id*='Test_Feature_Layer'] image"));
        action.moveToElement(point).click().perform();
        driver.findElement(By.cssSelector("span.atiDeleteButton")).click();
    }

    @Step("Click on remove filter button")
    public void clickOnRemoveFilterBtn(){
        WebElement removeFilter = driver.findElement(removeFilterBtn);
        removeFilter.click();
        sleep(1000);
    }

    @Step("Select field {fieldToBeSelected} in Filter popup")
    public void selectFieldToFilter(String fieldToBeSelected){
        //System.out.println(driver.getPageSource());
        sleep(1000);
        driver.findElement(selectFieldDrpDwn).click();
        //clickOnElementWithJS(driver.findElement(selectFieldDrpDwn));
        List<WebElement> options = driver.findElements(menuOption);
        options.stream().filter(option -> option.getText().equalsIgnoreCase(fieldToBeSelected)).findFirst().get().click();
        sleep(1000);
    }

    @Step("Select operation '{operationToBeSelected}' in Filter popup")
    public void selectOperationToFilter(String operationToBeSelected){
        driver.findElement(selectOperationDrpDwn).click();
        List<WebElement> options = driver.findElements(operatorList);
        options.stream().filter(option -> option.getText().equalsIgnoreCase(operationToBeSelected)).findFirst().get().click();
        sleep(1000);
    }

    @Step("Input value {value} to filter by in Filter popup")
    public void inputValueForSelectedField(String value){
        driver.findElement(valueTxtBox).sendKeys(value);
        sleep(3000);
    }

    @Step("Click on filter button")
    public void clickOnFilterButton(){
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(applyFilterBtn))).click();
        //action.moveToElement(driver.findElement(applyFilterBtn)).click().perform();
        sleep(6000);

    }

    @Step("Click on Apply Filter And Zoom To button")
    public void clickOnFilterAndZoomToButton(){
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(applyFilterAndZoomToBtn))).click();
        //action.moveToElement(driver.findElement(applyFilterBtn)).click().perform();
        sleep(6000);

    }

    @Step("Click on Select button for Location")
    public void clickOnSelectBtnForLocation(){
       driver.findElement(locationMenuSelectBtn).click();
    }

    @Step("Click on Option button for Location")
    public void clickOnOptionBtnForLocation(){
        scrollToElementWithJS(driver.findElement(locationMenuOptionBtn));
        /*wait.until(ExpectedConditions.visibilityOfElementLocated(locationMenuOptionBtn));
        action.moveToElement(driver.findElement(locationMenuOptionBtn))
        .click()
        .perform();*/
        clickOnElementWithJS(driver.findElement(locationMenuOptionBtn));
        sleep(2000);
    }

    @Step("Click on Symbols")
    public void clickOnSymbolsLink(){
        driver.findElement(symbolsLink).click();
        sleep(2000);
    }

    @Step("Choose symbol in row {row} and column {column}")
    public String chooseSymbolInRowAndColumn(String row, String column){
        WebElement symbolToChoose = driver.findElement(By.cssSelector(".dojoxGridContent .dojoxGridRow:nth-child("+row+") .dojoxGridCell:nth-child("+column+")"));
        symbolToChoose.click();
        System.out.println(symbolToChoose.findElement(By.tagName("image")).getAttribute("xlink:href"));
        return symbolToChoose.findElement(By.tagName("image")).getAttribute("xlink:href");
    }

    @Step("Click on Ok")
    public void clickOnOkOnSymbolChangePopup(){

        //click on Ok Button
        action.moveToElement(driver.findElement(symbolPopupOkBtn)).click().perform();
    }

    @Step("Set symbol size as {sizeInPixel} pixels")
    public void setSymbolSizeInPixel(String sizeInPixel){
        WebElement sizeTxtBox = driver.findElement(symbolSizeTxtBox);
        //sizeTxtBox.clear();
        action.moveToElement(sizeTxtBox).doubleClick().perform();
        sizeTxtBox.sendKeys(sizeInPixel);

        WebElement upArrowBtn = driver.findElement(symbolPopupUpArrowBtn);
        while(!sizeTxtBox.getAttribute("aria-valuenow").equalsIgnoreCase(sizeInPixel)){
            upArrowBtn.click();
            System.out.println(sizeTxtBox.getText());
            System.out.println("Clicking on arrow btn");
        }
        sizeTxtBox.sendKeys(Keys.ENTER);
        sleep(5000);
    }

    @Step("Click on OK")
    public void clickOnOK(){
        sleep(2000);
        driver.findElement(changeStylePanelOkBtn).click();
    }

    @Step("Click on Done")
    public void clickOnDone(){
        sleep(2000);
        driver.findElement(changeStylePanelDoneBtn).click();
    }

    @Step("Click on Measure")
    public void clickOnMeasureBtn(){
        measureBtn.click();
        sleep(2000);
    }

    @Step("Select {measuringType} to measure")
    public void selectTypeOfMeasuringToPerform(String measuringType){
        action.moveToElement(driver.findElement(By.cssSelector("span[title='"+measuringType+"']"))).click().perform();
        sleep(2000);
    }

    @Step("Draw a line to measure the distance")
    public void drawLineToMeasureDistance(){
        /*action.moveByOffset(-50, -50).click().perform();
        action.moveByOffset(-100, -100).doubleClick().perform();*/
        action.moveToElement(map,-100, 100).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(0, 80).doubleClick().
                perform();
        sleep(2000);
    }

    @Step("Draw area on the map to measure")
    public void drawAreaToMeasureDistance(){
        action.moveToElement(map,-100, 100).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(0, 100).click().
                pause(Duration.ofSeconds(1)).
                moveByOffset(50, 0).click().
                pause(Duration.ofSeconds(1)).
                doubleClick().
                perform();
        //action.moveByOffset(-50, -50).click().perform();
        //action.moveByOffset(-100, -100).doubleClick().perform();
        sleep(2000);
    }

    @Step("Draw a point on map")
    public void drawPointToMeasureLocation(){
        //action.moveByOffset(-50, -50).click().perform();
        action.moveToElement(map,-100, 100).click().perform();
        sleep(2000);
    }

    @Step("Verify that latitude and longitude of the point is displayed")
    public void verifyLatitudeAndLongitudeOfThePoint(){
        String latitude = driver.findElement(By.cssSelector(".esriMeasurementTableRow [dojoattachpoint='markerLatitude']")).getText();
        String longitude = driver.findElement(By.cssSelector(".esriMeasurementTableRow [dojoattachpoint='markerLatitude']")).getText();
        System.out.println(latitude +" -- "+ longitude);
        assertThat(latitude).isNotBlank();
        assertThat(longitude).isNotBlank();
    }

    @Step("Verify the change in measurement value when unit changes")
    public void verifyMeasurementKmToM(String valueKm, String valueM){
        assertThat(Double.parseDouble(valueKm)).isEqualTo(round((Double.parseDouble(valueM)/1000), 1));
    }

    @Step("Verify the change in measurement value when unit changes")
    public void verifyMeasurementSqKmToM(String valueSqKm, String valueSqM){
        assertThat(Double.parseDouble(valueSqKm)).isEqualTo(round((Double.parseDouble(valueSqM)/1000000), 1));
    }

    @Step("Get measurement value")
    public String getMeasurementResultValue(){
        System.out.println(measurementResult.getText());
        return measurementResult.getText();
    }

    @Step("Click on measurement unit dropdown")
    public void clickOnMeasurementDropDwn(){
        measurementDropDwn.click();
    }

    @Step("Select measurement unit {unit}")
    public void selectMeasurementUnit(String unit){
        driver.findElement(By.cssSelector(".unitDropDown[aria-label='"+unit+" ']")).click();
    }

    @Step("Verify that layer {layerName} is displayed in the content pane")
    public void verifyLayerIsDisplayedInContentPane(String layerName) {
        assertThat(isLayerDisplayedInContentPane(layerName)).isTrue();
    }

    @Step("Verify that layer {layerName} is displayed on the map")
    public void verifyLayerIsDisplayedOnTheMap(String layerName) {
        assertThat(isLayerDisplayedOnMap(layerName)).isTrue();
    }

    @Step("Verify that layer {layerName} is NOT displayed on the map")
    public void verifyLayerIsRemoved(String layerName) {
        assertThat(isLayerDisplayedInContentPane(layerName)).isFalse();
    }

    @Step("Click on Transparency option for layer {layerName}")
    public void clickOnTransparency(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(transparency).click();
    }

    @Step("Click on Save Layer option for layer {layerName}")
    public void clickOnSaveLayer(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(saveLayer).click();
    }

    @Step("Click on Save Layer option for layer {layerName}")
    public void clickOnSaveLayerAsItem(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(saveLayerAsItem).click();
    }

    @Step("Click on Create Item in popup")
    public void clickOnCreateItem(){
        sleep(2000);
        createItemPopupBtn.click();
        sleep(10000);
        //wait.until(ExpectedConditions.invisibilityOf(createItemPopupBtn));

    }

    @Step("Verify that layer {layerName} is saved")
    public void verifyThatLayerIsSaved(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        assertThat(driver.findElement(saveLayer).getAttribute("class")).containsIgnoringCase("dijitMenuItemDisabled");
    }

    @Step("Click on Zoom To Layer {layerName}")
    public void clickOnZoomToLayer(String layerName){
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(zoomTo).click();
        sleep(7000);
    }

    @Step("Click on Set Visibility Range option for layer {layerName}")
    public void clickOnSetVisibilityRange(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(visibilityRange).click();
        sleep(2000);
    }

    public void clickOnVisibilityRangeDrpDwn() {
        driver.findElement(visibilityDrpDwn).click();
    }

    @Step("Click on Move Up option for layer {layerName}")
    public void clickOnMoveLayerUp(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(moveUp).click();
    }

    @Step("Click on Rename option for layer {layerName}")
    public void clickOnRename(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(rename).click();
    }

    @Step("Click on Copy option for layer {layerName}")
    public void clickOnCopy(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(copy).click();
        sleep(1000);
    }

    @Step("Click on Hide In Legend option for layer {layerName}")
    public void clickOnHideInLegend(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(hideInLegend).click();
    }

    @Step("Click on Hide In Legend option for layer {layerName}")
    public void clickOnConfigureAndShowPopup(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(configurePopup).click();
        driver.findElement(showPopupCheckbox).click();
        driver.findElement(By.xpath("//div[@id='popupBuilderContentButtonsCenter']//span[contains(text(), 'OK')]/../..")).click();
    }

    @Step("Click on Enable Popup option for layer {layerName}")
    public void clickOnEnablePopup(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(enablePopup).click();
    }

    @Step("Click on Remove Popup option for layer {layerName}")
    public void clickOnRemovePopup(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(removePopup).click();
    }

    @Step("Set refresh interval for layer {LayerName}")
    public void setRefreshIntervalForLayer(String layerName, String interval) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(refreshInterval).click();
        driver.findElement(refreshIntervalCheckBox).click();
        WebElement txtBox = driver.findElement(refreshIntervalTxtBox);
        txtBox.clear();
        txtBox.sendKeys(interval);
        this.clickOnMoreOptionsForLayer(layerName);
    }

    @Step("Click on Manage Label option for layer {layerName}")
    public void clickOnManageLabels(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(createLabels).click();
        driver.findElement(By.xpath("//div[@id='rendererLabelsContentButtonsCenter']//span[contains(text(), 'OK')]/../..")).click();
    }

    @Step("Click on Show Item Details option for layer {layerName}")
    public void clickOnShowItemDetails(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(showItemDetails).click();
    }

    @Step("Verify that layer {layerName} is opened in new window")
    public void verifyThatLayerIsOpenedInNewWindow(String layerName) {
        assertThat(driver.getWindowHandles().size()).isEqualTo(2);
        String currentWindow = driver.getWindowHandle();
        switchToNewWindow();
        sleep(1000);
        //assertThat(driver.findElement(By.id("pagetitle")).getText()).isEqualToIgnoringCase(layerName);
        switchToWindow(currentWindow);
    }

    @Step("Rename layer {layerName} with {newName}")
    public void renameLayer(String layerName, String newName) {
        this.clickOnRename(layerName);
        WebElement txtBox = driver.findElement(layerNameTxtBox);
        txtBox.clear();
        txtBox.sendKeys(newName);
        WebElement okBtn = driver.findElement(By.xpath("//div[@id='rename-dialog']//span[contains(text(), 'OK')]/../.."));
        okBtn.click();
        sleep(5000);
    }

    @Step("Click on Remove option for layer {layerName}")
    public void removeLayer(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(remove).click();
        driver.findElement(By.xpath("//div[@id='choice-dialog']//span[contains(text(), 'remove')]/../..")).click();
    }

    @Step("Click on Move Down option for layer {layerName}")
    public void clickOnMoveLayerDown(String layerName) {
        this.clickOnMoreOptionsForLayer(layerName);
        driver.findElement(moveDown).click();
    }

    @Step("Select visibility {option}")
    public void selectVisibilityOption(String option) {
        this.clickOnVisibilityRangeDrpDwn();
        driver.findElements(visibilityDrpDwnOpts).stream().filter(opt -> opt.getText().contains(option)).findFirst().get().click();
        sleep(2000);
        action.moveByOffset(0,0).click().perform();
    }

    @Step("Verify that layer is NOT visible")
    public void verifyVisibilityIsOutOfRangeForLayer(String layerName){
        sleep(2000);
        WebElement layer = getLayer(layerName).findElement(By.cssSelector(".toc_layerName"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute("class").contains("outOfScaleRange")).isTrue();
    }

    @Step("Verify that layer is visible")
    public void verifyVisibilityIsInRangeForLayer(String layerName){
        sleep(2000);
        WebElement layer = getLayer(layerName).findElement(By.cssSelector(".toc_layerName"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute(
                "class").contains("outOfScaleRange")).isFalse();
    }

    @Step("Verify that layer is visible")
    public void verifyVisibilityIsInRangeForSubLayer(String layerName){
        sleep(2000);
        WebElement layer = getSubLayer(layerName).findElement(By.cssSelector(".toc_layerName"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute(
                "class").contains("outOfScaleRange")).isFalse();
    }

    @Step("Verify that layer is NOT visible")
    public void verifyVisibilityIsNotInRangeForSubLayer(String layerName){
        sleep(2000);
        WebElement layer = getSubLayer(layerName).findElement(By.cssSelector(".toc_layerName"));
        System.out.println(layer.getAttribute("class"));
        assertThat(layer.getAttribute(
                "class").contains("outOfScaleRange")).isTrue();
    }

    @Step("Expand layer {layerName} in content pane")
    public void expandLayerInContentPane(String layerName) {
        getLayer(layerName).findElement(By.cssSelector(".iconGroupClosed")).click();
        sleep(2000);
        addScreenshotToStep();
    }

    public int getCurrentTransparencyValue() {
        return Integer.parseInt(driver.findElement(transparencyValue).getAttribute("value"));
    }

    public void setTransparency(int valueToBeSet) {
        if (valueToBeSet > this.getCurrentTransparencyValue()) {
            while (this.getCurrentTransparencyValue() < valueToBeSet) {
                action.sendKeys(driver.findElement(transparencySlider), Keys.ARROW_RIGHT).release().perform();
            }
        } else if (valueToBeSet < this.getCurrentTransparencyValue()) {
            while (this.getCurrentTransparencyValue() > valueToBeSet) {
                action.sendKeys(driver.findElement(transparencySlider), Keys.ARROW_LEFT).release().perform();
            }
        }
        sleep(2000);
        action.moveByOffset(10,10).click().perform();
    }

    public void setVisibilityRange() {
        action.sendKeys(driver.findElement(visibilitySlider), Keys.ARROW_RIGHT).release().perform();
        sleep(2000);
    }

    @Step("Click on Add Layer from Web")
    public void clickOnAddLayerFromWeb() {
        addLayerFromWeb.click();
    }

    @Step("Click on Add Layer from File")
    public void clickOnAddLayerFromFile() {
        addLayerFromFile.click();
    }

    @Step("Select and upload file {fileName}")
    public void uploadFileToBeAddedAsLayer(String fileName){
        fileUploadInput.sendKeys(BRQ_18_DATA_FILES_PATH + fileName);
    }

    @Step("Click on Import Layer button")
    public void clickOnImportLayerBtn(){
        importLayerBtn.click();
        sleep(3000);
    }

    public String getImportLayerErrorMsg(){
        return importLayerErrorMsg.getText();
    }

    @Step("Verify that no error message is displayed on importing a layer")
    public void verifyThatImportLayerErrorMessageIsNotDisplayed(){
        assertThat(importLayerErrorMsg.isDisplayed()).isFalse();
    }

    @Step("Input URL")
    public void inputWebLayerURL(String url) {
        sleep(1000);
        layerWebURL.clear();
        layerWebURL.sendKeys(url);
        sleep(3000);
    }

    @Step("Click on Add Layer button")
    public void clickOnAddLayerBtn() {
        addLayerFromWebBtn.click();
    }

    @Step("Click on Bookmarks")
    public void clickOnBookmarks() {
        wait.until(ExpectedConditions.elementToBeClickable(bookmarks));
        bookmarks.click();
    }

    @Step("Close Bookmark")
    public void closeBookmark() {
        closeBookmarkBtn.click();
    }

    @Step("Click on Add Bookmark")
    public void clickOnAddBookmark() {
        sleep(3000);
        addBookmark.click();
        sleep(1000);
    }

    @Step("Click on Bookmark {bookmarkName}")
    public void clickOnBookmarkWithName(String bookmarkName) {
        createdBookmarks.stream().filter(bookmark -> bookmark.getText().equalsIgnoreCase(bookmarkName)).collect(Collectors.toList()).get(0).click();
        sleep(3000);
    }

    public boolean isBookmarkPresent(String searchBookmark) {
        return createdBookmarks.stream().filter(bookmark -> bookmark.getText().equalsIgnoreCase(searchBookmark)).collect(Collectors.toList()).size() == 1;
    }

    public void inputBookmarkName(String bookmarkName) {
        bookmarkTxtbox.sendKeys(bookmarkName);
        bookmarkTxtbox.sendKeys(Keys.ENTER);
        action.moveByOffset(0, 100).click().perform();
    }

    @Step("Find a place - {place} on the map")
    public void findPlaceOnMap(String place) {
        inputPlaceOrAddressToFindOnMap(place);
        sleep(1000);
        suggestionsMenu.stream().filter(menu -> menu.getText().contains(place)).findFirst().get().click();
        sleep(3000);

    }

    @Step("Verify that place {place} is displayed on the map")
    public void verifyThatSearchedPlaceIsDisplayed(String place) {
        sleep(1000);
        assertThat(this.getTextForTheSearchedPlace()).containsIgnoringCase(place);
        driver.findElement(By.cssSelector(".titleButton.close")).click();
        sleep(3000);
    }

    public void cleanDirectory() {
        try {
            FileUtils.cleanDirectory(new File(SCREENSHOT_DIRECTORY));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void findTextCairns() {

        Rectangle rect = map.getRect();
        System.out.println(rect.x);
        System.out.println(rect.y);
        System.out.println(rect.height);
        System.out.println(rect.width);
        java.awt.Rectangle rect1 = new java.awt.Rectangle();
        rect1.setRect(rect.x, rect.y, rect.width, rect.height);
        Region region = Region.create(rect1);
        //region.highlight();
        Match m = null;
        try {
            m = region.findText("Cairns");
        } catch (FindFailed findFailed) {
            System.out.println("Not Found");
            findFailed.printStackTrace();
        }
        m.highlight("Green");
        m.click();
    }

    @Step("Take screenshot")
    public void takeScreenshot(String imageName) {
        sleep(3000);
        Screenshot s = new AShot().
                shootingStrategy(ShootingStrategies.viewportPasting(100)).coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, map);
        try {
            boolean flag = ImageIO.write(s.getImage(), "png", new File(System.getProperty("user.dir") + "\\Screenshots\\Script Screenshots\\" + imageName + ".png"));
            System.out.println(flag);
        } catch (IOException e) {
            e.printStackTrace();
        }
        addScreenshotToStep();
    }

    @Step("Verify that map {image1} image and {image2} image are same")
    public void compareImagesWithDiff(String image1, String image2, int difference) {
        try {
            BufferedImage expectedImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + image1 + ".png")).getSubimage(400, 200, 572, 243);
            BufferedImage actualImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + image2 + ".png")).getSubimage(400, 200, 569, 252);

            ImageIO.write(expectedImage, "png", new File(SCREENSHOT_DIRECTORY + image1 + "1.png"));
            ImageIO.write(actualImage, "png", new File(SCREENSHOT_DIRECTORY + image2 + "1.png"));
            ImageDiffer imgDiff = new ImageDiffer();

            ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
            //ImageIO.write(diff.getDiffImage(), "png", new File(System.getProperty("user.dir") + "\\Screenshots\\Difference.png"));


            if (diff.withDiffSizeTrigger(difference).hasDiff() == true) { //.withDiffSizeTrigger(90000)
                System.out.println(diff.getDiffSize());
                System.out.println("Images are different");

            } else {
                System.out.println("Images are same");
            }
            System.out.println(diff.getDiffSize());
            assertThat(diff.withDiffSizeTrigger(difference).hasDiff()).isFalse();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void compareImage() {
        try {
            BufferedImage expectedImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + "Expected.png"));
            BufferedImage actualImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + "Actual.png"));

            ImageDiffer imgDiff = new ImageDiffer();

            ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
            //ImageIO.write(diff.getDiffImage(), "png", new File(System.getProperty("user.dir") + "\\Screenshots\\Difference.png"));


            if (diff.hasDiff() == true) { //.withDiffSizeTrigger(90000)
                System.out.println(diff.getDiffSize());
                System.out.println("Images are different");

            } else {
                System.out.println("Images are same");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Step("Verify that image {imageName1} and image {imageName2} are same")
    public void compareImage(String imageName1, String imageName2) {
        try {
            BufferedImage expectedImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + imageName1 + ".png"));
            BufferedImage actualImage = ImageIO.read(new File(SCREENSHOT_DIRECTORY + imageName2 + ".png"));

            ImageDiffer imgDiff = new ImageDiffer();

            ImageDiff diff = imgDiff.makeDiff(actualImage, expectedImage);
            //ImageIO.write(diff.getDiffImage(), "png", new File(System.getProperty("user.dir") + "\\Screenshots\\Difference.png"));

            if (diff.hasDiff() == true) { //.withDiffSizeTrigger(90000)
                System.out.println(diff.getDiffSize());
                System.out.println("Images are different");
            } else {
                System.out.println("Images are same");
            }
            assertThat(diff.hasDiff()).isFalse();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getVisibleMapLayers() {
        return mapLayers.stream().filter(layer -> layer.getCssValue("display").equalsIgnoreCase("block")).collect(Collectors.toList()).size();
    }

    public int getNumberOfMapLayers() {
        return mapLayers.size();
    }

    public String getTextForTheSearchedPlace() {
        return searchedPlace.getText();
    }

    public void inputPlaceOrAddressToFindOnMap(String place) {
        findPlaceInputTextBox.clear();
        findPlaceInputTextBox.sendKeys(place);
        sleep(2000);
    }

    @Step("Zoom Out the map")
    public void clickOnZoomOut(int number) {
        while (number > 0) {
            zoomOut.click();
            number--;
        }
        sleep(1500);
    }

    @Step("Zoom In the map")
    public void clickOnZoomIn(int number) {
        while (number > 0) {
            zoomIn.click();
            number--;
        }
        sleep(3000);
    }

    public void pan4(){
        action.clickAndHold(map).moveByOffset(200, 200).release().perform(); //x-300
    }

    public void pan() {
        /*action.moveToElement(map)
        .clickAndHold()
        .moveByOffset(300,0)
        .moveByOffset(0,200)
        .release()
        .perform();*/
        action.clickAndHold(map).moveByOffset(200, 200).release().perform(); //x-300
        for (int i = 0; i < 6; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }

        for (int i = 0; i < 4; i++) {//10
            sleep(1000);
            action.clickAndHold(map).moveByOffset(100, 0).release().perform();
        }

        for (int i = 0; i < 5; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }

        for (int i = 0; i < 4; i++) {//10
            sleep(1000);
            action.clickAndHold(map).moveByOffset(100, 0).release().perform();
        }

        for (int i = 0; i < 5; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }

        clickOnZoomIn(2);

        for (int i = 0; i < 8; i++) {//10
            sleep(1000);
            action.clickAndHold(map).moveByOffset(100, 0).release().perform();
        }

        for (int i = 0; i < 10; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }

        for (int i = 0; i < 4; i++) {//10
            sleep(1000);
            action.clickAndHold(map).moveByOffset(100, 0).release().perform();
        }

        clickOnZoomIn(2);

        for (int i = 0; i < 4; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }

        for (int i = 0; i < 1; i++) {//10
            sleep(1000);
            action.clickAndHold(map).moveByOffset(315, 0).release().perform();
        }

        for (int i = 0; i < 1; i++) { //18
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 52).release().perform();
        }

        sleep(5000);
    }

    public void pan2() {
        /*action.moveToElement(map)
        .clickAndHold()
        .moveByOffset(300,0)
        .moveByOffset(0,200)
        .release()
        .perform();*/
        for (int i = 0; i < 2; i++) {//12
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 76).release().perform();
        }
        for (int i = 0; i < 1; i++) {
            sleep(1000);
            action.clickAndHold(map).moveByOffset(5, 0).release().perform();
        }
    }

    public void pan3() {
        /*action.moveToElement(map)
        .clickAndHold()
        .moveByOffset(300,0)
        .moveByOffset(0,200)
        .release()
        .perform();*/
        for (int i = 0; i < 1; i++) {
            sleep(1000);
            action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        }


        for (int i = 0; i < 4; i++) {
            sleep(1000);
            action.clickAndHold(map).moveByOffset(150, 0).release().perform(); //x=116
        }

        sleep(2000);
    }

    public void panToCairns() {

        action.moveToElement(map)
                .clickAndHold()
                .moveByOffset(500, 0)
                .moveByOffset(0, 300)
                .release()
                .perform();
        action.clickAndHold(map).moveByOffset(0, 200).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold(map).moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(0, 100).release().perform();
        action.clickAndHold().moveByOffset(400, 100).release().perform();


        sleep(5000);
    }

    public void clickOnStickPin() {
        action.click(driver.findElement(By.id("mapNotes_7738_3_layer")))
                .perform();
        System.out.println(driver.findElement(By.cssSelector(".titlePane")).isDisplayed());
        sleep(5000);
    }


    @Step("Verify that map zoomed in {zoomedInBy} times")
    public void verifyThatMapZoomedInBy(int zoomedInBy, String previousZoomValue) {
        String currentZoomValue = this.getMapZoomData();
        int expectedValue = Integer.parseInt(previousZoomValue) + zoomedInBy > 23 ? 23 : Integer.parseInt(previousZoomValue) + zoomedInBy;
        assertThat(Integer.parseInt(currentZoomValue)).isEqualTo(expectedValue);
    }

    @Step("Verify that map zoomed out {zoomedOutBy} times")
    public void verifyThatMapZoomedOutBy(int zoomedOutBy, String previousZoomValue) {
        String currentZoomValue = this.getMapZoomData();
        int expectedValue = Integer.parseInt(previousZoomValue) - zoomedOutBy < 0 ? 0 : Integer.parseInt(previousZoomValue) - zoomedOutBy;
        assertThat(Integer.parseInt(currentZoomValue)).isEqualTo(expectedValue);
    }

    @Step("Deselect all layers on the map")
    public void deselectLayers() {
        mapContents.stream().filter(layer -> layer.getAttribute("checked") != null).collect(Collectors.toList()).forEach(WebElement::click);
        sleep(1000);
    }

    @Step("Select all layers on the map")
    public void selectLayers() {
        sleep(6000);
        //wait.until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("#tocContentPane .toc_layer"), "Indigenous Land Use Agreements"));
        //mapContents.stream().filter(layer -> layer.getAttribute("checked") == null).collect(Collectors.toList()).forEach(WebElement::click);
        List<WebElement> l = mapContents.stream().filter(layer -> layer.getAttribute("checked") == null).collect(Collectors.toList());
        for (WebElement elem : l){
            if(errorPopupOkBtn.isDisplayed()){
                errorPopupOkBtn.click();
                sleep(1000);
            }
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            elem.click();
            sleep(1000);
        }
    }

    public void selectLayer(String layerName){
        sleep(3000);
        WebElement layer = this.getLayer(layerName);
        layer.findElement(By.cssSelector("input[type='checkbox']")).click();
    }

    @Step("Click on Base Map")
    public void clickOnBaseMapBtn() {
        wait.until(ExpectedConditions.elementToBeClickable(baseMapBtn));
        baseMapBtn.click();
        sleep(3000);
    }

    @Step("Click on New Map")
    public void createANewMap(){
        sleep(2000);
        newMapBtn.click();
        if(newMapBtn.getAttribute("id").equalsIgnoreCase("recentMapsLabel"))
            createNewMapDrpDwnOption.click();
        clickYesOptionToDiscardCurrentMap.click();
    }

    @Step("Click on Show Contents of Map")
    public void clickOnShowMapContentBtn() {
        /*try {
            //driver.findElement(By.cssSelector("#webmap-details-content, #webmap-details-about-content")).click();
//            WebElement content = driver.findElement(By.cssSelector("span[widgetid='webmap-details-content'] #webmap-details-content, #webmap-details-about-content"));
            WebElement content = driver.findElement(By.cssSelector("[widgetid='webmap-details-about-content'] span[title='Show Contents of Map']"));
            if(content.getAttribute("aria-pressed") != "null"){
//                content.click();
                clickOnElementWithJS(content);
                //content.click();
                System.out.println("show msp content cliked");
            }
        } catch (NoSuchElementException e) {
            clickOnElementWithJS(showMapContentBtn);
            System.out.println("Clicked on show contents");
            //showMapContentBtn.click();
        }*/
        clickOnElementWithJS(showMapContentBtn);
        sleep(1500);
    }

    public void waitUntilIndigenousLayerIsDisplayed() {
        wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath("//div[@id='tocContentPane']//div[contains(@class,'toc_layer')]//span[contains(text(),'Indigenous Land Use Agreements')]"), "Indigenous Land Use Agreements"));
    }

    public void clickOnShowLegendOfMap(){
        showMapLegendBtn.click();
    }

    @Step("Get map legend icons")
    public List<String> getMapLegendIcons() {
        legendIcons.stream().forEach(item-> System.out.println(item.getAttribute("outerHTML")));
        return legendIcons.stream().map(item -> item.getAttribute("outerHTML")).collect(Collectors.toList());
    }

    @Step("Click on Details")
    public void clickOnDetails() {
        sleep(1000);
        System.out.println(details.getCssValue("aria-pressed"));
        System.out.println(details.getAttribute("aria-pressed"));
        if (details.getAttribute("aria-pressed") == null || details.getAttribute("aria-pressed").equalsIgnoreCase("false")){
            System.out.println("Clicking on Details ");
//            action.moveToElement(details).click().perform();
            clickOnElementWithJS(details);
            sleep(2000);
        }

    }

    @Step("Close Details tab")
    public void closeDetails() {
        if (details.getCssValue("aria-pressed").equalsIgnoreCase("true"))
            details.click();
    }

    @Step("Select a basemap")
    public void selectBaseMap(String baseMap) {
        baseMaps.stream().filter(map -> map.getText().equalsIgnoreCase(baseMap)).collect(Collectors.toList()).get(0).click();
        sleep(3000);
    }

    @Step("Click on Add")
    public void clickOnAddBtn() {
        sleep(2000);
        baseMapAddBtn.click();
    }

    @Step("Browse Living Atlas Layers")
    public void clickOnBrowseLivingAtlasLayers() {
        browseLivingAtlasLayers.click();
        sleep(3000);
    }

    @Step("Select Search for Layers")
    public void clickOnSearchForLayers() {
        sleep(2000);
        searchLayers.click();
        sleep(3000);
    }

    @Step("Click on drop down to search layers")
    public void clickOnDrpDwnSearchLayersIn(){
        searchLayerInDrpDwn.click();
    }

    @Step("Search for layer {layerToBeSearched}")
    public void inputLayerNameInSearchBox(String layerToBeSearched){
        searchLayersTextBox.clear();
        searchLayersTextBox.sendKeys(layerToBeSearched);
        searchLayersTextBox.sendKeys(Keys.ENTER);
        sleep(1000);
        addScreenshotToStep();
    }

    @Step("Select option {option} to search for layers")
    public void selectOptionToSearchLayersIn(String option){
        clickOnDrpDwnSearchLayersIn();
        driver.findElements(By.cssSelector(".drp-text-option__link")).stream().
                filter(elem -> elem.getText().equalsIgnoreCase(option)).
                findFirst().get().click();
    }

    @Step("Select and add layer {layerName} to the map")
    public void selectAndAddLayerToMap(String layerName) {
        System.out.println(searchedLayers.size());
        searchedLayers.stream().filter(layer -> layer.getText().toLowerCase().contains(layerName.toLowerCase())).collect(Collectors.toList()).
                get(0).click();
        addLayerToMapBtn.click();
        sidePanelCloseBtn.click();
        sleep(5000);
    }

    @Step("Click on back button")
    public void clickOnBackBtn(){
        backBtn.click();
    }

    @Step("Select and add layer {layerName} to the map")
    public void selectAndAddFeatureLayerToMap(String layerName) {
        System.out.println(searchedFeatureLayers.size());
        searchedFeatureLayers.stream().filter(layer -> layer.getText().toLowerCase().contains(layerName.toLowerCase())).collect(Collectors.toList()).
                get(0).click();
        addLayerToMapBtn.click();
        sidePanelCloseBtn.click();
        sleep(5000);
    }

    @Step("Click on Add Map Notes dropdown option")
    public void clickOnAddMapNotes() {
        addMapNotes.click();
    }

    @Step("Click on Create Map Notes in the popup dialog")
    public void clickOnCreateMapNotes() {
        sleep(4000);
        driver.findElement(By.cssSelector("#mapnotes-title-dialog")).click();
        driver.findElement(By.cssSelector("span[widgetid='mapnotes-create'] .dijitButtonNode")).click();
        //driver.findElement(By.cssSelector("span[widgetid='mapnotes-create']")).click();
        //createMapNoteBtn.click();
    }

    @Step("Create a map note with name {name} and template {template}")
    public void clickOnCreateMapNotes(String name, String template) {
        sleep(4000);
        WebElement nameElem = driver.findElement(By.name("mapnotes-title"));
        nameElem.clear();
        nameElem.sendKeys(name);
        driver.findElement(By.cssSelector("#mapnotes-title-dialog")).click();
        driver.findElement(By.cssSelector("span[widgetid='mapnotes-create'] .dijitButtonNode")).click();
        //driver.findElement(By.cssSelector("span[widgetid='mapnotes-create']")).click();
        //createMapNoteBtn.click();
    }

    @Step("Add Stick Pin on map")
    public void addStickPinOnMap() {
        stickpin.click();
        action.moveToElement(driver.findElement(By.id("map_layers"))).
                click().perform();
        driver.switchTo().frame(driver.findElement(By.id("dijit_Editor_0_iframe")));
        driver.findElement(By.id("dijitEditorBody")).sendKeys("This is descirption");
        driver.switchTo().defaultContent();
    }

    @Step("Add map note {mapNote}")
    public void addMapNote(String mapNote) {

        selectTypeOfNoteToAdd(mapNote);

        if (mapNote.contains("Freehand")) {
            action.moveToElement(driver.findElement(By.id("map_layers"))).moveByOffset(50, -100)
                    .pause(Duration.ofSeconds(1))
                    .clickAndHold() //driver.findElement(By.id("map_layers"))
                    .pause(Duration.ofSeconds(1))
                    .moveByOffset(200, -100)
                    .pause(Duration.ofSeconds(1))
                    .release(driver.findElement(By.id("map_layers")))
                    .perform();
            sleep(2000);
        } else if (mapNote.equalsIgnoreCase("Area")) {
            action.moveToElement(map, 0, 100)
                    .click()
                    .moveToElement(map, 50, 0)
                    .doubleClick()
                    .perform();
        } else if (mapNote.equalsIgnoreCase("Text")) {
            action.moveToElement(map, 0, 100)
                    .click();
            driver.findElement(By.className("esriTextEditorInput"))
                    .sendKeys("test");
            action.sendKeys(Keys.ENTER)
                    .perform();
            return;
        } else if (!mapNote.equalsIgnoreCase("Stickpin")) {
            action.moveToElement(map, 0, 100)
                    .click()
                    .perform();
        }
        sleep(2000);
        inputTitleAndDescriptionForMapNote("Title", "This is description of a note");
        closeMapNotePopup();
    }

    @Step("Add map note {mapNote}")
    public void addMapNoteWithTitleAndDescription(String mapNote, String title, String description) {

        selectTypeOfNoteToAdd(mapNote);

        if (mapNote.contains("Freehand")) {
            action.moveToElement(driver.findElement(By.id("map_layers"))).moveByOffset(50, -100)
                    .pause(Duration.ofSeconds(1))
                    .clickAndHold() //driver.findElement(By.id("map_layers"))
                    .pause(Duration.ofSeconds(1))
                    .moveByOffset(200, -100)
                    .pause(Duration.ofSeconds(1))
                    .release(driver.findElement(By.id("map_layers")))
                    .perform();
            sleep(2000);
        } else if (mapNote.equalsIgnoreCase("Area")) {
            action.moveToElement(map, 0, 100)
                    .click()
                    .moveToElement(map, 50, 0)
                    .doubleClick()
                    .perform();
        } else if (mapNote.equalsIgnoreCase("Text")) {
            action.moveToElement(map, 0, 100)
                    .click();
            driver.findElement(By.className("esriTextEditorInput"))
                    .sendKeys("test");
            action.sendKeys(Keys.ENTER)
                    .perform();
            return;
        } else if (!mapNote.equalsIgnoreCase("Stickpin")) {
            action.moveToElement(map, 0, 100)
                    .click()
                    .perform();
        }
        sleep(2000);
        inputTitleAndDescriptionForMapNote(title, description);
        //closeMapNotePopup();
    }

    @Step("Change color of the symbol. Choose symbol in a row {row} and column {column}")
    public String changeColorOfTheSymbol(int row, int column) {
        clickOnChangeSymbol();
        String chosenColor = chooseColorInRowAndColumn(row, column);
        sleep(1000);
        return chosenColor;
    }

    public float getTransparencyValueInChangeSymbolDialogue() {
        return Float.parseFloat(changeSymbolDialogueTransparencyValue.getAttribute("value"));
    }

    @Step("Set transparency as {valueToBeSet} in Change Symbol dialoge")
    public void setTransparencyInChangeSymbolDialogue(int valueToBeSet) {
        clickOnChangeSymbol();
        if (valueToBeSet > this.getTransparencyValueInChangeSymbolDialogue()) {
            while (this.getTransparencyValueInChangeSymbolDialogue() < valueToBeSet) {
                action.sendKeys(changeSymbolTransparencySlider, Keys.ARROW_RIGHT).release().perform();
            }
        } else if (valueToBeSet < this.getTransparencyValueInChangeSymbolDialogue()) {
            while (this.getTransparencyValueInChangeSymbolDialogue() > valueToBeSet) {
                action.sendKeys(changeSymbolTransparencySlider, Keys.ARROW_LEFT).release().perform();
            }
        }

        //Click on Ok Button
        clickOnElementWithJS(driver.findElement(symbolPopupOkBtn));

        sleep(2000);
    }

    public void addStickPinAndChangeSymbol(int row, int column) {
        driver.findElement(By.xpath("//div[contains(text(),'" + "Stickpin" + "')][@class='itemLabel']/..")).click();
        sleep(2000);
        action.moveToElement(driver.findElement(By.id("map_layers")), -100, -50).
                click().perform();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(".atiLabel[data-fieldname='TITLE'] + td .dijitInputInner")))).sendKeys("Test");
        driver.switchTo().frame(0);
        driver.findElement(By.id("dijitEditorBody")).sendKeys("This is description");
        driver.switchTo().defaultContent();

        //Change the symbol of the stickpin
        driver.findElement(By.xpath("//span[text()='Change Symbol']/../..")).click();
        //driver.switchTo().frame(0);
        //driver.findElement(By.id("tpick-surface-500")).click();
        //driver.findElement(By.cssSelector("#symbolStylerDlg table.dojoxGridRowTable .item")).click();
        WebElement symbolToChoose = driver.findElement(By.cssSelector("#symbolStylerDlg .dojoxGridRow:nth-child(" + row + ") .dojoxGridCell:nth-child(" + column + ")"));
        symbolToChoose.click();
        String href = symbolToChoose.findElement(By.cssSelector("image")).getAttribute("xlink:href");
        System.out.println(href);
        driver.findElement(symbolPopupOkBtn).click();
        driver.findElement(By.xpath("//span[text()='Close']/../..")).click();
        sleep(3000);
        WebElement mapNote = driver.findElement(By.cssSelector("g[data-geometry-type] image,path,text"));
        System.out.println(mapNote.getAttribute("xlink:href"));
        System.out.println(getNoOfMapNotes());
    }

    @Step("Click on {noteType} to create a map note")
    public void selectTypeOfNoteToAdd(String noteType) {
        driver.findElement(By.xpath("//div[contains(text(),'" + noteType + "')][@class='itemLabel']/..")).click();
        sleep(2000);
        action.moveToElement(driver.findElement(By.id("map_layers")), -200, 200).
                click().perform();
    }

    @Step("Input Title and Description for a map note")
    public void inputTitleAndDescriptionForMapNote(String title, String description){
        action.moveToElement(driver.findElement(By.cssSelector(".atiLabel[data-fieldname='TITLE'] + td .dijitInputInner"))).sendKeys(title).perform();
        driver.switchTo().frame(0);
        WebElement descriptionTxtBox = driver.findElement(By.id("dijitEditorBody"));
        descriptionTxtBox.clear();
        descriptionTxtBox.sendKeys(description);
        driver.switchTo().defaultContent();
    }

    @Step("Close Map Note Popup")
    public void closeMapNotePopup(){
        //Close the popup window
        sleep(1000);
        driver.findElement(By.xpath("//span[text()='Close']/../..")).click();
        sleep(3000);
    }

    @Step("Create a text map note")
    public void addTextMapNote(String text){
        selectTypeOfNoteToAdd("Text");

        //Move cursor on to the map
        action.moveToElement(map, -50, 50)
                .click();

        //Type Text Note on the map
        driver.findElement(By.className("esriTextEditorInput"))
                .sendKeys(text);
        action.sendKeys(Keys.ENTER)
                .perform();

        //inputTitleAndDescriptionForMapNote("Title", "This is description of a text note");
        //closeMapNotePopup();
    }

    @Step("Create a line and change it's color")
    public String addLineAndChangeSymbolColorByChoosingColorIn(int row, int column) {
        selectTypeOfNoteToAdd("Line");

        sleep(1000);

        //Move cursor to the map
        action.moveToElement(driver.findElement(By.id("map_layers")), -200, 200).
                click().perform();

        //Draw a line
        action.moveToElement(driver.findElement(By.id("map_layers")), -150, 150)
                .pause(Duration.ofSeconds(1))
                .clickAndHold()
                .pause(Duration.ofSeconds(1))
                //.release(driver.findElement(By.id("map_layers")))
                .doubleClick()
                .perform();
        sleep(2000);

        inputTitleAndDescriptionForMapNote("Line Note", "This is a description for a line");

        //Change the color of the line
        String chosenColor = changeColorOfTheSymbol(row, column);

        //Close the popup window
        closeMapNotePopup();

        return chosenColor;
    }

    @Step("Choose color for the note in row {row} and column {column}")
    public String chooseColorInRowAndColumn(int row, int column){

        WebElement colorToChoose = driver.findElement(By.cssSelector(".esriPalette.esriContainer .esriPalette .esriSwatchRow:nth-child(" + row + ") .esriSwatch:nth-child(" + column + ")"));
        colorToChoose.click();

        String chosenColor = colorToChoose.getAttribute("style");
        System.out.println(chosenColor);

        //Click on Ok Button
        clickOnElementWithJS(driver.findElement(symbolPopupOkBtn));
        /*action.moveToElement(driver.findElement(symbolPopupOkBtn)).
                click(driver.findElement(symbolPopupOkBtn)).
                perform();*/
        sleep(2000);

        return chosenColor;
    }

    @Step("Click on change symbol")
    public void clickOnChangeSymbol(){
        driver.findElement(By.xpath("//span[text()='Change Symbol']/../..")).click();
    }

    @Step("Verify that color of the symbol is changed as expected")
    public void verifyColorOfTheSymbolIsChangedTo(String chosenColor){
        //Verify that color of the line is changed by counting the number of notes on map with chosen color
        int num = mapNotes.stream().filter(note -> note.getAttribute("fill")!=null && chosenColor.contains(note.getAttribute("fill"))).collect(Collectors.toList()).size();
        assertThat(num).isEqualTo(1);
    }

    @Step("Verify that color of the line is changed as expected")
    public void verifyColorOfTheLineIsChangedTo(String chosenColor){
        //Verify that color of the line is changed by counting the number of notes on map with chosen color
        int num = mapNotes.stream().filter(note -> note.getAttribute("stroke")!=null && chosenColor.contains(note.getAttribute("stroke"))).collect(Collectors.toList()).size();
        assertThat(num).isEqualTo(1);
    }

    public int getNoOfNotesWithTransparency(String transparencyValue){
        return mapNotes.stream().filter(note -> note.getAttribute("fill-opacity")!=null && note.getAttribute("fill-opacity").equals(transparencyValue)).collect(Collectors.toList()).size();
    }

    @Step("Verify that transparency of the symbol is changed as expected")
    public void verifyTransparencyOfTheSymbolIsChangedTo(String chosenTransparency, String chosenColor){
        //Verify that color of the line is changed by counting the number of notes on map with chosen color
        int num = mapNotes.stream().filter(note -> note.getAttribute("fill")!=null && chosenColor.contains(note.getAttribute("fill"))).filter(note -> note.getAttribute("fill-opacity")!=null && chosenTransparency.equals(note.getAttribute("fill-opacity"))).collect(Collectors.toList()).size();
        assertThat(num).isEqualTo(1);
    }

    @Step("Verify that size of the symbol is changed as expected")
    public void verifySizeOfTheSymbolIsChangedTo(String expectedSize){

        //Verify that size of the symbol is changed
        WebElement layer = mapNoteLayers.stream().filter(note -> note.getAttribute("id").contains("BushfireCurrentIncident")).collect(Collectors.toList()).get(0);
        WebElement layerImg = layer.findElement(By.tagName("image"));
        assertThat(layerImg.getAttribute("width")).isEqualTo(expectedSize);
        assertThat(layerImg.getAttribute("height")).isEqualTo(expectedSize);
    }

    @Step("Verify that symbol is changed to the chosen symbol")
    public void verifySymbolIsChangedTo(String expectedSymbolImage){
        WebElement layer = mapNoteLayers.stream().filter(note -> note.getAttribute("id").contains("BushfireCurrentIncident")).collect(Collectors.toList()).get(0);
        WebElement layerImg = layer.findElement(By.tagName("image"));
        assertThat(layerImg.getAttribute("xlink:href")).isEqualToIgnoringCase(expectedSymbolImage);
    }

    public int getNoOfMapNotes() {
        mapNotes.stream().forEach(note -> System.out.println(note.getText()));
        return mapNotes.size();
    }

    @Step("Verify that map has {expectedNoOfNotes} notes")
    public void verifyNumberOfMapNotes(int expectedNoOfNotes) {
        assertThat(this.getNoOfMapNotes()).isEqualTo(expectedNoOfNotes);
    }

    @Step("Verify that text {noteText} is displayed on the map")
    public void verifyTextMapNote(String noteText) {
        assertThat(
                this.mapTextNotes.stream().filter(
                        note -> note.getText().contains(noteText)).collect(Collectors.toList()).
                        size()).isEqualTo(1);
    }

    @Step("Click on Save Map")
    public void clickOnSaveWebMapBtn() {
        saveWebMapDrpDwn.click();
        saveWebMapBtn.click();
    }

    @Step("Click on Save As Map")
    public void clickOnSaveAsWebMapBtn() {
        saveWebMapDrpDwn.click();
        saveAsWebMapBtn.click();
    }

    @Step("Input Webmap name")
    public String inputWebMapName(String title) {
        sleep(5000);
        WebElement mapNameTxtBox = driver.findElement(By.cssSelector("input#save-webmap-title"));
        mapNameTxtBox.clear();
        mapNameTxtBox.sendKeys(title);
        sleep(5000);
        return title;
    }

    @Step("Input map tags")
    public void inputTags(String tag) {
        driver.findElement(By.cssSelector("#save-webmap-tags input")).sendKeys(tag);
        sleep(5000);
    }

    @Step("Save Web map")
    public void saveWebMap() {
        driver.findElement(By.cssSelector("span#save-webmap-ok")).click();
        sleep(5000);
    }

    @Step("Logout from the portal")
    public void logout() {
        portalUser.click();
        signOut.click();
    }

    @Step("Go to Content Page")
    public void goToContentPage() {
        wait.until(ExpectedConditions.elementToBeClickable(homeNavigation));
        homeNavigation.click();
        wait.until(ExpectedConditions.elementToBeClickable(content));
        content.click();
        sleep(3000);
    }

    @Step("Go to Groups Page")
    public void goToGroupsPage() {
        wait.until(ExpectedConditions.elementToBeClickable(homeNavigation));
        homeNavigation.click();
        wait.until(ExpectedConditions.elementToBeClickable(groups));
        groups.click();
        sleep(3000);
    }

    @Step("Verify that bookmark is present")
    public void verifyThatBookMarkIsPresent(String bookmark) {
        assertThat(this.isBookmarkPresent(bookmark)).isTrue();
    }

    @Step("Verify that no layers are visible on the map")
    public void verifyThatNoLayersAreVisible() {
        assertThat(this.getVisibleMapLayers()).isEqualTo(0);
    }

    @Step("Verify that all the layers are visible on the map")
    public void verifyThatAllLayersAreVisible() {
        assertThat(this.getNumberOfMapLayers()).isEqualTo(this.getVisibleMapLayers());
    }

    @Step("Click on Map to show the popup")
    public void clickOnNoteOnMapLayer(){
        //scrollToElementWithJS(mapNoteLayers.get(0));
        sleep(5000);
        //action.moveToElement(driver.findElement(By.id("map_layers"))).click().perform();
        clickOnElementWithJS(driver.findElement(By.id("map_layers")));
        System.out.println(mapNotes.size());
        action.moveToElement(mapNotes.get(2)).click().perform();
        //clickOnElementWithAdvancedJS(mapNotes.get(0));
    }

    @Step("Click on Map to show the popup")
    public void clickOnMapToShowPopup(){
        sleep(1000);
        action.moveToElement(map).moveByOffset(100,10).click().perform();
    }

    @Step("Verify that Popup is displayed on the map")
    public void verifyThatPopupIsPresentOnMap(){
        action.moveToElement(mapPopup).perform();
        assertThat(mapPopup.isDisplayed()).isTrue();
        action.moveToElement(getMapPopupCloseBtn).click().perform();
    }

    public void clickOnErrorPopupOkBtn() {
        sleep(2000);
        wait.until(ExpectedConditions.visibilityOf(errorPopupOkBtn));
        errorPopupOkBtn.click();
    }

}
