package pages;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.LoadProperties;
import utils.Utils;
import utils.WebDriverController;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class BasePage {

    private static Logger logger = LogManager.getLogger(WebDriverController.class);
    WebDriver driver;
    private Properties properties = LoadProperties.getProperties();
    SoftAssertions softly = new SoftAssertions();

    public WebDriverWait wait;
    public Actions action;
    public String token;
    public final String test_env = properties.getProperty("environment");
    public static final String DOWNLOAD_DIRECTORY_PATH = System.getProperty("user.dir") + "\\Downloads";

    @FindBy(css = ".esri-header-account-control--signin")
    WebElement signInLink;

    @FindBy(css = ".arcgis-login #loginTitle")
    WebElement arcgisLogin;

    @FindBy(css = ".enterprise-info #enterpriseTitle")
    WebElement enterpriseLogin;

    @FindBy(css = "#enterprisePanel .btn")
    WebElement corporateLoginBtn;

    @FindBy(css = "input[type='Email']")
    WebElement corpEmailTxtBox;

    @FindBy(css = "input[value='Next']")
    WebElement corpLoginNextBtn;

    @FindBy(id = "user_username")
    WebElement user_username;

    @FindBy(id = "user_password")
    WebElement user_password;

    @FindBy(id = "signIn")
    WebElement signInBtn;


    public BasePage() {
        driver = WebDriverController.getWebDriverControllerInstance().getDriver();
        wait = new WebDriverWait(driver, 80);
        action = new Actions(driver);

        logger.info("Creating BasePage");
        logger.info(driver.hashCode() + " base page: " + this.hashCode());
        PageFactory.initElements(driver, this);
    }

    public void login() {

        logger.info("LOGIINING IN AGAIN!!!!!!!!!");
        //if(test_env.equalsIgnoreCase("sandpit"))
            //signInLink.click();
        action.moveToElement(arcgisLogin).click().perform();
        sleep(3000);
        user_username.clear();
        user_password.clear();
        user_username.sendKeys( properties.getProperty(test_env+ "_"+ "admin_username"));
        user_password.sendKeys(properties.getProperty(test_env + "_"+ "admin_password"));
        signInBtn.click();
        sleep(3000);
    }

    @Step("Login through corporate login option")
    public void corporateLogin(String emailId) {

        logger.info("LOGIINING IN !!!!!!!!!");
        //signInLink.click();
        sleep(2000);
        if(enterpriseLogin.getAttribute("aria-expanded") == null || !enterpriseLogin.getAttribute("aria-expanded").equals("true")){
            action.moveToElement(enterpriseLogin).click().perform();
            //clickOnElementWithJS(enterpriseLogin);
            System.out.println("Clicking hereeeeeeeeeee");
        }

        //clickOnElementWithJS(enterpriseLogin);
        sleep(3000);
        corporateLoginBtn.click();

        if(!properties.getProperty("browser").equalsIgnoreCase("edge")){
            corpEmailTxtBox.sendKeys(emailId);
            corpLoginNextBtn.click();
            sleep(3000);
        }
    }

    public void login(String user) {
        if (user == null) {
            login();
            return;
        } else if (user.equalsIgnoreCase("admin")) {
            signInLink.click();
            arcgisLogin.click();
            user_username.sendKeys(properties.getProperty("admin_username"));
            user_password.sendKeys(properties.getProperty("admin_password"));
            signInBtn.click();
        }
        sleep(3000);
    }

    public void login(String username, String password) {

        logger.info("LOGIINING IN AGAIN!!!!!!!!!");
        //if(test_env.equalsIgnoreCase("sandpit"))
            //signInLink.click();
        if(!arcgisLogin.getAttribute("aria-expanded").equalsIgnoreCase("true"))
            arcgisLogin.click();
        user_username.sendKeys(username);
        user_password.sendKeys(password);
        signInBtn.click();
        sleep(3000);
    }

    @Step("Open URL {url}")
    public void openPage(String url) {
        driver.get(url);
    }

    public void sleep(long miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void switchToNewWindow(){
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
    }

    public void switchToPreviousWindow(){
        String currentWindow = driver.getWindowHandle();
        for(String winHandle : driver.getWindowHandles()){
            if(!winHandle.equalsIgnoreCase(currentWindow))
                driver.switchTo().window(winHandle);
        }
    }

    public void clickOnElementWithJS(WebElement elem){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", elem);
    }

    public void clickOnElementWithAdvancedJS(WebElement elem) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].dispatchEvent(new MouseEvent('click'))", elem);
    }

    public void switchToWindow(String winHandle){
        driver.switchTo().window(winHandle);
    }

    public void scrollToElementWithJS(WebElement elem){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView(true);", elem);
        sleep(2000);
    }

    public void setAttributeValueWithJS(WebElement elem, String attribute, String value){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])", elem, attribute, value);
        sleep(1000);
    }

    public void closeBrowser() {
        driver.quit();
    }

    @Step("Verify that Legends icons for map and app are the same")
    public  static void verifyTwoLists(List<String> map, List<String> app){
        System.out.println(map.size() + " --- " + app.size());
        map.stream().forEach(item-> System.out.println("xxx --> " + item));
        map.removeIf(item -> item==null);
        app.removeIf(item -> item==null);
        assertThat(map.size()).isEqualTo(app.size());
        for(String mapValue : map){
           /* System.out.println(mapItemFill);
            System.out.println(appItemFill);*/
            if(mapValue != null && app.get(map.indexOf(mapValue)) != null)
                assertThat(mapValue.equalsIgnoreCase(app.get(map.indexOf(mapValue))));
        }
    }

    public  static void verifyTwoLists2(List<WebElement> map, List<WebElement> app){
        System.out.println(map.size() + " --- " + app.size());
        map.stream().forEach(item-> System.out.println("xxx --> " + item));
        assertThat(map.size()).isEqualTo(app.size());
        System.out.println(map.hashCode());
        System.out.println(app.hashCode());
    }

    public void addScreenshotToStep() {
        Allure.addAttachment("Click here for screenshot", new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
    }

    public void moveMouseToElement(WebElement element) {
        Locatable hoverItem = (Locatable) element;
        Mouse mouse = ((HasInputDevices) driver).getMouse();
        mouse.mouseMove(hoverItem.getCoordinates());
    }

    @Step("Verify that file {fileName} is downloaded")
    public void verifyThatFileIsDownloaded(String fileName) {
        assertThat(Utils.doesFileExist(DOWNLOAD_DIRECTORY_PATH + "\\" + fileName)).isTrue();
    }

    public void cleanDownloadDirectory() {
        try {
            FileUtils.cleanDirectory(new File(DOWNLOAD_DIRECTORY_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
