package pages;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.CustomCondition;
import utils.WebDriverController;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class HeaderPage extends BasePage {

    private static Logger logger = LogManager.getLogger(WebDriverController.class);

    @FindBy(css = ".if-can-create")
    WebElement createGroupBtn;

    @FindBy(css = "#esri-header-menus-link-desktop-0-0")
    WebElement home_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-1")
    WebElement gallery_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-2")
    WebElement map_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-3")
    WebElement scene_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-4")
    WebElement groups_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-5")
    WebElement content_tab;

    @FindBy(css = "#esri-header-menus-link-desktop-0-6")
    WebElement organization_tab;

    @FindBy(css = "#esri-header-account-control")
    WebElement accountHeader;

    @FindBy(css = ".esri-header-account-signin-control.-logout")
    WebElement signOutBtn;

    @FindBy(id = "securityAnswer")
    WebElement securityAnswer;

    @FindBy(id = "okBtn")
    WebElement okBtn;

    @FindBy(id = "esri-header-account-control")
    WebElement userProfileIcon;

    @FindBy(css = ".esri-header-account-content-link[href*='./user']")
    WebElement myProfileLink;

    @FindBy(css = ".esri-header-account-content-link[href*='help']")
    WebElement helpLink;

    @FindBy(css = ".esri-header-account-name")
    WebElement headerUsername;

    @FindBy(css = ".flex-center-self")
    WebElement successMsg;

    @FindBy(css = ".if-has-file-errors")
    WebElement errorMsg;

    @FindBy(css = "a[href*='DataSetManager']")
    WebElement dataSetManagerLink;

    public HeaderPage() {
        PageFactory.initElements(driver, this);
    }

    public void clickOnAccountHeader() {
        wait.until(ExpectedConditions.elementToBeClickable(accountHeader));
        accountHeader.click();
    }

    @Step("Click on Sign Out")
    public void clickOnSignOut() {
        signOutBtn.click();
    }

    @Step("Click on Home Tab")
    public void clickOnHomeTab() {
        home_tab.click();
    }

    @Step("Verify that create group button is not displayed")
    public void verifyThatCreateBtnIsNotDisplayed() {
        assertThat(createGroupBtn.isDisplayed()).isFalse();
    }

    @Step("Click on Gallery Tab")
    public void clickOnGalleryTab() {
        gallery_tab.click();
    }

    @Step("Click on Map Tab")
    public void clickOnMapTab() {
        wait.until(ExpectedConditions.elementToBeClickable(map_tab));
        map_tab.click();
        sleep(3000);
    }

    @Step("Click on Scene Tab")
    public void clickOnSceneTab() {
        scene_tab.click();
    }

    @Step("Click on Group Tab")
    public void clickOnGroupsTab() {
        groups_tab.click();
    }

    @Step("Click on Content Tab")
    public void clickOnContentTab() {
        wait.until(ExpectedConditions.elementToBeClickable(content_tab));
        content_tab.click();
        sleep(5000);
    }

    @Step("Click on Organization Tab")
    public void clickOnOrgTab() {
        wait.until(ExpectedConditions.elementToBeClickable(organization_tab));
        organization_tab.click();
    }

    @Step("Log out")
    public void logout() {
        wait.until(ExpectedConditions.elementToBeClickable(accountHeader));
        //clickOnAccountHeader();
        clickOnElementWithAdvancedJS(accountHeader);
        clickOnSignOut();
    }

    public void inputSecurityAnswer(String answer){
        securityAnswer.sendKeys(answer);
        okBtn.click();
        sleep(500);
    }

    @Step("Click on user profile icon")
    public void clickOnUserProfileIcon(){
        action.moveToElement(userProfileIcon).click(userProfileIcon).perform();
        sleep(1000);
    }

    @Step("Click on My Profile")
    public void clickOnMyProfile(){
        wait.until(ExpectedConditions.elementToBeClickable(myProfileLink));
        myProfileLink.click();
        wait.until(CustomCondition.waitForPageToLoad());
        sleep(1000);
    }

    @Step("Click on Help")
    public void clickOnHelp(){
        wait.until(ExpectedConditions.elementToBeClickable(helpLink));
        helpLink.click();
        wait.until(CustomCondition.waitForPageToLoad());
        sleep(1000);
        clickOnAccountHeader();
    }

    @Step("Verify that Help document page is opened in a new window")
    public void verifyThatHelpDocIsOpenedInNewWindow(){
        assertThat(driver.getWindowHandles().size()).isEqualTo(2);
        String currentWindow = driver.getWindowHandle();
        switchToNewWindow();
        sleep(1000);
        assertThat(driver.getCurrentUrl()).containsIgnoringCase("portalhelp");
        switchToWindow(currentWindow);
        sleep(500);
    }

    public String getHeaderUsername(){
        return headerUsername.getText();
    }

    @Step("Verify that username {expectedUsername} is displayed in the header")
    public void verifyUsername(String expectedUsername){
        assertThat(this.getHeaderUsername()).isEqualToIgnoringCase(expectedUsername);
    }

    public String getSuccessMsg() {
        wait.until(ExpectedConditions.visibilityOf(successMsg));
        return successMsg.getText();
    }

    @Step("Verify that success message {msg} is displayed")
    public void verifyThatSuccessMsgIsDisplayed(String msg) {
        assertThat(this.getSuccessMsg()).isEqualToIgnoringCase(msg);
    }

    public String getErrorMsg(){
        return errorMsg.getText();
    }

    @Step("Verify that error message {expectedMsg} is displayed")
    public void verifyErrorMessage(String expectedMsg){
        assertThat(this.getErrorMsg()).isEqualToIgnoringCase(expectedMsg);
    }

    @Step("Click on DataSet Manager Link on home page")
    public void clickOnDataSetManagerLink() {
        dataSetManagerLink.click();
        sleep(10000);
    }

    @Step("Verify that DataSet Manager is opened in a new window")
    public void verifyThatDataSetManagerIsOpenedInNewWindow(){
        Set<String> currentWindowHandles = driver.getWindowHandles();
        assertThat(currentWindowHandles.size()).isEqualTo(2);
        switchToNewWindow();
        System.out.println(driver.getCurrentUrl());
        assertThat(driver.getCurrentUrl().contains("DataSetManager")).isTrue();
    }
}
