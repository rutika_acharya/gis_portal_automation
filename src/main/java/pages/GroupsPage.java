package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class GroupsPage extends HeaderPage {

    @FindBy(css = "a[href='#my']")
    WebElement myGroupsTab;

    @FindBy(css = "a[href='#featured']")
    WebElement featuredGroupsTab;

    @FindBy(css = "a[href='#organization']")
    WebElement myOrgGroupsTab;

    @FindBy(css = ".js-add-to-groups-btn")
    WebElement addToGroupsBtn;

    @FindBy(css = ".searchControls input")
    WebElement addToGroupsPopupSearchBox;

    @FindBy(css = ".control-bar__search input[type='search']")
    WebElement searchBoxGroup;

    @FindBy(css = "a.linkNoHref")
    List<WebElement> addToGroupsPopupGroupList;

    @FindBy(css = ".btn-blue")
    WebElement addToGroupsPopupAddBtn;

    @FindBy(css = "a.card-title-link")
    List<WebElement> groupContentList;

    @FindBy(css = "a.text-ellipsis.js-group-card-title")
    List<WebElement> groupsList;

    @FindBy(css = ".subnav a[href='#content']")
    WebElement groupContentTab;

    @FindBy(css = ".js-overview-content .card")
    List<WebElement> groupOverviewPageContent;

    @FindBy(css = "button[data-action='inviteUsers']")
    WebElement inviteUsersBtn;

    @FindBy(css = ".searchControls input")
    WebElement inviteUsersPopupSearchBox;

    @FindBy(css = "a.linkNoHref")
    private List<WebElement> popupUsersList;

    @FindBy(css = "span[widgetid='button_send-invite'] .dijitButtonNode")
    WebElement sendInvitationBtn;

    @FindBy(css = "button[data-action='openInvitations']")
    WebElement openInvitationsBtn;

    @FindBy(css = ".js-accept-invitation")
    WebElement acceptInvitationBtn;

    public GroupsPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Click on My Groups Tab")
    public void clickOnMyGroupsTab(){
        myGroupsTab.click();
    }

    @Step("Click on Featured Groups Tab")
    public void clickOnFeaturedGroupsTab(){
        featuredGroupsTab.click();
    }

    @Step("Click on My Organization's Groups Tab")
    public void clickOnMyOrgGroupsTab(){
        myOrgGroupsTab.click();
    }

    @Step("Search for {searchText} on groups tab")
    public void searchForGroupOrContent(String searchText) {
        searchBoxGroup.clear();
        searchBoxGroup.sendKeys(searchText);
        searchBoxGroup.sendKeys(Keys.ENTER);
        sleep(2000);
    }

    @Step("Search for group {groupName} in the popup")
    public void searchForGroupInAddGroupPopup(String groupName) {
        addToGroupsPopupSearchBox.clear();
        addToGroupsPopupSearchBox.sendKeys(groupName);
        addToGroupsPopupSearchBox.sendKeys(Keys.ENTER);
        sleep(5000);
    }

    @Step("Click on Add to Groups")
    public void clickOnAddToGroupsBtn(){
        addToGroupsBtn.click();
    }

    @Step("Select the group {groupName} from the list")
    public void selectGroupFromTheListAndAdd(String groupName) {
        System.out.println(addToGroupsPopupGroupList.size());
        addToGroupsPopupGroupList.stream().filter(group -> group.getText().equalsIgnoreCase(groupName)).findFirst().get().click();
        addToGroupsPopupAddBtn.click();
    }

    @Step("Click on the searched group {groupName}")
    public void selectGroup(String groupName) {
        groupsList.stream().filter(group -> group.getAttribute("title").equalsIgnoreCase(groupName)).findFirst().get().click();
    }

    @Step("Click on the searched content {contentName}")
    public void selectGroupContent(String contentName) {
        groupContentList.stream().filter(group -> group.getText().equalsIgnoreCase(contentName)).findFirst().get().click();
        sleep(2000);
    }

    @Step("Verify that group items are displayed")
    public void verifyThatGroupContentIsDisplayed(){
        assertThat(groupOverviewPageContent.size()).isGreaterThanOrEqualTo(1);
    }

    @Step("Click on Invite Users button")
    public void clickOnInviteUser(){
        inviteUsersBtn.click();
    }

    @Step("Click on Content Tab")
    public void clickOnContentTab(){
        groupContentTab.click();
    }

    @Step("Search for user {memberName}")
    public void searchForUser(String memberName) {
        inviteUsersPopupSearchBox.clear();
        inviteUsersPopupSearchBox.sendKeys(memberName);
        inviteUsersPopupSearchBox.sendKeys(Keys.ENTER);
        sleep(5000);
    }

    @Step("Select the user {username} from the list")
    public void selectUserFromTheList(String username) {
        System.out.println(popupUsersList.size());
        popupUsersList.stream().filter(user -> user.getAttribute("data-id").equalsIgnoreCase(username)).findFirst().get().click();
    }

    @Step("Click on send invitation button")
    public void clickOnSendInvitation(){
        sendInvitationBtn.click();
    }

    @Step("Verify that member has been added to the group {groupName}")
    public void verifyThatGroupIsPresent(String groupName) {
        assertThat(
                groupsList.stream().filter(group -> group.getAttribute("title").equalsIgnoreCase(groupName))
                        .collect(Collectors.toList()).size()
        ).isEqualTo(1);
    }

    @Step("Verify that member has received an invitation to join the group")
    public void verifyThatGroupInvitationIsReceived(){
        wait.until(ExpectedConditions.visibilityOf(openInvitationsBtn));
        assertThat(openInvitationsBtn.isDisplayed()).isTrue();
    }

    @Step("Open the invitation and join the group")
    public void acceptTheInvitationToJoinTheGroup() {
        openInvitationsBtn.click();
        wait.until(ExpectedConditions.elementToBeClickable(acceptInvitationBtn));
        acceptInvitationBtn.click();
    }
}
