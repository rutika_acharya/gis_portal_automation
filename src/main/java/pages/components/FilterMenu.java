package pages.components;

import org.openqa.selenium.By;

public interface FilterMenu {

    By selectFieldDrpDwn = By.cssSelector("div[widgetid='expr_1.fieldsList'] .dijitDownArrowButton");
    By selectOperationDrpDwn = By.cssSelector("div[widgetid='expr_1.operatorList'] .dijitDownArrowButton");
    By operatorList = By.cssSelector("div[id*='operatorList'][role='option']");
    By menuOption = By.cssSelector("div[role='option']");
    By valueTxtBox = By.cssSelector("input[id='expr_1.value']");
    By applyFilterBtn = By.cssSelector("span[widgetid='applyFilterButton']");
    By applyFilterAndZoomToBtn = By.cssSelector("span[widgetid='applyFilterAndZoomButton']");
    By removeFilterBtn = By.cssSelector("span[widgetid='removeFilterButton'] .dijitButtonNode");
}
