package pages.components;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Thread.sleep;

public class MyOrganization {

    @FindBy(css = ".js-filter-category-container div.js-category-filter a.filter-tree-link")
    List<WebElement> categories;

    @FindBy(css = "#uniqName_1_3 a")
    List<WebElement> itemTypes;

    WebDriver driver;
    WebDriverWait wait;

    public MyOrganization(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    @Step("Select filter Category as {categoryToBeSelected}")
    public void selectFilterCategory(String categoryToBeSelected) throws InterruptedException {
        categories.stream().filter(
                category -> category.getAttribute("data-category-name")
                        .equalsIgnoreCase(categoryToBeSelected)).findFirst().get().click();
        sleep(2000);
    }

    @Step("Select filter Item Type as {itemType}")
    public void selectFilterItemType(String itemType) {
        itemTypes.stream().forEach(item -> System.out.println(item.getText()));
        itemTypes.stream().filter(
                item -> item.getText()
                        .equalsIgnoreCase(itemType)).findFirst().get().click();
    }

    @Step("Select map {mapName}")
    public void selectMap(String mapName) {
        driver.findElement(By.cssSelector("a[title='" + mapName + "']")).click();
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
