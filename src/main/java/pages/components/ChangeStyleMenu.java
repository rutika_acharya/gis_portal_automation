package pages.components;

import org.openqa.selenium.By;

public interface ChangeStyleMenu {

    By locationMenuSelectBtn = By.cssSelector(".renderer_simple .rendererTitle+ .rendererSelect .dijitButtonNode");
    By locationMenuOptionBtn = By.cssSelector(".renderer_simple");//.renderer_unique .rendererSelect + .rendererOptions input
    By symbolsLink = By.cssSelector(".toolsLabel");
    By symbolSizeTxtBox = By.cssSelector("#symbolStylerDlgContent div[id^='widget_dijit_form_NumberSpinner'] input[id^='dijit_form_NumberSpinner']");
    By symbolPopupUpArrowBtn = By.cssSelector("#symbolStylerDlgContent div[id^='widget_dijit_form_NumberSpinner'] .dijitUpArrowButton");
    By symbolPopupOkBtn = By.xpath("//div[@id='symbolStylerDlg']//span[text()='OK']/../..");
    By changeStylePanelOkBtn = By.xpath("//*[@id='rendererSimpleContentClose']//span[text()='OK']/../..");
    By changeStylePanelDoneBtn = By.xpath("//*[@id='rendererGalleryContentClose']//span[text()='Done']/../..");
}
